CREATE OR ALTER TRIGGER diet_meal_trig ON dbo.Diet_meal
AFTER INSERT, UPDATE
AS
	DECLARE @duration INT;

	SELECT @duration = dbo.calculate_duration(diet_id, 'Diet') FROM inserted;

	UPDATE Diet
	SET duration = @duration
	FROM inserted i
	WHERE id = i.diet_id;
GO