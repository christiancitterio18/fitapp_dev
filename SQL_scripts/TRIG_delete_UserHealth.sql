CREATE OR ALTER TRIGGER dbo.health_delete ON dbo.User_health
INSTEAD OF DELETE
AS
	DECLARE @deletedUserId INT;
	DECLARE @deletedHealthId INT;
 
	SELECT @deletedUserId = user_Person_id, @deletedHealthId = health_condition_id FROM deleted;

	UPDATE dbo.User_health
	SET is_actual = 0
	WHERE user_Person_id = @deletedUserId
	AND health_condition_id = @deletedHealthId;
GO