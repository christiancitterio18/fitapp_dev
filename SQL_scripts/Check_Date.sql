CREATE OR ALTER FUNCTION date_check(@user_id INT, @date_from DATE, @date_to DATE, @table NVARCHAR(20))
RETURNS SMALLINT
AS
	BEGIN
		DECLARE @True_False SMALLINT;
		DECLAre @from DATE;
		DECLARE @to DATE;

		SET @True_False = 0;

		IF(@table = 'Plan_diet')
			BEGIN
				DECLARE dietPlan_cursor CURSOR FOR 
				SELECT date_from, date_to 
				FROM dbo.Plan_diet
				WHERE app_user_id = @user_id;
				
				OPEN dietPlan_cursor;
				FETCH NEXT FROM dietPlan_cursor INTO @from, @to;

				WHILE @@FETCH_STATUS = 0
					BEGIN
						IF((@date_to BETWEEN @from AND @to) OR
							(@date_from BETWEEN @from AND @to))
							BEGIN
								SET @True_False = 1;
							END;
						FETCH NEXT FROM dietPlan_cursor INTO @from, @to;
					END;

				CLOSE dietPlan_Cursor;
				DEALLOCATE dietPlan_Cursor;
			END;

		IF(@table = 'Plan_training')
			BEGIN
				DECLARE trainingPlan_cursor CURSOR FOR 
				SELECT date_from, date_to 
				FROM dbo.Plan_training
				WHERE app_user_id = @user_id;
				
				OPEN trainingPlan_cursor;
				FETCH NEXT FROM trainingPlan_cursor INTO @from, @to;

				WHILE @@FETCH_STATUS = 0
					BEGIN
						IF((@date_to BETWEEN @from AND @to) OR
							(@date_from BETWEEN @from AND @to))
							BEGIN
								SET @True_False = 1;
							END;
						FETCH NEXT FROM trainingPlan_cursor INTO @from, @to;
					END;

				CLOSE dietPlan_Cursor;
				DEALLOCATE dietPlan_Cursor;
			END;

			RETURN @True_false;
	END;
GO