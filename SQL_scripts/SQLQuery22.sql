USE [FITAPP_DEV]
GO
/****** Object:  Trigger [dbo].[user_contusion_actual]    Script Date: 26/04/2021 22:57:33 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   TRIGGER [dbo].[user_contusion_actual] ON [dbo].[User_contusion]
INSTEAD OF INSERT, UPDATE
AS
	DECLARE @actual INT;
	DECLARE @date DATE;

	SELECT @date = contusion_date FROM inserted;

	IF (@date >= DATEADD(DAY, -10, CONVERT(date, GETDATE())))
		BEGIN
			SET @actual = 1;
		END

	IF EXISTS (SELECT  a.contusion_id, a.user_Person_id, a.body_part_id 
				FROM User_contusion a, inserted b 
				WHERE a.contusion_id = b.contusion_id 
				AND a.user_Person_id = b.user_Person_id
				AND a.body_part_Id = b.body_part_Id)
		BEGIN
			UPDATE dbo.User_contusion
			SET contusion_date = inserted.contusion_date, 
			description = inserted.description, 
			is_actual = @actual
			FROM inserted
			WHERE User_contusion.contusion_id = inserted.contusion_id
			AND User_contusion.user_person_id = inserted.user_Person_id
			AND User_contusion.body_part_id = inserted.body_part_Id 
		END
	ELSE
		BEGIN
			
			INSERT INTO dbo.User_contusion
			SELECT contusion_id, user_Person_id, body_part_Id, contusion_date, description, @actual
			FROM inserted
		END