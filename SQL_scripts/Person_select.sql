CREATE OR ALTER PROCEDURE Person_select
@id INT
AS
	SELECT id, name, surname, birth_date, sex, dbo.decrypt(email) as email, dbo.decrypt(phone) as phone
	FROM dbo.Person
	WHERE id = @id;
go