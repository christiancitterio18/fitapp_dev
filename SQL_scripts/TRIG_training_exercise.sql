CREATE OR ALTER TRIGGER training_exercise_trig ON dbo.Training_exercise
AFTER INSERT, UPDATE
AS
	DECLARE @duration INT;

	SELECT @duration = dbo.calculate_duration(training_id, 'Training') FROM inserted;

	UPDATE Training
	SET duration = @duration
	FROM inserted i
	WHERE id = i.training_id;
GO