CREATE OR ALTER PROCEDURE Diet_select
@person_id INT
AS

	SELECT *
	FROM dbo.Diet
	WHERE id NOT IN (SELECT  a.diet_id
			FROM Diet_meal a
			JOIN Ingredient_meal b ON b.meal_id = a.meal_id
			WHERE b.ingredient_id in (SELECT ingredient_id FROM dbo.User_allergy 
			WHERE user_Person_id = @person_id))
go