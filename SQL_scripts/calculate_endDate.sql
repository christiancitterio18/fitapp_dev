CREATE OR ALTER FUNCTION calculate_endDate(@id INT, @date_from DATE, @table nvarchar(20)) 
RETURNS DATE
AS
	BEGIN 
		DECLARE @date_to DATE;
		DECLARE @interval INT;

		If (@table = 'Plan_diet')
			BEGIN
				SELECT @interval = duration 
				FROM Diet 
				WHERE id = @id;
				SET @date_to = DATEADD(DAY, @interval, @date_from);

			END

		IF (@table = 'Plan_training')
			BEGIN
				
				SELECT @interval = duration 
				FROM Training 
				WHERE id = @id;
				SET @date_to = DATEADD(DAY, @interval, @date_from);

			END
		RETURN @date_to;
	END;
GO
