CREATE OR ALTER TRIGGER trig_person
ON dbo.Person
INSTEAD OF INSERT, UPDATE
AS
	DECLARE @birth_date DATE;

	SELECT @birth_date = birth_date FROM inserted;

	IF (@birth_date >= GETDATE())
		THROW 5100, 'Birth_date can not be setted to today or after!', 1;
	ELSE
		INSERT INTO Person 
		SELECT name, surname, birth_date, sex, dbo.encrypt(email), dbo.encrypt(phone)
		FROM inserted;
GO