USE [FITAPP_DEV]
GO

/****** Object:  UserDefinedFunction [dbo].[calories_ingredient]    Script Date: 18/04/2021 19:25:54 ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE   FUNCTION [dbo].[calories_ingredient]( @ingredientId as int)
RETURNS int
AS
BEGIN 
	DECLARE @result as int;
	DECLARE @Add as int;

	SET @result = 0;

	DECLARE Ingredient_Cursor CURSOR FOR
		SELECT  @result + cast((cast(a.grams as numeric) * b.calories_gram) as int)
		FROM dbo.Ingredient_macroelement a
		JOIN dbo.Macroelement b on a.macroelement_id = b.id
		WHERE a.ingredient_id = @ingredientId;

	OPEN Ingredient_Cursor;

	FETCH NEXT FROM Ingredient_Cursor INTO @Add;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @result = @Add;
		FETCH NEXT FROM Ingredient_Cursor INTO @Add;
	END;

	Close Ingredient_Cursor;
	Deallocate Ingredient_Cursor;

	RETURN @result;
END; 
GO


