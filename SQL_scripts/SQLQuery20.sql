USE [FITAPP_DEV]
GO
/****** Object:  Trigger [dbo].[plan_diet_trig]    Script Date: 26/04/2021 22:56:14 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
ALTER   TRIGGER [dbo].[plan_diet_trig] ON [dbo].[Plan_diet]
INSTEAD OF INSERT, UPDATE
AS
	BEGIN
		DECLARE @date_to DATE, @date_from DATE;
		DECLARE @id INT, @plan INT;
		DECLARE @bool SMALLINT;

		SELECT @date_to = dbo.calculate_endDate(diet_id, date_from, 'Plan_diet'), @date_from = date_from, @id = app_user_id, @plan = id 
		FROM inserted;

		SET @bool = dbo.date_check(@id, @date_from, @date_to, 'Plan_diet');

		IF(@date_to > @date_from)
			BEGIN
				IF EXISTS (SELECT * FROM dbo.Plan_diet WHERE id = @plan)
					BEGIN
						UPDATE Plan_diet
						SET diet_id = i.diet_id
						, app_user_id = i.app_user_id
						, date_from = i.date_from
						, date_to = @date_to
						, last_updated = GETDATE()
						FROM inserted i
						WHERE Plan_diet.id = i.id;
					END
				ELSE 
					BEGIN
						IF (@date_from >= CONVERT(date, GETDATE()))
							BEGIN
								IF (@bool = 0)
									BEGIN
										INSERT INTO Plan_diet SELECT diet_id, app_user_id, date_From, @date_to, 0, GETDATE() FROM inserted;
									END
								ELSE
									BEGIN
										RAISERROR(15005, 1, 2, 'The plan is colliding with another plan');
									END
							END
						ELSE
							BEGIN
								RAISERROR(15006, 1, 3, 'start date cannot be past,  it should be set to today or more');
							END
					END
			END
		ELSE
			BEGIN
				RAISERROR(15007, 1, 4, 'date from should be grather than date to');
			END
	END;
