CREATE OR ALTER TRIGGER dbo.contusion_delete ON dbo.User_contusion
INSTEAD OF DELETE
AS
	DECLARE @deletedUserId INT;
	DECLARE @deletedContusionId INT;


	SELECT @deletedUserId = user_person_id, @deletedContusionId = contusion_id FROM deleted;

	UPDATE dbo.User_contusion
	SET is_actual = 0
	WHERE user_person_id = @deletedUserId
	AND contusion_id = @deletedContusionId;
GO