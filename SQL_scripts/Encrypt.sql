CREATE OR ALTER function encrypt
(@tekst as nvarchar(max))
RETURNS nvarchar(max)
AS 
BEGIN
	Declare @encrypted_tekst varbinary(max);
	DECLARE @pass nvarchar(32);

	SET @pass = (SELECT TOP 1 pass FROM Archived.Pass ORDER BY id DESC);
	SET @encrypted_tekst = ENCRYPTBYPASSPHRASE(@pass, @tekst);

	RETURN CONVERT(nvarchar(max), @encrypted_tekst);
END;
GO