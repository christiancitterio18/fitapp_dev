CREATE OR ALTER TRIGGER user_contusion_actual ON dbo.User_contusion
INSTEAD OF INSERT, UPDATE
AS
	DECLARE @actual INT;
	DECLARE @date DATE;

	SELECT @date = contusion_date FROM inserted;

	IF (@date >= DATEADD(DAY, -30, GETDATE()))
		SET @actual = 0;
	ELSE 
		SET @actual = 1;

	INSERT INTO dbo.User_contusion
	SELECT contusion_id, user_person_id, body_part_id, contusion_date, description, is_actual
	FROM inserted
GO