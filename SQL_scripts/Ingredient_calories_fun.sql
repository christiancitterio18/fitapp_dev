CREATE OR ALTER FUNCTION calories_ingredient( @ingredientId as int)
RETURNS int
AS
BEGIN 
	DECLARE @result as int;
	DECLARE @Add as int;

	SET @result = 0;

	DECLARE Ingredient_Cursor CURSOR FOR
		SELECT  @result + (PARSE(a.grams as int) * b.calories_gram)
		FROM dbo.Ingredient_macroelement a
		JOIN dbo.Macroelement b on a.macroelement_id = b.id
		WHERE a.ingredient_id = @ingredientId;

	OPEN Ingredient_Cursor;

	FETCH NEXT FROM Ingredient_Cursor INTO @Add;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @result = @Add;
		FETCH NEXT FROM Ingredient_Cursor INTO @Add;
	END;

	Close Ingredient_Cursor;
	Deallocate Ingredient_Cursor;

	RETURN @result;
END; 
GO