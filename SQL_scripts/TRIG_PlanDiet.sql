CREATE OR ALTER TRIGGER plan_diet_trig ON dbo.Plan_diet
INSTEAD OF INSERT, UPDATE
AS
	BEGIN
		DECLARE @date_to DATE, @date_from DATE;
		DECLARE @id INT, @plan INT;
		DECLARE @bool SMALLINT;

		SELECT @date_to = dbo.calculate_endDate(diet_id, date_to, 'Plan_diet'), @date_from = date_from, @id = app_user_id, @plan = id 
		FROM inserted;

		SET @bool = dbo.date_check(@id, @date_from, @date_to, 'Plan_diet');

		IF(@date_from >= GETDATE() AND @date_to > @date_from)
			BEGIN
				IF EXISTS (SELECT * FROM dbo.Plan_diet WHERE id = @plan)
					BEGIN
						IF(@bool = 0)
							BEGIN
								UPDATE Plan_diet
								SET diet_id = i.diet_id
								, app_user_id = i.app_user_id
								, date_from = i.date_from
								, date_to = @date_to
								, last_updated = GETDATE()
								FROM inserted i;
							END
						ELSE
							BEGIN
								RAISERROR(50005, 10, 1);
							END
					END
				ELSE 
					BEGIN
						IF (@bool = 0)
							BEGIN
								INSERT INTO Plan_diet SELECT diet_id, app_user_id, date_From, @date_to, 0, GETDATE() FROM inserted;
							END
						ELSE
							BEGIN
								RAISERROR(50005, 10, 1);
							END
					END
			END
		ELSE
			BEGIN
				RAISERROR(500051, 10,1);
			END
	END;
GO