CREATE OR ALTER TRIGGER dbo.diet_delete ON dbo.Plan_diet
INSTEAD OF DELETE
AS
	DECLARE @deletedId INT;

	SELECT @deletedId = id FROM deleted;

	UPDATE dbo.Plan_diet
	SET is_deleted = 1, last_updated = GETDATE()
	WHERE id = @deletedId;
GO