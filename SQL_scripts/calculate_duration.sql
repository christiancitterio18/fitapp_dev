CREATE OR ALTER FUNCTION calculate_duration (@id as INT, @table nvarchar(15))
RETURNS INT
AS 
	BEGIN 
		DECLARE @duration INT;
		
		IF (@table = 'Diet')
			BEGIN
				SELECT @duration = MAX(day) FROM dbo.Diet_meal
				WHERE diet_id = @id;
			END

		IF (@table = 'Training')
			BEGIN
				SELECT @duration = MAX(day) FROM dbo.Training_exercise
				WHERE training_id = @id;
			END
	
		RETURN @duration;
	END;
GO