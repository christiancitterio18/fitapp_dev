CREATE OR ALTER TRIGGER dbo.training_delete ON dbo.Plan_training
INSTEAD OF DELETE
AS
	DECLARE @deletedId INT;

	SELECT @deletedId = id FROM deleted;

	UPDATE dbo.Plan_Training
	SET is_deleted = 1, last_updated = GETDATE()
	WHERE id = @deletedId;
GO