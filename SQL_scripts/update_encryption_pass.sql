CREATE OR ALTER PROCEDURE encryption_pass_change
AS
	DECLARE @pass nvarchar(32);

	EXEC dbo.GenerateRandomString @pass OUTPUT;

	SELECT id, dbo.decrypt(email) as emails, dbo.decrypt(phone) as phones
	INTO dbo.#decrypted
	FROM dbo.Person;
	
	INSERT INTO archived.Passwords values (@pass);

	UPDATE dbo.Person
	SET email = a.emails,
	phone = a.phones
	FROM  dbo.#decrypted as a
	WHERE a.id = Person.id;
	
GO
