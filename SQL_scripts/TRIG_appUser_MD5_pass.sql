
CREATE OR ALTER   TRIGGER [dbo].[user_pass_hash] ON [dbo].[App_user]
INSTEAD OF INSERT, UPDATE
AS
	DECLARE @hash nvarchar(32);
	DECLARE @id int;
	DECLARE @pass nvarchar(32);

	SELECT @id = person_id, @hash = password FROM inserted;
	SELECT @pass = password FROM App_user;
	  
	if exists(select person_id from App_user where person_id = @id)
		BEGIN
			if (@hash = @pass) 
				BEGIN
					Update dbo.App_user SET
						person_id = inserted.person_id,
						username = inserted.username,
						password = @hash,
						authorizations_id = inserted.authorizations_id
						FROM inserted
						Where App_user.person_id = @id;
				END
			ELSE
				BEGIN
	
					Update dbo.App_user SET
						person_id = inserted.person_id,
						username = inserted.username,
						password = CONVERT(nvarchar(32), HASHBYTES('MD5', inserted.password),2),
						authorizations_id = inserted.authorizations_id
						FROM inserted
						Where App_user.person_id = @id;
				END
		END
	ELSE
		BEGIN

		SELECT @id = Person.id, @hash = CONVERT(nvarchar(32), HASHBYTES('MD5', inserted.password),2) 
		FROM dbo.Person, inserted Where dbo.decrypt(Person.email) = inserted.username;  
			
			INSERT INTO dbo.App_user
			SELECT @id, username, @hash, authorizations_id FROM inserted;
		END



INSERT INTO Person values ('test','testowe','2020-09-10','M','citty_92@hotmail.it','881047518');
INSERT INTO App_user values (0,'citty_92@hotmail.it','pass123',1);