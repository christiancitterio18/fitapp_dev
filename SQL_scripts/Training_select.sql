CREATE OR ALTER PROCEDURE Training_select
@person_id INT
AS

	SELECT *
	FROM dbo.Training
	WHERE id NOT IN (SELECT  a.training_id
			FROM Training_exercise a
			JOIN BodyPart_exercise b ON b.Exercise_id = a.exercise_id
			WHERE b.Body_part_Id in (SELECT body_part_id FROM dbo.User_contusion 
			WHERE user_Person_id = @person_id AND is_actual = 1))
go