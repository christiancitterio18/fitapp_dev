CREATE OR ALTER function decrypt
(@tekst as nvarchar(max))
RETURNS nvarchar(max)
AS 
BEGIN
	Declare @decrypted_tekst nvarchar(max);
	DECLARE @pass nvarchar(32);

	SET @pass =  (SELECT TOP 1 pass FROM Archived.Pass ORDER BY id DESC);
	SET @decrypted_tekst = DECRYPTBYPASSPHRASE(@pass, CONVERT(varbinary(max),@tekst));

	RETURN @decrypted_tekst;
END;
GO