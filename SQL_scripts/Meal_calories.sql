CREATE OR ALTER FUNCTION meal_calories(@mealId as int)
RETURNS int
AS
BEGIN 
	DECLARE @result as int;
	DECLARE @Add as int;

	SET @result = 0;

	DECLARE Meal_Cursor CURSOR FOR
		SELECT  @result + (PARSE(a.grams as int) * dbo.calories_ingredient(b.id))
		FROM dbo.Ingredient_Meal a
		JOIN dbo.Ingredient b on a.ingredient_id = b.id
		WHERE a.meal_id = @mealId;

	OPEN Meal_Cursor;

	FETCH NEXT FROM Meal_Cursor INTO @Add;

	WHILE @@FETCH_STATUS = 0
	BEGIN
		SELECT @result = @Add;
		FETCH NEXT FROM Meal_Cursor INTO @Add;
	END;

	Close Meal_Cursor;
	Deallocate Meal_Cursor;

	RETURN @result;
END; 
GO
