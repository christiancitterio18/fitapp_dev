# FitApp [![pipeline status](https://gitlab.com/christiancitterio18/fitapp_dev/badges/master/pipeline.svg)](https://gitlab.com/christiancitterio18/fitapp_dev/-/commits/master)  [![coverage report](https://gitlab.com/christiancitterio18/fitapp_dev/badges/master/coverage.svg)](https://gitlab.com/christiancitterio18/fitapp_dev/-/commits/master)

FitApp is a school project to be presented for the final thesis of the engineering degree.
For more information about the project please read the [documentation](https://gitlab.com/christiancitterio18/fitapp_dev/-/raw/master/Project_documentation.docx)

## Hosting

FitApp is currently accessible at the following links:

- http://54.38.52.4/:4200/ Front End of the application
- http://54.38.52.4/:53431/ URL addressing the application api

## Install

### Need to have

To run the application in debug mode you need to have installed

- [Visual Studio 2019](https://visualstudio.microsoft.com/it/downloads/), with tools for ASP.NET and NET.Framework application development
- [Node.js](https://nodejs.org/en/download/)
- Angular
- [Git](https://git-scm.com/downloads) _(Not required)_

to install angular enter the following command in the console **after installing node.js**

`npm install -g @angular/cli`

to download the repository, enter the following command in the console

`git clone https://gitlab.com/christiancitterio18/fitapp_dev`

or download directly from the project [home page](https://gitlab.com/christiancitterio18/fitapp_dev).

### Run

The back-end of the application can be started via Viaual Studio in debug mode or using the command below on the root path of the application:

`dotnet run`

To start the front-end of the application open the UI\FitAppDev path using the command:

```
#Change %APP_PATH% to the path where the application is located on your device
cd %APP_PATH%/UI/FitAppDev
```

and then enter the following command:

`ng serve --open`
