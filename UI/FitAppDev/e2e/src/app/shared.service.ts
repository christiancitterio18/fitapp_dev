import { Injectable } from '@angular/core';
import { HttpClient} from '@angular/common/http';
import { Observable} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class SharedService {
  readonly APIUrl = "http://localhost:53431/api";

  constructor( private http:HttpClient) { }

  getDietCategoryList():Observable<any[]>{

    return this.http.get<any>(this.APIUrl+'/Diet_Category');
  }

  addDietCategory(val:any){
    return this.http.post(this.APIUrl+'/Diet_Category',val);
    
  }
  updateDietCategory(val:any){
    return this.http.put(this.APIUrl+'/Diet_Category',val);

  }

  deleteDietCategory(val:any){
    return this.http.delete(this.APIUrl+'/Diet_Category/'+val);

  }
}
