import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DietCategoryComponent } from './diet-category/diet-category.component';
import { ShowDietCategoryComponent } from './diet-category/show-diet-category/show-diet-category.component';
import { AddEditDietCategoryComponent } from './diet-category/add-edit-diet-category/add-edit-diet-category.component';

import { SharedService } from './shared.service';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    DietCategoryComponent,
    ShowDietCategoryComponent,
    AddEditDietCategoryComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule
  ],
  providers: [SharedService],
  bootstrap: [AppComponent]
})
export class AppModule { }
