import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditDietCategoryComponent } from './add-edit-diet-category.component';

describe('AddEditDietCategoryComponent', () => {
  let component: AddEditDietCategoryComponent;
  let fixture: ComponentFixture<AddEditDietCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditDietCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDietCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
