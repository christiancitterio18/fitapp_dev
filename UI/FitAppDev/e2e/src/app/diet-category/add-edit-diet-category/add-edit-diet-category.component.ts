import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-diet-category',
  templateUrl: './add-edit-diet-category.component.html',
  styleUrls: ['./add-edit-diet-category.component.css']
})
export class AddEditDietCategoryComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() dietCat:any;
  id!: string;
  category!:string;

  ngOnInit(): void {
    this.id=this.dietCat.id;
    this.category = this.category;
  }

  addDietCategory(){
    var val = {id:this.id,
      category:this.category};
    this.service.AddDietCategory(val).subscribe(res=>{
      alert(res.toString());
    });
  }

  updateDietCategory(){
    var val = {id:this.id,
      category:this.category};
this.service.UpdateDietCategory(val).subscribe(res=>{
alert(res.toString());
});
  }

}
