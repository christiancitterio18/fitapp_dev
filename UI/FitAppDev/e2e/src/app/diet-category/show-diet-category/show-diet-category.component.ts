import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-diet-category',
  templateUrl: './show-diet-category.component.html',
  styleUrls: ['./show-diet-category.component.css']
})
export class ShowDietCategoryComponent implements OnInit {

  constructor(private service:SharedService) { }

  DietCategoryList:any=[];
  ModalTitle!:string
  ActivateAddEditDietCatComp:boolean=false;
  dietCat:any;

  ngOnInit(): void {
    this.refreshDietCategoryList();
  }

  addClick(){
    this.dietCat={
      id:0,
      category:""
    }
    this.ModalTitle="Add Diet Category";
    this.ActivateAddEditDietCatComp=true;
  }

  editClick(item: any){
    this.dietCat=item;
    this.ModalTitle="Edit Diet Category";
    this.ActivateAddEditDietCatComp=true;
  }

  deleteClick(item: { id: any; }){
    if(confirm('Are you sure?')){
        this.service.DeleteDietCategory(item.id).subscribe(data=>{
          alert(data.toString());
          this.refreshDietCategoryList();
        })
    }
    
    
  }

  closeClick(){
    this.ActivateAddEditDietCatComp=false;
    this.refreshDietCategoryList();
  }

  refreshDietCategoryList(){
    this.service.GetDietCategoryList().subscribe(data=>{
      this.DietCategoryList=data;
    });
  }

}
