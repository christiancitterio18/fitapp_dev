import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DietCategoryComponent } from './diet-category/diet-category.component';

const routes: Routes = [
  {path:'Diet_Category', component:DietCategoryComponent}
  ];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
