import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-authorization',
  templateUrl: './add-authorization.component.html',
  styleUrls: ['./add-authorization.component.css']
})
export class AddAuthorizationComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Authorization: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();
  
  id: string = '';
  authorization: string = '';

  authorizationErrorList: any = [];

  ngOnInit(): void {
    this.id = this.Authorization.id;
    this.authorization = this.Authorization.authorization;
  }

  AddAuth(){
    var data = {
      id: this.id,
      authorization: this.authorization
    };
          
    this.service.PostAuthorization(data).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.authorizationErrorList = err.error.errors.authorization;  
        } else {
          alert('Unknown Server Error!');
        }
      }
    );
  }

  isError() {
    return (this.authorizationErrorList.length > 0);
  }
}
