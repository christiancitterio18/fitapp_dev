import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowAuthorizationComponent } from './show-authorization.component';

describe('ShowAuthorizationComponent', () => {
  let component: ShowAuthorizationComponent;
  let fixture: ComponentFixture<ShowAuthorizationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowAuthorizationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowAuthorizationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
