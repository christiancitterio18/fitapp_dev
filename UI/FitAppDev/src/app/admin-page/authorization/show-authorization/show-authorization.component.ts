import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-authorization',
  templateUrl: './show-authorization.component.html',
  styleUrls: ['./show-authorization.component.css']
})
export class ShowAuthorizationComponent implements OnInit {

  constructor(private service: SharedService) { }

  authorizationsList: any = [];

  modalTitle: string = '';
  activateAddEditAuthorization: Boolean = false;
  Authorization: any = {};


  ngOnInit(): void {
    this.RefreshAuthList();
  }

  // tslint:disable-next-line: typedef
  AddClick(){
    this.Authorization = {
      id: 0,
      authorization: ''
    };
    this.modalTitle = 'Add Authorization';
    this.activateAddEditAuthorization = true;
  }

  // tslint:disable-next-line: typedef
  CloseClick(){
    this.activateAddEditAuthorization = false;
    this.RefreshAuthList();
  }

  // tslint:disable-next-line: typedef
  RefreshAuthList() {
    this.service.GetAuthorizationList().subscribe(
      (data: any) => {
        this.authorizationsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  SuccessForm(tf: boolean) {
    document.getElementById("dismissModalAuth")?.click();
  }
}
