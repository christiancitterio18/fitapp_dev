import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-macroelement',
  templateUrl: './add-edit-macroelement.component.html',
  styleUrls: ['./add-edit-macroelement.component.css']
})
export class AddEditMacroelementComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Macroelement: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  macroelement: string = '';
  calories_gram: string = '';

  macroelementErrors = {
    macroelement: [],
    calories_gram: []
  }

  ngOnInit(): void {
  this.id = this.Macroelement.id;
  this.macroelement = this.Macroelement.macroelement;
  this.calories_gram = this.Macroelement.calories_gram;
  }

  AddMacroelement(){
    var val = {id: this.id,
      macroelement: this.macroelement,
      calories_gram: this.calories_gram};           
    this.service.AddMacroelement(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.macroelement) {
            this.macroelementErrors.macroelement = err.error.errors.macroelement;
          }
          if (err.error.errors.calories_gram) {
            this.macroelementErrors.calories_gram = err.error.errors.calories_gram;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateMacroelement(){
    var val = {id: this.id,
      macroelement: this.macroelement,
      calories_gram: this.calories_gram};
    this.service.UpdateMacroelement(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {

          if (err.error.errors.macroelement) {
            this.macroelementErrors.macroelement = err.error.errors.ingredient;
          }

          if (err.error.errors.calories_gram) {
            this.macroelementErrors.calories_gram = err.error.errors.calories_gram;
          }
          
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}

