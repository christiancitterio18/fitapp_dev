import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditMacroelementComponent } from './add-edit-macroelement.component';

describe('AddEditMacroelementComponent', () => {
  let component: AddEditMacroelementComponent;
  let fixture: ComponentFixture<AddEditMacroelementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditMacroelementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditMacroelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
