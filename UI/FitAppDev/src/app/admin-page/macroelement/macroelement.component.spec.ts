import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MacroelementComponent } from './macroelement.component';

describe('MacroelementComponent', () => {
  let component: MacroelementComponent;
  let fixture: ComponentFixture<MacroelementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MacroelementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MacroelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
