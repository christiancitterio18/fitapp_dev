import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-macroelement',
  templateUrl: './show-macroelement.component.html',
  styleUrls: ['./show-macroelement.component.css']
})
export class ShowMacroelementComponent implements OnInit {

  constructor(private service: SharedService) { }

  macroelementsList: any = [];
  macroelementsListWithoutFilter:any=[];

  modalTitle: string = '';
  sctivateAddEditMacroelement: boolean = false;
  Macroelement: any;
  
  macroelementFilter:string="";


  ngOnInit(): void {
    this.RefresMacroelementList();
  }

  AddClick(){
    this.Macroelement = {
      id: 0,
      macroelement: '',
      calories_gram: 0
    };
    this.modalTitle = 'Add Macroelement';
    this.sctivateAddEditMacroelement = true;
  }

  EditClick(item: any){
    this.Macroelement = item;
    this.modalTitle = 'Edit Macroelement';
    this.sctivateAddEditMacroelement = true;
  }

  CloseClick(){
    this.sctivateAddEditMacroelement = false;
    this.RefresMacroelementList();
  }

  RefresMacroelementList() {
    this.service.GetMacroelementList().subscribe(data => {
      this.macroelementsList = data;
      this.macroelementsListWithoutFilter = data;
    });
  }

  DeleteClick(item: any){
    let val = {
      id: item.id,
      microelement: item.microelement
    };
      
    if (confirm('Are you sure??')){
      this.service.DeleteMacroelement(val).subscribe(data => {
        alert(data.toString());
        this.RefresMacroelementList();
      });
    }
  }

  FilterFn(){

    var macroelementFilter = this.macroelementFilter;
    
    this.macroelementsList = this.macroelementsListWithoutFilter.filter((el:any) =>{
        return el.macroelement.toString().toLowerCase().includes(
          macroelementFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: any) {
    document.getElementById('modalDismissMacroelement')?.click();
  }
  

}
