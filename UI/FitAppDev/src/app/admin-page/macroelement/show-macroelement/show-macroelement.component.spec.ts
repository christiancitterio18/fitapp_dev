import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMacroelementComponent } from './show-macroelement.component';

describe('ShowMacroelementComponent', () => {
  let component: ShowMacroelementComponent;
  let fixture: ComponentFixture<ShowMacroelementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowMacroelementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMacroelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
