import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTrainingExerciseComponent } from './show-training-exercise.component';

describe('ShowTrainingExerciseComponent', () => {
  let component: ShowTrainingExerciseComponent;
  let fixture: ComponentFixture<ShowTrainingExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowTrainingExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTrainingExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
