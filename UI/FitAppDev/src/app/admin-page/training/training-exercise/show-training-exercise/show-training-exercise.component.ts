import { isNgTemplate } from '@angular/compiler';
import { Component, OnInit, Input } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-training-exercise',
  templateUrl: './show-training-exercise.component.html',
  styleUrls: ['./show-training-exercise.component.css']
})
export class ShowTrainingExerciseComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() id!: string;
  
  trainingId: string = '';

  trainingExercisesList: any = [];
  exercisesList: any = [];

  modalTitle: string = '';
  activateAddEditTrainingExercise: boolean = false;
  trainingExercise: any;

  ngOnInit(): void {
    this.trainingId = this.id;
    this.RefreshTrainingExerciseList();
  }

  AddClick(){
    this.trainingExercise = {
      training_id: this.trainingId,
      exercise_id: '',
      day: '',
      series_number: '',
      repetition: ''
    };
    this.modalTitle = 'Add exercise';
    this.activateAddEditTrainingExercise = true;
  }

  EditClick(item: any){
    this.trainingExercise = item;
    this.modalTitle = 'Edit exercise';
    this.activateAddEditTrainingExercise = true;
  }

  CloseClick(){
    this.activateAddEditTrainingExercise = false;
    this.RefreshTrainingExerciseList();
  }

  RefreshTrainingExerciseList() {
    var dataList: any = [];
    this.service.GetExerciseList().subscribe(
      (data: any) => {
        this.exercisesList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    this.service.GetTrainingExerciseList().subscribe(
      (res: any) => {
        dataList = res;
        this.trainingExercisesList = dataList.filter(
          (element: any) => {
            return element.training_id == this.trainingId;
          }
        ).sort(
          (a: any, b: any) => {
            return (a.day > b.day) ? 1 : ((a.day === b.day) ? 0 : -1);
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
    
  }
  
  DeleteClick(item: any) {
    let val = {
      training_id: item.training_id,
      exercise_id: item.exercise_id,
      day: item.day,
      series_number: item.series_number,
      repetition: item.repetition
    };        
    if (confirm('Are you sure??')) {
      this.service.DeleteTrainingExercise(val).subscribe(
        (data: any) => {
          if(data == 'Success'){
            alert('Deleted');
            this.RefreshTrainingExerciseList();
          } else {
            alert('Something went wrong, please try again later!'); 
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  getExerciseName(item: any): string {
    return this.exercisesList.find((element: any) => {
      return element.id == item.exercise_id;
    }).exercise;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissTrainingExercise')?.click();
  }
}
