import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TrainingExerciseComponent } from './training-exercise.component';

describe('TrainingexerciseComponent', () => {
  let component: TrainingExerciseComponent;
  let fixture: ComponentFixture<TrainingExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TrainingExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TrainingExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
