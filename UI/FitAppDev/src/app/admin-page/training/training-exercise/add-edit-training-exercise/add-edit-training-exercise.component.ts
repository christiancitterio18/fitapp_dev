import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-training-exercise',
  templateUrl: './add-edit-training-exercise.component.html',
  styleUrls: ['./add-edit-training-exercise.component.css']
})
export class AddEditTrainingExerciseComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() trainingExercise: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  training_id: string = '';
  exercise_id: string = '';
  day: string = '';
  series_number: string = '';
  repetition: string = '';

  exercisesList: any = [];

  trainingExcerciseErrors: any = {
    exercise_id: new Array(),
    day: new Array(),
    series_number: new Array(),
    repetition: new Array()
  }

  ngOnInit(): void {
    this.GetExerciseList();
    this.training_id = this.trainingExercise.training_id;
    this.exercise_id = this.trainingExercise.exercise_id;
    this.day = this.trainingExercise.day;
    this.series_number = this.trainingExercise.series_number;
    this.repetition = this.trainingExercise.repetition;
  }
  
  AddTrainingExercise(){
    let val = {
      training_id: this.training_id,
      exercise_id: this.exercise_id,
      day: this.day,
      series_number: this.series_number,
      repetition: this.repetition,
    };
    this.service.AddTrainingExercise(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.exercise_id) {
            this.trainingExcerciseErrors.exercise_id.push('Please select a valid option!');
          }
          if (err.error.errors.day) {
            this.trainingExcerciseErrors.day.push('Please enter a positive and greather than 0 whole number!');
          }
          if (err.error.errors.series_number) {
            this.trainingExcerciseErrors.series_number.push('Please enter a positive and greather than 0 whole number!');
          }
          if (err.error.errors.repetition) {
            this.trainingExcerciseErrors.repetition.push('Please enter a positive and greather than 0 whole number!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateTrainingExercise() {
    let val = {
      training_id: this.training_id,
      exercise_id: this.exercise_id,
      day: this.day,
      series_number: this.series_number,
      repetition: this.repetition
    };
    
    this.service.UpdateTrainingExercise(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.exercise_id) {
            this.trainingExcerciseErrors.exercise_id = [];
            this.trainingExcerciseErrors.exercise_id.push('Please select a valid option!');
          }
          if (err.error.errors.day) {
            this.trainingExcerciseErrors.day = [];
            this.trainingExcerciseErrors.day.push('Please enter a positive and greather than 0 whole number!');
          }
          if (err.error.errors.series_number) {
            this.trainingExcerciseErrors.series_number = [];
            this.trainingExcerciseErrors.series_number.push('Please enter a positive and greather than 0 whole number!');
          }
          if (err.error.errors.repetition) {
            this.trainingExcerciseErrors.repetition = [];
            this.trainingExcerciseErrors.repetition.push('Please enter a positive and greather than 0 whole number!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetExerciseList() {
    this.service.GetExerciseList().subscribe(
      (data: any) => {
        this.exercisesList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }
}
