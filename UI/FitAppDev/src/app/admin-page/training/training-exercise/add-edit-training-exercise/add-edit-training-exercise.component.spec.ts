import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditTrainingExerciseComponent } from './add-edit-training-exercise.component';

describe('AddEditTrainingExerciseComponent', () => {
  let component: AddEditTrainingExerciseComponent;
  let fixture: ComponentFixture<AddEditTrainingExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditTrainingExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditTrainingExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
