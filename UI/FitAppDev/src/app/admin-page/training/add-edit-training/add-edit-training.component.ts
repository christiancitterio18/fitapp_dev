import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-training',
  templateUrl: './add-edit-training.component.html',
  styleUrls: ['./add-edit-training.component.css']
})
export class AddEditTrainingComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Training: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  training_category_id: string = '';
  training: string = '';
  description: string = '';
  duration: string = '';

  categoriesList: any = [];

  trainingErrors: any = {
    training_category_id: new Array(),
    training: [],
    description: []
  }

  ngOnInit(): void {
    this.GetCategoryList();
    this.id = this.Training.id;
    this.training_category_id = this.Training.training_category_id;
    this.training = this.Training.training;
    this.description = this.Training.description;
    this.duration = this.Training.duration;
  }

  AddTraining(){
    let val = {
      id: this.id,
      training_category_id: this.training_category_id,
      training: this.training,
      description: this.description,
      duration: 1
    };

    this.service.AddTraining(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.training_category_id) {
            this.trainingErrors.training_category_id.push('Please select a valid option!');
          }
          if (err.error.errors.training) {
            this.trainingErrors.training = err.error.errors.training;
          }
          if (err.error.errors.description) {
            this.trainingErrors.description = err.error.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }


  UpdateTraining(){
    let val = {
      id: this.id,
      training_category_id: this.training_category_id,
      training: this.training,
      description: this.description,
      duration: this.duration
    };
    
    this.service.UpdateTraining(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.training_category_id) {
            this.trainingErrors.training_category_id = [];
            this.trainingErrors.training_category_id.push('Please select a valid option!');
          }
          if (err.error.errors.training) {
            this.trainingErrors.training = err.error.errors.training;
          }
          if (err.error.errors.description) {
            this.trainingErrors.description = err.error.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetCategoryList() {
    this.service.GetTrainingCategoryList().subscribe(
      (data: any) => {
        this.categoriesList = data
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
