import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-training',
  templateUrl: './show-training.component.html',
  styleUrls: ['./show-training.component.css']
})
export class ShowTrainingComponent implements OnInit {

  constructor(private service: SharedService) { }
    
  trainingsList: any = [];
  categoriesList: any = [];
  trainingsWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditTraining: boolean = false;
  Training: any;

  trainingFilter:string="";

  ngOnInit(): void {
    this.RefreshTrainingList();
  }

  AddClick(){
    this.Training = {
      id: 0,
      training_category_id: '',
      training: '',
      description: '',
      duration: ''
    };
    this.modalTitle = 'Add Training';
    this.activateAddEditTraining = true;
  }


  EditClick(item: any){
    this.Training = item;
    this.modalTitle = 'Edit Training';
    this.activateAddEditTraining = true;
  }


  CloseClick(){
    this.activateAddEditTraining = false;
    this.RefreshTrainingList();
  }

  RefreshTrainingList() {
    this.service.GetTrainingCategoryList().subscribe(
      (data: any) => {
      this.categoriesList = data;
        this.service.GetTrainingList().subscribe(
          (dataTable: any) => {
            this.trainingsList = dataTable;
            this.trainingsWithoutFilter = dataTable;
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(id: any){
    if (confirm('Are you sure??')) {
      this.service.DeleteTraining(id).subscribe(
        (data: any) => {
          if(data == 'Success'){
            alert('Deleted');
            this.RefreshTrainingList();
          } else {
            alert('Something went wrong, please try again later!'); 
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetCategoryName(item: any) {
    return this.categoriesList.find((element: any) => {
      return element.id == item.training_category_id;
    }).category;
  }

  FilterFn(){
    var trainingFilter = this.trainingFilter;
    
    this.trainingsList = this.trainingsWithoutFilter.filter((el: any) =>
    {
        return el.training.toString().toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )||
        this.GetCategoryName(el).toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )||
        el.description.toString().toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )||
        el.duration.toString().toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismisstraining')?.click();
  }
}
