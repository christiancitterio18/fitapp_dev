import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTrainingComponent } from './show-training.component';

describe('ShowTrainingComponent', () => {
  let component: ShowTrainingComponent;
  let fixture: ComponentFixture<ShowTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
