import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPlanTrainingComponent } from './show-plan-training.component';

describe('ShowPlanTrainingComponent', () => {
  let component: ShowPlanTrainingComponent;
  let fixture: ComponentFixture<ShowPlanTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowPlanTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPlanTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
