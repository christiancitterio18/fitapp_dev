import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-plan-training',
  templateUrl: './show-plan-training.component.html',
  styleUrls: ['./show-plan-training.component.css']
})
export class ShowPlanTrainingComponent implements OnInit {

  constructor(private service: SharedService) { }

  
  planTrainingsList: any = [];
  usersList: any = [];
  trainingsList: any = [];
  planTrainingsWithoutFilter:any=[];

  modalTitle: string = '';

  activateAddEditPlanTraining: boolean = false;
  trainingPlan: any;

  trainingPlanFilter:string='';
  


  ngOnInit(): void {
    this.RefreshTrainingPlanList();
  }

  AddClick(){
    this.trainingPlan = {
      id: 0,
      training_id: '',
      app_user_id: '',
      date_from: '',
      date_to: '',
      is_deleted: '',
      last_updated: ''
    };
    this.modalTitle = 'Add Plan Training';
    this.activateAddEditPlanTraining = true;
  }

  EditClick(item: any){
    this.trainingPlan = item;
    this.modalTitle = 'Edit Plan Training';
    this.activateAddEditPlanTraining = true;
  }

  CloseClick(){
    this.activateAddEditPlanTraining = false;
    this.RefreshTrainingPlanList();
  }

  RefreshTrainingPlanList() {
    this.service.GetTrainingList().subscribe(
      (trainingData: any) => {
      this.trainingsList = trainingData;
        this.service.GetUserList().subscribe(
          (userData: any) => {
            this.usersList = userData;
            this.service.GetTrainingPlanList().subscribe(
              (data: any) => {
                this.planTrainingsList = data.filter((el: any) => {
                  return el.is_deleted == 0;
                });
                this.planTrainingsWithoutFilter = data;
              },
              (err: any) => {
                alert('Unknown Server Error');
              }
            );
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){
      
    if (confirm('Are you sure??')){
      this.service.DeleteTrainingPlan(item.id).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert('Deleted');
            this.RefreshTrainingPlanList()
          };
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetUserName(item: any) {
    return this.usersList.find((element: any) => {
      return element.person_id == item.app_user_id;
    }).username;
  }

  GetTrainingName(item: any) {
    return this.trainingsList.find((element: any) => {
      return element.id == item.training_id
    }).training;
  }

  FilterFn(){

    var planFilter = this.trainingPlanFilter;   
    
    this.planTrainingsList = this.planTrainingsWithoutFilter.filter((el:any) => {
        return  this.GetTrainingName(el).toString().toLowerCase().includes(
          planFilter.toString().trim().toLowerCase()
        )||
        this.GetUserName(el).toString().toLowerCase().includes(
          planFilter.toString().trim().toLowerCase()
        )
    });
  }

  GetDate(str: string) {
    let date = new Date(str)
    return date.toDateString();
  }

  SuccessForm(tf: any) {
    document.getElementById('modalDismissPlanTraining')?.click();
  }

}

