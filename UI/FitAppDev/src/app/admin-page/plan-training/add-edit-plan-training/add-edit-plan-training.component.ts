import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-plan-training',
  templateUrl: './add-edit-plan-training.component.html',
  styleUrls: ['./add-edit-plan-training.component.css']
})
export class AddEditPlanTrainingComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() trainingPlan: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  dateNow = new Date();

  id: string = '';
  training_id: string = '';
  app_user_id: string = '';
  date_from: string = '';
  date_to: string = '';
  is_deleted: string = '';
  last_updated: string = '';

  trainingsList: any = [];
  usersList: any = [];

  planTrainingErrors: any = {
    training_id: new Array(),
    app_user_id: new Array(),
    date_from: new Array()
  }

  ngOnInit(): void {
    this.GetTrainingList();
    this.GetUserList();
    this.id = this.trainingPlan.id;
    this.training_id = this.trainingPlan.training_id;
    this.app_user_id = this.trainingPlan.app_user_id;
    this.date_from = this.trainingPlan.date_from;
    this.date_to = this.trainingPlan.date_to;
    this.is_deleted = this.trainingPlan.is_deleted;
    this.last_updated = this.dateNow.toDateString();
  }

  AddTrainingPlan(){

    let val = {id: this.id,
      training_id: this.training_id,
      app_user_id: this.app_user_id,
      date_from: this.date_from,
      date_to: this.GetEndDate(this.training_id),
      is_deleted: 0,
      last_updated: this.last_updated
    };
    
    this.service.PostTrainingPlan(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          if (res) {
            alert(res);
            this.planTrainingErrors.date_from = [];
            this.planTrainingErrors.date_from.push('Please select a valid date!');
          } else {
            alert('Something went wrong adding the user account, check the input data and try again later!');
          }
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.training_id) {
            this.planTrainingErrors.training_id = [];
            this.planTrainingErrors.training_id.push('Please select a valid option!');
          }
          if (err.error.errors.app_user_id) {
            this.planTrainingErrors.app_user_id = [];
            this.planTrainingErrors.app_user_id.push('Please select a valid option!');
          }
          if (err.error.errors.date_from) {
            this.planTrainingErrors.date_from = [];
            this.planTrainingErrors.date_from.push('Please select a valid date!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateTrainingPlan(){

    let val = {
      id: this.id,
      training_id: this.training_id,
      app_user_id: this.app_user_id,
      date_from: this.date_from,
      date_to: this.GetEndDate(this.training_id),
      is_deleted: this.is_deleted,
      last_updated: this.last_updated
    };
    
    this.service.PutTrainingPlan(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          if (res) {
            alert(res);
            this.planTrainingErrors.date_from = [];
            this.planTrainingErrors.date_from.push('Please select a valid date!');
          } else {
            alert('Something went wrong adding the user account, check the input data and try again later!');
          }
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.training_id) {
            this.planTrainingErrors.training_id = [];
            this.planTrainingErrors.training_id.push('Please select a valid option!');
          }
          if (err.error.errors.app_user_id) {
            this.planTrainingErrors.app_user_id = [];
            this.planTrainingErrors.app_user_id.push('Please select a valid option!');
          }
          if (err.error.errors.date_from) {
            this.planTrainingErrors.date_from = [];
            this.planTrainingErrors.date_from.push('Please select a valid date!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetTrainingList() {
    this.service.GetTrainingList().subscribe(
      (trainingData: any) => {
        this.trainingsList = trainingData;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetUserList() {
    this.service.GetUserList().subscribe(
      (userData: any) => {
        this.usersList = userData;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetEndDate(trainingId: any) {
    if (!trainingId) { return; }
    
    let training = this.trainingsList.filter((el: any) => {
      return el.id == trainingId
    });

    let date = new Date(this.date_from);
    this.dateNow.setDate(date + training[0].duration);

    return date;
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}

