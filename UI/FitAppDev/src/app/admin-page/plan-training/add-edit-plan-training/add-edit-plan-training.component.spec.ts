import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPlanTrainingComponent } from './add-edit-plan-training.component';

describe('AddEditPlanTrainingComponent', () => {
  let component: AddEditPlanTrainingComponent;
  let fixture: ComponentFixture<AddEditPlanTrainingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditPlanTrainingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPlanTrainingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
