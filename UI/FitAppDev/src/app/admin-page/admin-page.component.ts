import { Component, OnInit } from '@angular/core';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-admin-page',
  templateUrl: './admin-page.component.html',
  styleUrls: ['./admin-page.component.css']
})
export class AdminPageComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

  CanViewDiets() {
    return (this.auth.IsDietetician() || this.auth.IsAdmin());
  }

  CanViewTrainings(){
    return (this.auth.IsTrainer() || this.auth.IsAdmin());
  }

}
