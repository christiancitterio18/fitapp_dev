import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowPlanDietComponent } from './show-plan-diet.component';

describe('ShowPlanDietComponent', () => {
  let component: ShowPlanDietComponent;
  let fixture: ComponentFixture<ShowPlanDietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowPlanDietComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowPlanDietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
