import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-plan-diet',
  templateUrl: './show-plan-diet.component.html',
  styleUrls: ['./show-plan-diet.component.css']
})
export class ShowPlanDietComponent implements OnInit {

  constructor(private service: SharedService) { }

  
  planDietsList: any = [];
  usersList: any = [];
  dietsList: any = [];
  planDietsWithoutFilter:any=[];

  modalTitle: string = '';

  activateAddEditPlanDiet: boolean = false;
  planDiet: any;

  planDietFilter:string="";
 

  ngOnInit(): void {
    this.RefreshDietPlanList();
  }


  AddClick(){
    this.planDiet = {
      id: 0,
      diet_id: '',
      app_user_id: '',
      date_from: '',
      date_to: '',
      is_deleted: 0,
      last_updated: ''
    };
    this.modalTitle = 'Add diet plan';
    this.activateAddEditPlanDiet = true;
  }

  EditClick(item: any){
    this.planDiet = item;
    this.modalTitle = 'Edit diet plan';
    this.activateAddEditPlanDiet = true;
  }

  CloseClick(){
    this.activateAddEditPlanDiet = false;
    this.RefreshDietPlanList();
  }

  RefreshDietPlanList() {
    this.service.GetDietList().subscribe(
      (dietData: any) => {
        this.dietsList = dietData;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    this.service.GetUserList().subscribe(
      (userData: any) => {
        this.usersList = userData;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    this.service.GetDietPlanList().subscribe(
      (data: any) => {
        this.planDietsList = data.filter((el: any) => {
          return el.is_deleted == 0;
        });
        this.planDietsWithoutFilter = this.planDietsList;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){

    if (confirm('Are you sure??')){
      this.service.DeleteDietPlan(item.id).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert('Deleted');
            this.RefreshDietPlanList()
          };
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetUserName(item: any) {
    return this.usersList.find((element: any) => {
      return element.person_id == item.app_user_id;
    }).username;
  }

  GetDietName(item: any) {
    return this.dietsList.find((element: any) => {
      return element.id == item.diet_id
    }).diet;
  }

  FilterFn(){

    var planFilter = this.planDietFilter;
    
    this.planDietsList = this.planDietsWithoutFilter.filter((el:any) => {
        return this.GetDietName(el).toString().toLowerCase().includes(
          planFilter.toString().trim().toLowerCase()
        )||
        this.GetUserName(el).toString().toLowerCase().includes(
          planFilter.toString().trim().toLowerCase()
        )
    });
  }

  GetDate(str: string) {
    let date = new Date(str)
    return date.toLocaleDateString();
  }

  SuccessForm(tf: any) {
    document.getElementById('modalDismissPlanDiet')?.click();
  }
}
