import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditPlanDietComponent } from './add-edit-plan-diet.component';

describe('AddEditPlanDietComponent', () => {
  let component: AddEditPlanDietComponent;
  let fixture: ComponentFixture<AddEditPlanDietComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditPlanDietComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditPlanDietComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
