import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-health-condition',
  templateUrl: './add-edit-health-condition.component.html',
  styleUrls: ['./add-edit-health-condition.component.css']
})
export class AddEditHealthConditionComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() healthCondition: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  health_condition: string = '';

  healthConditionErrors = [];

  
  ngOnInit(): void {
    this.id = this.healthCondition.id;
    this.health_condition = this.healthCondition.health_condition;
  }

  AddHealthCondition(){
    var val = {id: this.id,
      health_condition: this.health_condition};   
             
    this.service.AddHealthCondition(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.healthConditionErrors = err.error.errors.health_condition;
        } else {
          alert('Unknown Server Error!');
        }
      }
    );
  }

  
  UpdateHealthCondition(){
    var val = {
      id: this.id,
      health_condition: this.health_condition
    };

    this.service.UpdateHealthCondition(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.healthConditionErrors = err.error.errors.health_condition;
        } else {
          alert('Unknown Server Error!');
        }
      }
    );
  }

  isError() {
    return (this.healthConditionErrors.length > 0);
  }
}
