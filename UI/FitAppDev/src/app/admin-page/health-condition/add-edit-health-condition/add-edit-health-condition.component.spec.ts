import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditHealthConditionComponent } from './add-edit-health-condition.component';

describe('AddEditHealthConditionComponent', () => {
  let component: AddEditHealthConditionComponent;
  let fixture: ComponentFixture<AddEditHealthConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditHealthConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditHealthConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
