import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-health-condition',
  templateUrl: './show-health-condition.component.html',
  styleUrls: ['./show-health-condition.component.css']
})
export class ShowHealthConditionComponent implements OnInit {

  constructor(private service: SharedService) { }

  
  healthConditionsList: any = [];
  healthConditionsWithoutFilter: any=[];

  modalTitle: string = '';

  activateAddEditHealthCondition: Boolean = false;
  healthCondition: any;

  healthCategorFilter:string="";
  

  ngOnInit(): void {
    this.RefreshHealthConditionList();
  }

  AddClick(){
    this.healthCondition = {
      id: 0,
      health_condition: ''
    };
    this.modalTitle = 'Add health condition';
    this.activateAddEditHealthCondition = true;
  }

  
  EditClick(item: any){
    this.healthCondition = item;
    this.modalTitle = 'Edit health condition';
    this.activateAddEditHealthCondition = true;
  }

  
  CloseClick(){
    this.healthConditionsList = false;
    this.RefreshHealthConditionList();

    
    var formFields = document.getElementsByName('health-condition-form');

      for (var i = 0; i < formFields.length; i++) {
        formFields[i].setAttribute('style', 'border-color: initial;');
      }
  }

  
  RefreshHealthConditionList() {
    this.service.GethealthconditionList().subscribe(
      (data: any) => {
        this.healthConditionsList = data;
        this.healthConditionsWithoutFilter = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){
    var val = {
      id: item.id,
      health_condition: item.health_condition
    };
      
    if (confirm('Are you sure??')){
      this.service.DeleteHealthCondition(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshHealthConditionList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  FilterFn(){
    var healthCategorFilter = this.healthCategorFilter;
    
    this.healthConditionsList = this.healthConditionsWithoutFilter.filter(function (el: { health_condition: { toString: () => string; }; }){
        return el.health_condition.toString().toLowerCase().includes(
          healthCategorFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: any) {
    document.getElementById('modalDismissHealth')?.click();
  }

}

