import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowHealthConditionComponent } from './show-health-condition.component';

describe('ShowHealthConditionComponent', () => {
  let component: ShowHealthConditionComponent;
  let fixture: ComponentFixture<ShowHealthConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowHealthConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowHealthConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
