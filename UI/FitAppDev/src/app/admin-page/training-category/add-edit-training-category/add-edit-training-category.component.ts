import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-training-category',
  templateUrl: './add-edit-training-category.component.html',
  styleUrls: ['./add-edit-training-category.component.css']
})
export class AddEditTrainingCategoryComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() trainingCategory: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  category: string = '';

  trainingCategoryErrors = []

  ngOnInit(): void {
  this.id = this.trainingCategory.id;
  this.category = this.trainingCategory.category;
  }
  
  AddTrainingCategory(){
    var val = {
      id: this.id,
      category: this.category
    };
               
    this.service.AddTrainingCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.trainingCategoryErrors = err.error.errors.category;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }


  UpdateTrainingCategory(){
    var val = {
      id: this.id,
      category: this.category
    };
    
    this.service.UpdateTrainingCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.trainingCategoryErrors = err.error.errors.category;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  isError() {
    return this.trainingCategoryErrors.length > 0;
  }

}
