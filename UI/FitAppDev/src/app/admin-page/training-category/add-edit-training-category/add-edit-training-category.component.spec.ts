import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditTrainingCategoryComponent } from './add-edit-training-category.component';

describe('AddEditTrenCatComponent', () => {
  let component: AddEditTrainingCategoryComponent;
  let fixture: ComponentFixture<AddEditTrainingCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditTrainingCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditTrainingCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
