import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-training-category',
  templateUrl: './show-training-category.component.html',
  styleUrls: ['./show-training-category.component.css']
})

export class ShowTrainingCategoryComponent implements OnInit {
  constructor(private service: SharedService) { }

  trainingCategoriesList: any = [];
  trainingCategoriesListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditTrainingCategory: Boolean = false;
  trainingCategory: any;

  trainingCategorFilter:string="";

  ngOnInit(): void {
    this.RefreshTainingCategoryList();
    console.log(this.trainingCategoriesList);
  }

  AddClick(){
    this.trainingCategory = {
      id: 0,
      category: ''
    };
    this.modalTitle = 'Add Category';
    this.activateAddEditTrainingCategory = true;
  }

  EditClick(item: any){
    this.trainingCategory = item;
    this.modalTitle = 'Edit Category';
    this.activateAddEditTrainingCategory = true;
  }

  CloseClick(){
    this.activateAddEditTrainingCategory = false;
    this.RefreshTainingCategoryList();
  }

  RefreshTainingCategoryList() {
    this.service.GetTrainingCategoryList().subscribe(
      (data: any) => {
        this.trainingCategoriesList = data;
        this.trainingCategoriesListWithoutFilter = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){
    var val = {
      id: item.id,
      category: item.category
    };
    if (confirm('Are you sure??')){
      this.service.DeleteTrainingCategory(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert(data.toString());
            this.RefreshTainingCategoryList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  FilterFn(){
    var trainingCategorFilter = this.trainingCategorFilter;
    
    this.trainingCategoriesList = this.trainingCategoriesListWithoutFilter.filter(function (el: { category: { toString: () => string; }; }){
        return el.category.toString().toLowerCase().includes(
          trainingCategorFilter.toString().trim().toLowerCase()
        )
    });
  }
  
  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissTrainingCategory')?.click();
  }
}
