import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTrainingCategoryComponent } from './show-training-category.component';

describe('ShowTrainingCategoryComponent', () => {
  let component: ShowTrainingCategoryComponent;
  let fixture: ComponentFixture<ShowTrainingCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowTrainingCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTrainingCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
