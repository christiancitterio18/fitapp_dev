import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-diet',
  templateUrl: './show-diet.component.html',
  styleUrls: ['./show-diet.component.css']
})
export class ShowDietComponent implements OnInit {

  constructor(private service: SharedService) { }

  dietList: any = [];
  dietCategoryList: any = [];
  dietListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditDiet: boolean = false;
  dietData: any;

  
  dietFilter:string="";

  

  ngOnInit(): void {
    this.RefreshDietList();
  }


  AddClick(){
    this.dietData = {
      id: 0,
      diet_category_id: '',
      diet: '',
      description: '',
      duration: ''
    };
    
    this.modalTitle = 'Add diet';
    this.activateAddEditDiet = true;
  }

  EditClick(item: any){
    this.dietData = item;
    this.modalTitle = 'Edit diet';
    this.activateAddEditDiet = true;
  }

  CloseClick(){
    this.activateAddEditDiet = false;
    this.RefreshDietList();
  }

  RefreshDietList() {

    this.service.GetDietCategoryList().subscribe(
      (data: any) => {
        this.dietCategoryList = data;
      },
      (err: any) => {
        alert('Unknown Server Error!');
      }
    );

    this.service.GetDietList().subscribe(
      (data: any) => {
        this.dietList = data;
        this.dietListWithoutFilter=data;
      },
      (err: any) => {
        alert('Unknown Server Error!');
      }
    );
    
  }

  
  DeleteClick(item: any) {
    console.log(item);
    if (confirm('Are you sure?')){
      this.service.DeleteDiet(item.id).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshDietList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error!');
        }
      );
    }
  }

 GetCategory(item: any): string {
    return this.dietCategoryList.find((element: any) => {
      return element.id == item.diet_category_id;
    }).category;
  }

  FilterFn(){
    var dietFilter = this.dietFilter;
    
    this.dietList = this.dietListWithoutFilter.filter((el: any) => {
        return el.diet.toString().toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        )||
        this.GetCategory(el).toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        )||
        el.description.toString().toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        )||
        el.duration.toString().toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('dismissModalDiet')?.click();
  }

}

