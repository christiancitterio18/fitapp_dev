import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDietMealComponent } from './show-diet-meal.component';

describe('ShowDietMealComponent', () => {
  let component: ShowDietMealComponent;
  let fixture: ComponentFixture<ShowDietMealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowDietMealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDietMealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
