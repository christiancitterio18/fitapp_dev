import { Component, Input, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-diet-meal',
  templateUrl: './show-diet-meal.component.html',
  styleUrls: ['./show-diet-meal.component.css']
})
export class ShowDietMealComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() id: any;

  dietId: string = ''
  
  dietMealsList: any = [];
  mealsList: any = [];

  modalTitle: string = '';
  activateAddEditDietMeal:boolean=false;
  dietMeal:any;

  ngOnInit(): void {
    this.RefreshDietMealList(); 
    this.dietId = this.id
  }

  AddClick(){
    this.dietMeal={
      diet_id: this.dietId,
      meal_id: '',
      day: ''
    }

    this.modalTitle="Add meal";
    this.activateAddEditDietMeal=true;
  }

  DeleteClick(item: any){

    var val = {
      diet_id: item.diet_id,
      meal_id: item.meal_id,
      day: item.day
    };
    if(confirm('Are you sure?')){
      this.service.DeleteDietMeal(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshDietMealList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error!');
        }
      )
    }
  }

  CloseClick(){
    this.activateAddEditDietMeal=false;
    this.RefreshDietMealList();
  }

  RefreshDietMealList(){
    var list: any = [];

    this.service.GetMealList().subscribe(
      (dataList: any) => {
        this.mealsList = dataList;
        this.service.GetDietMealList().subscribe(
          (data: any) => {
            list = data;
            this.dietMealsList = list.filter(
              (element: any) => {
                return element.diet_id == this.dietId;
              }
            );
          },
          (err: any) => {
            alert('Unknown Server Error!');
          }
        );
      },
      (errs: any) => {
        alert('Unknown Server Error!');
      }
    );
  }

  GetMealName(item: any) {
    return this.mealsList.find((element: any) => {
      return element.id == item.meal_id;
    }).meal;
  }

  SuccessForm(tf: any) {
    document.getElementById('dataDismissDietMeal')?.click();
  }

}
