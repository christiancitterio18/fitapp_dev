import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-diet-meal',
  templateUrl: './add-edit-diet-meal.component.html',
  styleUrls: ['./add-edit-diet-meal.component.css']
})
export class AddEditDietMealComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() dietMeal: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  diet_id: string = '';
  meal_id: string = '';
  day: string = '';
  
  mealsList: any = [];
  dietMealErrors: any = {
    meal_id: [],
    day: []
  };

  ngOnInit(): void {
    this.GetMealList();
    this.diet_id=this.dietMeal.diet_id;
    this.meal_id=this.dietMeal.meal_id;
    this.day = this.dietMeal.day;
  }

  AddDietMeal(){
    var val = {
      diet_id: this.diet_id,
      meal_id: this.meal_id,
      day: this.day
    }
    
    this.service.AddDietMeal(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          if (err.error.errors.meal_id) {
            this.dietMealErrors.meal_id = [];
            this.dietMealErrors.meal_id.push('Please select a valid Option!');
          }
          if (err.error.errors.day) {
            this.dietMealErrors.day = [];
            this.dietMealErrors.day.push('Please enter a positive and greather than 0 whole number!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetMealList() { 
    this.service.GetMealList().subscribe(
      (data: any) => {
        this.mealsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(lenCheck: any[]) {
    return (lenCheck.length > 0);
  }

}
