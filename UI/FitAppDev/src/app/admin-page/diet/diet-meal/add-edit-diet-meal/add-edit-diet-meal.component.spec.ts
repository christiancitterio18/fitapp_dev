import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditDietMealComponent } from './add-edit-diet-meal.component';

describe('AddEditDietMealComponent', () => {
  let component: AddEditDietMealComponent;
  let fixture: ComponentFixture<AddEditDietMealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditDietMealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditDietMealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
