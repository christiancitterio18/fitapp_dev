import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-diet-meal',
  templateUrl: './diet-meal.component.html',
  styleUrls: ['./diet-meal.component.css']
})
export class DietMealComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }

}
