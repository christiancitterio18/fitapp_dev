import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-diet',
  templateUrl: './add-edit-diet.component.html',
  styleUrls: ['./add-edit-diet.component.css']
})
export class AddEditDietComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() dietData: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  diet_category_id: string = '';
  diet: string = '';
  description: string = '';
  duration: string = '';

  dietCategoryList: any = [];

  dietErrors: any = {
    diet: [],
    description: [],
    diet_category_id: []
  };

  ngOnInit(): void {
    this.GetCategoryList();
    this.id = this.dietData.id;
    this.diet_category_id = this.dietData.diet_category_id;
    this.diet = this.dietData.diet;
    this.description = this.dietData.description;
    this.duration = this.dietData.duration;
  }

  AddDiet(){
    var dietData = {
      id: this.id,
      diet_category_id: this.diet_category_id,
      diet: this.diet,
      description: this.description,
      duration: 1
    }

    this.service.PostDiet(dietData).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          if (err.error.errors.description) {
            this.dietErrors.description = err.errer.errors.description;
          }
          if (err.error.errors.diet) {
            this.dietErrors.diet = err.error.errors.diet;
          }
          if (err.error.errors.diet_category_id) {
            this.dietErrors.diet_category_id = [];
            this.dietErrors.diet_category_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateDiet(){
    var dietData = {
      id: this.id,
      diet_category_id: this.diet_category_id,
      diet: this.diet,
      description: this.description,
      duration: this.duration
    }

    this.service.PutDiet(dietData).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          if (err.error.errors.description) {
            this.dietErrors.description = err.errer.errors.description;
          }
          if (err.error.errors.diet) {
            this.dietErrors.diet = err.error.errors.diet;
          }
          if (err.error.errors.diet_category_id) {
            this.dietErrors.diet_category_id = [];
            this.dietErrors.diet_category_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetCategoryList() {
    this.service.GetDietCategoryList().subscribe(
      (data: any) => {
        this.dietCategoryList = data;
      },
      (err: any) => {
        alert('Unknown server error');
      }
    );
  }

  isError(lenCheck: any[]) {
    return (lenCheck.length > 0);
  }

}
