import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowContusionComponent } from './show-contusion.component';

describe('ShowContusionComponent', () => {
  let component: ShowContusionComponent;
  let fixture: ComponentFixture<ShowContusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowContusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowContusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
