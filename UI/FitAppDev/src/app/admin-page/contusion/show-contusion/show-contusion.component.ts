import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-contusion',
  templateUrl: './show-contusion.component.html',
  styleUrls: ['./show-contusion.component.css']
})
export class ShowContusionComponent implements OnInit {


  constructor(private service: SharedService) { }

  contusionsList: any = [];
  contusionsListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditContusion: Boolean = false;
  Contusion: any;

  contusionsCategorFilter:string="";

  ngOnInit(): void {
    this.RefreshContusiontList();
  }

 
  AddClick(){
    this.Contusion = {
      id: 0,
      contusion: ''
    };
    this.modalTitle = 'Add Contysion';
    this.activateAddEditContusion = true;
  }


  EditClick(item: any){
    this.Contusion = item;
    this.modalTitle = 'Edit Contysion';
    this.activateAddEditContusion = true;
  }


  CloseClick(){
    this.activateAddEditContusion = false;
    this.RefreshContusiontList();
  }


  RefreshContusiontList() {
    this.service.GetContusionsList().subscribe(
      (data: any) => {
        this.contusionsList = data;
        this.contusionsListWithoutFilter = data;
      },
      (err: any) => {
        alert('Unknown server error');
      }
    );
  }


  DeleteClick(item: any){
    var val = {
      id: item.id,
      contusion: item.contusion
    };
    if (confirm('Are you sure??')){
      this.service.DeleteContusion(val).subscribe(
        (data: any) => {
          if (data == 'Success') {
            alert("Deleted!");
            this.RefreshContusiontList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown server error');
        }
      );
    }
  }

  FilterFn(){
    var contusionsCategorFilter = this.contusionsCategorFilter;
      
    this.contusionsList = this.contusionsListWithoutFilter.filter(function (Contusion: { contusion: { toString: () => string; }; }){
        return Contusion.contusion.toString().toLowerCase().includes(
          contusionsCategorFilter.toString().trim().toLowerCase()
        )
      }
    );
  }

  successForm(tf: boolean) {
    document.getElementById("dismissModalContusion")?.click();
  }

}
