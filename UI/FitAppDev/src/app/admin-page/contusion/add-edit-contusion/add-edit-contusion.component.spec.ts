import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditContusionComponent } from './add-edit-contusion.component';

describe('AddEditContusionComponent', () => {
  let component: AddEditContusionComponent;
  let fixture: ComponentFixture<AddEditContusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditContusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditContusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
