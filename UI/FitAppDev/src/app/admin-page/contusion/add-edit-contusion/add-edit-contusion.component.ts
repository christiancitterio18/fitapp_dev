import { Component,Input, OnInit, Output, EventEmitter} from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-contusion',
  templateUrl: './add-edit-contusion.component.html',
  styleUrls: ['./add-edit-contusion.component.css']
})
export class AddEditContusionComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Contusion: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  contusion: string = '';

  contusionErrorList = [];

  ngOnInit(): void {
  this.id = this.Contusion.id;
  this.contusion = this.Contusion.contusion;
  }

  AddContusion(){
    var val = {
      id: this.id,
      contusion: this.contusion
    };
    console.log(val)           
    this.service.AddContusion(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          this.contusionErrorList = err.error.errors.contusion;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateContusion(){
    var val = {
      id: this.id,
      contusion: this.contusion
    };
    this.service.UpdateContusion(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          this.contusionErrorList = err.error.errors.contusion;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  isError() {
    return (this.contusionErrorList.length > 0);
  }

}
