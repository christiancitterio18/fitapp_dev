import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContusionComponent } from './contusion.component';

describe('ContusionComponent', () => {
  let component: ContusionComponent;
  let fixture: ComponentFixture<ContusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
