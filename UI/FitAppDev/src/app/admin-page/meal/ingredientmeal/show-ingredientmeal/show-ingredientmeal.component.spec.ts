import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIngredientmealComponent } from './show-ingredientmeal.component';

describe('ShowIngredientmealComponent', () => {
  let component: ShowIngredientmealComponent;
  let fixture: ComponentFixture<ShowIngredientmealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowIngredientmealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIngredientmealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
