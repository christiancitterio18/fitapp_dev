import { Component, Input, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-ingredientmeal',
  templateUrl: './show-ingredientmeal.component.html',
  styleUrls: ['./show-ingredientmeal.component.css']
})
export class ShowIngredientmealComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() id: any;

  mealId: string = '';

  mealIngredientsList: any = [];
  ingredientsList: any = [];

  modalTitle: string = '';
  activateAddEditIngredientMeal: Boolean = false;
  ingredientMeal: any;


  ngOnInit(): void {
    this.RefreshIngredientMealList();
    this.mealId = this.id;
  }

  
  AddClick(){
    this.ingredientMeal = {
      meal_id: this.mealId,
      ingredient_id: '',
      grams: ''
    };
    this.modalTitle = 'Add Ingredient Meal Reference';
    this.activateAddEditIngredientMeal = true;
  }

  
  EditClick(item: any){
    this.ingredientMeal = item;
    this.modalTitle = 'Edit Ingredient Meal Reference';
    this.activateAddEditIngredientMeal = true;
  }

  
  CloseClick(){
    this.activateAddEditIngredientMeal = false;
    this.RefreshIngredientMealList();
  }

  
  RefreshIngredientMealList() {
    var dataList: any = [];
    this.service.GetIngredientList().subscribe(
      (data: any) => {
        this.ingredientsList = data;
        this.service.GetIngredientMealList().subscribe(
          (dataRes: any) => {
            dataList = dataRes;
            this.mealIngredientsList = dataList.filter((element: any) => {
              return element.meal_id == this.mealId;
            })
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    )
    
  }

  DeleteClick(item: any){
    var val = {
      meal_id: item.meal_id,
      ingredient_id: item.ingredient_id,
      grams: item.grams
    };
    if (confirm('Are you sure??')){
      this.service.DeleteIngredientMeal(val).subscribe(
        (data: any) => {
          if(data == 'Success'){
            alert('Deleted');
            this.RefreshIngredientMealList();
          } else {
            alert('Something went wrong, please try again later!'); 
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }
  
  GetIngredientName(item: any) {
    return this.ingredientsList.find((element: any) => {
      return element.id == item.ingredient_id
    }).ingredient;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissMealIngrdient')?.click();
  }
}
