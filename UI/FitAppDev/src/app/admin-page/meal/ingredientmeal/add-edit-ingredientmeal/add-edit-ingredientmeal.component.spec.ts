import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditIngredientmealComponent } from './add-edit-ingredientmeal.component';

describe('AddEditIngredientmealComponent', () => {
  let component: AddEditIngredientmealComponent;
  let fixture: ComponentFixture<AddEditIngredientmealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditIngredientmealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditIngredientmealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
