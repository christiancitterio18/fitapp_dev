import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-ingredientmeal',
  templateUrl: './add-edit-ingredientmeal.component.html',
  styleUrls: ['./add-edit-ingredientmeal.component.css']
})
export class AddEditIngredientmealComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() ingredientMeal: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  meal_id: string = '';
  ingredient_id: string = '';
  grams: string = '';

  ingredientsList: any = [];

  mealIngredientErrors: any = {
    ingredient_id: new Array(),
    grams: new Array()
  }

  ngOnInit(): void {
    this.GetIngredientList();
    this.meal_id = this.ingredientMeal.meal_id;
    this.ingredient_id = this.ingredientMeal.ingredient_id;
    this.grams = this.ingredientMeal.grams;
  }
  
  AddIngredientMeal(){
    var val = {
      meal_id: this.meal_id,
      ingredient_id: this.ingredient_id, 
      grams: this.grams
    };

    this.service.AddIngredientMeal(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert(res);
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.grams) {
            this.mealIngredientErrors.grams = err.error.errors.grams;
          }
          if (err.error.errors.ingredient_id) {
            this.mealIngredientErrors.ingredient_id = [];
            this.mealIngredientErrors.ingredient_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  
  UpdateIngredientMeal(){
    var val = {
      meal_id: this.meal_id,
      ingredient_id: this.ingredient_id, 
      grams: this.grams
    };
    this.service.UpdateIngredientMeal(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.grams) {
            this.mealIngredientErrors.grams = err.error.errors.grams;
          }
          if (err.error.errors.ingredient_id) {
            this.mealIngredientErrors.ingredient_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetIngredientList() {
    this.service.GetIngredientList().subscribe(
      (data: any) => {
        this.ingredientsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }
  
  isError(dataList: any[]) {
    return (dataList.length > 0);
  }
}
