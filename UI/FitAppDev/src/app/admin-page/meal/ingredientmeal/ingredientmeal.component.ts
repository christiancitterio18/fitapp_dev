import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ingredientmeal',
  templateUrl: './ingredientmeal.component.html',
  styleUrls: ['./ingredientmeal.component.css']
})
export class IngredientmealComponent implements OnInit {

   constructor(private route: ActivatedRoute) { }

  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }


}
