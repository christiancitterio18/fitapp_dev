import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientmealComponent } from './ingredientmeal.component';

describe('IngredientmealComponent', () => {
  let component: IngredientmealComponent;
  let fixture: ComponentFixture<IngredientmealComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngredientmealComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientmealComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
