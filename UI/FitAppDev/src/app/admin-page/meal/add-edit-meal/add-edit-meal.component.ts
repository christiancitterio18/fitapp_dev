import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-meal',
  templateUrl: './add-edit-meal.component.html',
  styleUrls: ['./add-edit-meal.component.css']
})
export class AddEditMealComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Meal: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  meal_category_id: string = '';
  meal: string = '';
  description: string = '';
  calories: string = '';

  categoriesList: any = [];

  mealsErrors: any = {
    meal_category_id: new Array(),
    meal: [],
    description: []
  }

  ngOnInit(): void {
    this.GetCategoryList();
    this.id = this.Meal.id;
    this.meal_category_id = this.Meal.meal_category_id;
    this.meal = this.Meal.meal;
    this.description = this.Meal.description;
    this.calories = this.Meal.calories;
  }
  
  AddMeal(){
    let val = {id: this.id,
      meal_category_id: this.meal_category_id,
      meal: this.meal,
      description: this.description,
      calories: 0,
    };
    console.log(val);
    this.service.AddMeal(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.meal_category_id) {
            this.mealsErrors.meal_category_id = []
            this.mealsErrors.meal_category_id.push('Please select a valid option!');
          }
          if (err.error.errors.meal) {
            this.mealsErrors.meal = err.error.errors.meal;
          }
          if (err.error.errors.description) {
            this.mealsErrors.description = err.error.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateMeal(){
    let val = {
      id: this.id,
      meal_category_id: this.meal_category_id,
      meal: this.meal,
      description: this.description,
      calories: this.calories
    };
    this.service.UpdateMeal(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.meal_category_id) {
            this.mealsErrors.meal_category_id = []
            this.mealsErrors.meal_category_id.push('Please select a valid option!');
          }
          if (err.error.errors.meal) {
            this.mealsErrors.meal = err.error.errors.meal;
          }
          if (err.error.errors.description) {
            this.mealsErrors.description = err.error.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetCategoryList() {
    this.service.GetMealCategoryList().subscribe(
      (data: any) => {
        this.categoriesList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(lenCheck: any[]) {
    return (lenCheck.length > 0);
  }

}

