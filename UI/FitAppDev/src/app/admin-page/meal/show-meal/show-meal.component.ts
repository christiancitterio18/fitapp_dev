import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-meal',
  templateUrl: './show-meal.component.html',
  styleUrls: ['./show-meal.component.css']
})
export class ShowMealComponent implements OnInit {

  constructor(private service: SharedService) { }

  mealsList: any = [];
  categoriesList: any = [];
  mealsListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditMeal: boolean = false;
  Meal: any;

  mealFilter:string="";
  

  ngOnInit(): void {
    this.RefreshMealList();
  }

  AddClick(){
    this.Meal = {
      id: 0,
      meal_category_id: '',
      meal: '',
      description: '',
      calories: ''
    };
    this.modalTitle = 'Add Meal';
    this.activateAddEditMeal = true;
  }

  EditClick(item: any){
    this.Meal = item;
    this.modalTitle = 'Edit Meal';
    this.activateAddEditMeal = true;
  }

  CloseClick(){
    this.activateAddEditMeal = false;
    this.RefreshMealList();
  }

  RefreshMealList() {
    this.service.GetMealCategoryList().subscribe(
      (data: any) => {
        this.categoriesList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    )
    this.service.GetMealList().subscribe(
      (dataList: any) => {
        this.mealsList = dataList;
        this.mealsListWithoutFilter = dataList; 
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
  }

  DeleteClick(item: any){
    let val = {
      id: item.id,
      meal_category_id: item.meal_category_id,
      meal: item.meal,
      description: item.description,
      calories: item.calories
    };
      
    if (confirm('Are you sure??')){
      this.service.DeleteMeal(val).subscribe(
        (data: any) => {
          if(data == 'Success'){
            alert('Deleted');
            this.RefreshMealList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetCategoryName(item: any) {
    return this.categoriesList.find(
      (element: any) => {
        return element.id == item.meal_category_id;
      }
    ).category;
  }

  FilterFn(){
    var mealFilter = this.mealFilter;
  
    
    this.mealsList = this.mealsListWithoutFilter.filter( (el:any) => {
        return el.meal.toString().toLowerCase().includes(
          mealFilter.toString().trim().toLowerCase()
        )||
        this.GetCategoryName(el).toString().toLowerCase().includes(
          mealFilter.toString().trim().toLowerCase()
        )||
        el.description.toString().toLowerCase().includes(
          mealFilter.toString().trim().toLowerCase()
        )||
        el.calories.toString().toLowerCase().includes(
          mealFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissMeal')?.click();
  }

}
