import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditMicroelementComponent } from './add-edit-microelement.component';

describe('AddEditMicroelementComponent', () => {
  let component: AddEditMicroelementComponent;
  let fixture: ComponentFixture<AddEditMicroelementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditMicroelementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditMicroelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
