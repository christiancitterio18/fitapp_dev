import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-microelement',
  templateUrl: './add-edit-microelement.component.html',
  styleUrls: ['./add-edit-microelement.component.css']
})
export class AddEditMicroelementComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() Microelement: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  microelement: string = '';

  microelementsErrors: any = [];

  ngOnInit(): void {
  this.id = this.Microelement.id;
  this.microelement = this.Microelement.microelement;
  }
  
  AddMicroelement(){
    var val = {
      id: this.id,
      microelement: this.microelement
    };
    
    this.service.AddMicroelement(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.microelementsErrors = err.error.errors.microelement;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateMicroelement(){
    var val = {
      id: this.id,
      microelement: this.microelement
    };
    
    this.service.UpdateMicroelement(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.microelementsErrors = err.error.errors.microelement;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  isError() {
    return (this.microelementsErrors.length > 0);
  }

}

