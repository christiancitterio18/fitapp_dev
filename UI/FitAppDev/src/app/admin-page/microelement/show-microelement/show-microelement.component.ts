import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-microelement',
  templateUrl: './show-microelement.component.html',
  styleUrls: ['./show-microelement.component.css']
})
export class ShowMicroelementComponent implements OnInit {

  constructor(private service: SharedService) { }

  microelementsList: any = [];
  microelementsListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditMicroelement: boolean = false;
  Microelement: any;

  microelementFilter:string="";

  ngOnInit(): void {
    this.RefresMicroelementList();
  }

  AddClick(){
    this.Microelement = {
      id: 0,
      microelement: ''
    };
    this.modalTitle = 'Add microelement';
    this.activateAddEditMicroelement = true;
  }

  EditClick(item: any){
    this.Microelement = item;
    this.modalTitle = 'Edit microelement';
    this.activateAddEditMicroelement = true;
  }

  CloseClick(){
    this.activateAddEditMicroelement = false;
    this.RefresMicroelementList();
  }

  RefresMicroelementList() {
    this.service.GetMicroelementList().subscribe(data => {
      this.microelementsList = data;
      this.microelementsListWithoutFilter = data;
    });
  }

  DeleteClick(item: any){
    let val = {
      id: item.id,
      microelement: item.microelement
    };
    
    if (confirm('Are you sure??')) {
      this.service.DeleteMicroelement(val).subscribe(data => {
        alert(data.toString());
        this.RefresMicroelementList();
      });
    }
  }

  FilterFn(){
  var microelementFilter = this.microelementFilter;
  
  this.microelementsList = this.microelementsListWithoutFilter.filter(function (el: { 
    microelement: { toString: () => string; }; 
    calories_gram: { toString: () => string; };}){
      return el.microelement.toString().toLowerCase().includes(
        microelementFilter.toString().trim().toLowerCase()
      )
   });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissMicroelement')?.click();
  }
}
