import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMicroelementComponent } from './show-microelement.component';

describe('ShowMicroelementComponent', () => {
  let component: ShowMicroelementComponent;
  let fixture: ComponentFixture<ShowMicroelementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowMicroelementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMicroelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
