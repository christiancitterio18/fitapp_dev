import { ComponentFixture, TestBed } from '@angular/core/testing';

import { MicroelementComponent } from './microelement.component';

describe('MicroelementComponent', () => {
  let component: MicroelementComponent;
  let fixture: ComponentFixture<MicroelementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ MicroelementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(MicroelementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
