import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ingredient-micronutrient',
  templateUrl: './ingredient-micronutrient.component.html',
  styleUrls: ['./ingredient-micronutrient.component.css']
})
export class IngredientMicronutrientComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }

}
