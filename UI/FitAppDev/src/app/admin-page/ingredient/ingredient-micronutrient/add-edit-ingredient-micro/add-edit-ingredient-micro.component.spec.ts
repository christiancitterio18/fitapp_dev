import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditIngredientMicroComponent } from './add-edit-ingredient-micro.component';

describe('AddEditIngredientMicroComponent', () => {
  let component: AddEditIngredientMicroComponent;
  let fixture: ComponentFixture<AddEditIngredientMicroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditIngredientMicroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditIngredientMicroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
