import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-ingredient-micro',
  templateUrl: './add-edit-ingredient-micro.component.html',
  styleUrls: ['./add-edit-ingredient-micro.component.css']
})
export class AddEditIngredientMicroComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() ingredientMicro: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();
  
  microelement_id: string = '';
  ingredient_id: string = '';
  grams: string = '';

  micronutrientsList: any = [];
  ingredientMicroErrors: any = {
    grams: [],
    microelement_id: []
  };

  ngOnInit(): void {
    this.GetMicronutrientsList();
    this.microelement_id = this.ingredientMicro.microelement_id;
    this.ingredient_id = this.ingredientMicro.ingredient_id;
    this.grams = this.ingredientMicro.grams;
  }

  Addingredient(){
    var data = {
      microelement_id: this.microelement_id,
      ingredient_id: this.ingredient_id,
      grams: this.grams
    }

    this.service.PostIngredientMicronutrient(data).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.grams) {
            this.ingredientMicroErrors.grams = err.error.errors.grams;
          }
          if (err.error.errors.microelement_id) {
            this.ingredientMicroErrors.microelement_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateIngredient(){
    var data = {
      microelement_id: this.microelement_id,
      ingredient_id: this.ingredient_id,
      grams: this.grams
    }

    this.service.PutIngredientMicronutrient(data).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.grams) {
            this.ingredientMicroErrors.grams = err.error.errors.grams;
          }
          if (err.error.errors.microelement_id) {
            this.ingredientMicroErrors.microelement_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetMicronutrientsList() {
    this.service.GetMicroelementList().subscribe(
      (data: any) => {
        this.micronutrientsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
