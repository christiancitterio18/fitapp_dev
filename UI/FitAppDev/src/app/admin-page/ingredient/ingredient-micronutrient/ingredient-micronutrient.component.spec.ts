import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientMicronutrientComponent } from './ingredient-micronutrient.component';

describe('IngredientMicronutrientComponent', () => {
  let component: IngredientMicronutrientComponent;
  let fixture: ComponentFixture<IngredientMicronutrientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngredientMicronutrientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientMicronutrientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
