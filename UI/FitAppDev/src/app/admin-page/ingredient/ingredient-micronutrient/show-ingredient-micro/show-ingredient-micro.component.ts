import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-ingredient-micro',
  templateUrl: './show-ingredient-micro.component.html',
  styleUrls: ['./show-ingredient-micro.component.css']
})
export class ShowIngredientMicroComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() id !: string;

  ingredientId: string = '';
  
  micronutrientsList: any = [];
  dataList: any = [];

  modalTitle: string = '';
  activateAddEditIngredientMicro: boolean = false;
  ingredientMicro: any;

  ngOnInit(): void {
    this.ingredientId = this.id;
    this.RefreshDataList();
  }

  AddClick(){
    this.ingredientMicro = {
      microelement_id: '',
      ingredient_id: this.ingredientId,
      grams: ''
    };
    this.modalTitle = 'Add micronutrient';
    this.activateAddEditIngredientMicro = true;
  }

  EditClick(item: any) {
    this.ingredientMicro = item;
    this.modalTitle = 'Edit micronutrient';
    this.activateAddEditIngredientMicro = true;
  }

  CloseClick(){
    this.activateAddEditIngredientMicro = false;
    this.RefreshDataList();
  }

  RefreshDataList() {
    var dataSet: any = [];
    this.service.GetMicroelementList().subscribe((data: any) => {
      this.micronutrientsList = data;
    });

    this.service.GetIngredientMicronutrientsList().subscribe(
      (data: any) => {
        dataSet = data;
        this.dataList = dataSet.filter(
          (element: any) => {
            return element.ingredient_id == this.ingredientId;
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }
  
  DeleteClick(item: any){
    
    let val = {
      microelement_id: item.microelement_id,
      ingredient_id: item.ingredient_id,
      grams: item.grams
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteIngredientMicronutrient(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshDataList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetMicronutrientName(item: any) {
    return this.micronutrientsList.find((element: any) => {
      return element.id == item.microelement_id;
    }).microelement;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissIngredientMicro')?.click();
  }

}
