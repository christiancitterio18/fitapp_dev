import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIngredientMicroComponent } from './show-ingredient-micro.component';

describe('ShowIngredientMicroComponent', () => {
  let component: ShowIngredientMicroComponent;
  let fixture: ComponentFixture<ShowIngredientMicroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowIngredientMicroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIngredientMicroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
