import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-ingredient',
  templateUrl: './add-edit-ingredient.component.html',
  styleUrls: ['./add-edit-ingredient.component.css']
})
export class AddEditIngredientComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Ingredient: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  ingredient: string = '';
  is_alergenic: string = '';

  ingredientErrors: any = {
    ingredient: [],
    is_alergenic: []
  };

  ngOnInit(): void {
    this.id = this.Ingredient.id;
    this.ingredient = this.Ingredient.ingredient;
    this.is_alergenic = this.Ingredient.is_alergenic;
  }

  AddIngredient(){
    var ingredientData = {
      id: this.id,
      ingredient: this.ingredient,
      is_alergenic: this.is_alergenic
    }

    this.service.PostIngredient(ingredientData).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.ingredient) {
            this.ingredientErrors.ingredient == err.error.errors.ingredient;
          }
          if (err.error.errors.is_alergenic) {
            this.ingredientErrors.is_alergenic[0] == 'Please select a valid option!';
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    )
  }

  UpdateIngredient(){
    var ingredientData = {
      id: this.id,
      ingredient: this.ingredient,
      is_alergenic: this.is_alergenic
    }

    this.service.PutIngredient(ingredientData).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.ingredient) {
            this.ingredientErrors.ingredient = err.error.errors.ingredient;
          }
          if (err.error.errors.is_alergenic) {
            this.ingredientErrors.is_alergenic.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
