import { ComponentFixture, TestBed } from '@angular/core/testing';

import { IngredientMacronutrientComponent } from './ingredient-macronutrient.component';

describe('IngredientMacronutrientComponent', () => {
  let component: IngredientMacronutrientComponent;
  let fixture: ComponentFixture<IngredientMacronutrientComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ IngredientMacronutrientComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(IngredientMacronutrientComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
