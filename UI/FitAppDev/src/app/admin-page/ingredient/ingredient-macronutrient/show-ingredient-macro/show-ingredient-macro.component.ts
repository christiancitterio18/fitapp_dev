import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-ingredient-macro',
  templateUrl: './show-ingredient-macro.component.html',
  styleUrls: ['./show-ingredient-macro.component.css']
})
export class ShowIngredientMacroComponent implements OnInit {

  
  constructor(private service: SharedService) { }

  @Input() id !: string;

  ingredientId: string = '';
  
  macronutrientsList: any = [];
  dataList: any = [];

  modalTitle: string = '';
  activateAddEditIngredientMacro: boolean = false;
  ingredientMacro: any;

  ngOnInit(): void {
    this.ingredientId = this.id;
    this.RefreshDataList();
  }

  AddClick(){
    this.ingredientMacro = {
      macroelement_id: '',
      ingredient_id: this.ingredientId,
      grams: ''
    };
    this.modalTitle = 'Add macronutrient';
    this.activateAddEditIngredientMacro = true;
  }

  EditClick(item: any){
    this.ingredientMacro = item;
    this.modalTitle = 'Edit macronutrient';
    this.activateAddEditIngredientMacro = true;
  }


  CloseClick(){
    this.activateAddEditIngredientMacro = false;
    this.RefreshDataList();
  }


  RefreshDataList() {
    var dataSet: any = [];
    this.service.GetMacroelementList().subscribe((data: any) => {
      this.macronutrientsList = data;
    });

    this.service.GetIngredientMacronutrientsList().subscribe(
      (data: any) => {
        dataSet = data;
        this.dataList = dataSet.filter(
          (element: any) => {
            return element.ingredient_id == this.ingredientId;
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }
  
  DeleteClick(item: any){
    
    let val = {
      macroelement_id: item.macroelement_id,
      ingredient_id: item.ingredient_id,
      grams: item.grams
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteIngredientMacronutrient(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshDataList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetMacronutrientName(item: any) {
    return this.macronutrientsList.find((element: any) => {
      return element.id == item.macroelement_id;
    }).macroelement;
  }

  GetCalories(item: any) {
    return this.macronutrientsList.find((element: any) => {
      return element.id == item.macroelement_id;
    }).calories_gram * item.grams;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissIngredientMacro')?.click();
  }

}