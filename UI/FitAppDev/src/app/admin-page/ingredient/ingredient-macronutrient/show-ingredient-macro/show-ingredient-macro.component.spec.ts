import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowIngredientMacroComponent } from './show-ingredient-macro.component';

describe('ShowIngredientMacroComponent', () => {
  let component: ShowIngredientMacroComponent;
  let fixture: ComponentFixture<ShowIngredientMacroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowIngredientMacroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowIngredientMacroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
