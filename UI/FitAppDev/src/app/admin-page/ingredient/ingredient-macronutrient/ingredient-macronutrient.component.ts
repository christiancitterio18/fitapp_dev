import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-ingredient-macronutrient',
  templateUrl: './ingredient-macronutrient.component.html',
  styleUrls: ['./ingredient-macronutrient.component.css']
})
export class IngredientMacronutrientComponent implements OnInit {

  
  constructor(private route: ActivatedRoute) {}
  
  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }

}
