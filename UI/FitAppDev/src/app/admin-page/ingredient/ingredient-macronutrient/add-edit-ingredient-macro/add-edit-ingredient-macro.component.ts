import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-ingredient-macro',
  templateUrl: './add-edit-ingredient-macro.component.html',
  styleUrls: ['./add-edit-ingredient-macro.component.css']
})
export class AddEditIngredientMacroComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() ingredientMacro: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  macroelement_id: string = '';
  ingredient_id: string = '';
  grams: string = '';

  macronutrientsList: any = [];

  ingredientMacroErrors: any = {
    grams: [],
    macroelement_id: []
  };

  ngOnInit(): void {
    this.GetMacroList();
    this.macroelement_id = this.ingredientMacro.macroelement_id;
    this.ingredient_id = this.ingredientMacro.ingredient_id;
    this.grams = this.ingredientMacro.grams;
  }

  Addingredient(){
    var data = {
      macroelement_id: this.macroelement_id,
      ingredient_id: this.ingredient_id,
      grams: this.grams
    }

    this.service.PostIngredientMacronutrient(data).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.grams) {
            this.ingredientMacroErrors.grams = err.error.errors.grams;
          }
          if (err.error.errors.macroelement_id) {
            this.ingredientMacroErrors.macroelement_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateIngredient(){
    var data = {
      macroelement_id: this.macroelement_id,
      ingredient_id: this.ingredient_id,
      grams: this.grams
    }

    this.service.PutIngredientMacronutrient(data).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.grams) {
            this.ingredientMacroErrors.grams = err.error.errors.grams;
          }
          if (err.error.errors.macroelement_id) {
            this.ingredientMacroErrors.macroelement_id.push('Please select a valid option!');
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    )
  }

  GetMacroList() {
    this.service.GetMacroelementList().subscribe(
      (data: any) => {
        this.macronutrientsList = data;
      },
      (err: any) => {
        alert('Uknown Server Error');
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
