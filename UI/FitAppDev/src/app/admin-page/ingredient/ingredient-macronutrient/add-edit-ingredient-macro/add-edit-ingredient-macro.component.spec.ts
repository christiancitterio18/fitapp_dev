import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditIngredientMacroComponent } from './add-edit-ingredient-macro.component';

describe('AddEditIngredientMacroComponent', () => {
  let component: AddEditIngredientMacroComponent;
  let fixture: ComponentFixture<AddEditIngredientMacroComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditIngredientMacroComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditIngredientMacroComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
