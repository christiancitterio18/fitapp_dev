import { isNgTemplate } from '@angular/compiler';
import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-ingredient',
  templateUrl: './show-ingredient.component.html',
  styleUrls: ['./show-ingredient.component.css']
})
export class ShowIngredientComponent implements OnInit {

  constructor(private service: SharedService) { }

  ingredientsList: any = [];
  ingredientsListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditIngredient: boolean = false;
  Ingredient: any;

  ingredientsNameFilter:string="";
  

  ngOnInit(): void {
    this.RefreshIngredientList();
  }


  AddClick(){
    this.Ingredient = {
      id: 0,
      ingredient: '',
      is_alergenic: ''
    };
    this.modalTitle = 'Add ingredient';
    this.activateAddEditIngredient = true;
  }


  EditClick(item: any){
    this.Ingredient = item;
    this.modalTitle = 'Edit ingredient';
    this.activateAddEditIngredient = true;
  }


  CloseClick(){
    this.activateAddEditIngredient = false;
    this.RefreshIngredientList();
  }


  RefreshIngredientList() {
    var value: any = [];
    this.service.GetIngredientList().subscribe(
      (data: any) => {
        value = data;
        value.forEach(
          (element: any) => {
            if (element.is_alergic === 0) {
              element.is_alergic = 'yes';
            } else {
              element.is_alergic = 'no';
            }
          }
        );
      this.ingredientsList = value;
      this.ingredientsListWithoutFilter = value;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
  }
  
  DeleteClick(item: any){
    
    let val = {
      id: item.id,
      ingredient: item.ingredient,
      is_alergic: item.is_alergic
    };
      
    if (confirm('Are you sure?')){
      this.service.DeleteIngredient(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshIngredientList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetIsAlergic(item: any): string {
    if (item.is_alergic === 0){
      return 'yes';
    } else return 'no';
  }

  FilterFn(){

    var ingredientsNameFilter = this.ingredientsNameFilter;
    
    
    this.ingredientsList = this.ingredientsListWithoutFilter.filter( (exc: any)=>{
        return exc.ingredient.toString().toLowerCase().includes(
          ingredientsNameFilter.toString().trim().toLowerCase()
        )||
        exc.is_alergic.toString().toLowerCase().includes(
          ingredientsNameFilter.toString().trim().toLowerCase()
        );
    });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissIngredient')?.click();
  }
}
