import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowExerciseCategoryComponent } from './show-exercise-category.component';

describe('ShowExerciseCategoryComponent', () => {
  let component: ShowExerciseCategoryComponent;
  let fixture: ComponentFixture<ShowExerciseCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowExerciseCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowExerciseCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
