import { Component, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-exercise-category',
  templateUrl: './show-exercise-category.component.html',
  styleUrls: ['./show-exercise-category.component.css']
})
export class ShowExerciseCategoryComponent implements OnInit {

  constructor(private service: SharedService) { }

  excerciseCategoriesList: any = [];
  excerciseCategoriesListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditExcCat: boolean = false;
  excerciseCategory: any;

  excerciseCategorFilter:string="";


  ngOnInit(): void {
    this.RefreshExcerciseCategoryList();
  }


  AddClick(){
    this.excerciseCategory = {
      id: 0,
      category: ''
    };
    
    this.modalTitle = 'Add Excercise';
    this.activateAddEditExcCat = true;
  }

 
  EditClick(item: any){
    this.excerciseCategory = item;
    this.modalTitle = 'Edit Excercise';
    this.activateAddEditExcCat = true;
  }

 
  CloseClick(){
    this.activateAddEditExcCat = false;
    this.RefreshExcerciseCategoryList();
  }


  RefreshExcerciseCategoryList() {
    this.service.GetExerciseCategoryList().subscribe(
      (data: any) => {
        this.excerciseCategoriesList = data;
        this.excerciseCategoriesListWithoutFilter = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any) {
    
    let val = {
      id: item.id,
      category: item.category
    };

    if (confirm('Are you sure??')){
      this.service.DeleteExerciseCategory(val).subscribe(
        (data: any) => {
          if (data == 'Success') {
            alert('Deleted');
            this.RefreshExcerciseCategoryList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }
  
  FilterFn(){
    var excerciseCategorFilter = this.excerciseCategorFilter;
      
    this.excerciseCategoriesList = this.excerciseCategoriesListWithoutFilter.filter(function (exc: { category: { toString: () => string; }; }){
        return exc.category.toString().toLowerCase().includes(
          excerciseCategorFilter.toString().trim().toLowerCase()
        )
    });
  }
  
  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissExcerciseCat')?.click();
  }

}
