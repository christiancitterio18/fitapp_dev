import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ExcerciseCategoryComponent } from './excercise-category.component';

describe('ExcerciseCategoryComponent', () => {
  let component: ExcerciseCategoryComponent;
  let fixture: ComponentFixture<ExcerciseCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ExcerciseCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ExcerciseCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
