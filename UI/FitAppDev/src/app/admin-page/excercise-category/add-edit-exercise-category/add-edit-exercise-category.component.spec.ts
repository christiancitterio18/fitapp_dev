import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditExerciseCategoryComponent } from './add-edit-exercise-category.component';

describe('AddEditExerciseCategoryComponent', () => {
  let component: AddEditExerciseCategoryComponent;
  let fixture: ComponentFixture<AddEditExerciseCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditExerciseCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditExerciseCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
