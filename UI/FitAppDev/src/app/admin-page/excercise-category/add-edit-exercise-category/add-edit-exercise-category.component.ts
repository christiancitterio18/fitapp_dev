import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-exercise-category',
  templateUrl: './add-edit-exercise-category.component.html',
  styleUrls: ['./add-edit-exercise-category.component.css']
})
export class AddEditExerciseCategoryComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() excerciseCategory: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id:string = '';
  category: string = '';
  
  exerCategoryErrors: any = [];

  ngOnInit(): void {
    this.id = this.excerciseCategory.id;
    this.category = this.excerciseCategory.category;
  }

  AddExerciseCategory(){
    var val = {
      id: this.id,
      category: this.category
    };
    
    this.service.AddExerciseCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.exerCategoryErrors = err.error.errors.category; 
        } else {
          alert('Unknown Server Error')
        }
      }
    );
  }

  UpdateExerciseCategory(){
    var val = {
      id: this.id,
      category: this.category
    };

    this.service.UpdateExerciseCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.exerCategoryErrors = err.error.errors.category; 
        } else {
          alert('Unknown Server Error')
        }
      }
    );
  }

  isError() {
    return (this.exerCategoryErrors.length > 0);
  }
}
