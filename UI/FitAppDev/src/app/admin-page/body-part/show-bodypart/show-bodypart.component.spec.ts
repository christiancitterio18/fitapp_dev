import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowBodypartComponent } from './show-bodypart.component';

describe('ShowBodypartComponent', () => {
  let component: ShowBodypartComponent;
  let fixture: ComponentFixture<ShowBodypartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowBodypartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowBodypartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
