import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-bodypart',
  templateUrl: './show-bodypart.component.html',
  styleUrls: ['./show-bodypart.component.css']
})
export class ShowBodypartComponent implements OnInit {

  constructor(private service: SharedService) { }

  bodyPartsList: any = [];
  bodyPartWithoutFilter:any=[];

  modalTitle: string = '';
 
  activateAddEditBodyPart: Boolean = false;
  BodyPart: any;

  bodyPartFilter:string="";


  ngOnInit(): void {
    this.RefreshBodyPartList();
  }

  
  AddClick(){
    this.BodyPart = {
      id: 0,
      body_part: ''
    };
    this.modalTitle = 'Add BodyPart';
    this.activateAddEditBodyPart = true;
  }

  
  EditClick(item: any){
    this.BodyPart = item;
    this.modalTitle = 'Edit BodyPart';
    this.activateAddEditBodyPart = true;
  }

  
  CloseClick(){
    this.activateAddEditBodyPart = false;
    this.RefreshBodyPartList();
  }

  
  RefreshBodyPartList() {
    this.service.GetBodyPartList().subscribe(
      (data: any) => {
        this.bodyPartsList = data;
        this.bodyPartWithoutFilter = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  
  DeleteClick(item: any){
    var val = {
      id: item.id,
      body_part: item.body_part
    };
    if (confirm('Are you sure??')){
      this.service.DeleteBodyPart(val).subscribe(
        (data: any) => {
          if (data == 'Success') {
            alert("Deleted!");
            this.RefreshBodyPartList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  FilterFn(){
    var bodyPartFilter = this.bodyPartFilter;
    
    this.bodyPartsList = this.bodyPartWithoutFilter.filter(function (bodyPart: { body_part: { toString: () => string; }; }){
        return bodyPart.body_part.toString().toLowerCase().includes(
          bodyPartFilter.toString().trim().toLowerCase()
        )
    });
  }

  successForm(tf: boolean) {
    document.getElementById("dismissModalBP")?.click();
  }

}
