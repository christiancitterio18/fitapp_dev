import { Component, Input, OnInit, Output, EventEmitter} from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-bodypart',
  templateUrl: './add-edit-bodypart.component.html',
  styleUrls: ['./add-edit-bodypart.component.css']
})
export class AddEditBodypartComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() BodyPart: any;
  @Output() successAddUpdate: EventEmitter<any> = new EventEmitter();
  
  id: string = '';
  body_part: string = '';

  bodyPartErrorList: any = [];

  ngOnInit(): void {
    this.id = this.BodyPart.id;
    this.body_part = this.BodyPart.body_part;
  }
  
  AddBodyPart(){
    var val = {
      id: this.id,
      body_part: this.body_part
    };    
    
    this.service.AddBodyPart(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successAddUpdate.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.bodyPartErrorList = err.error.errors.body_part;
        } else {
          alert('Something went wrong, please try again later!');
        }
      }
    );
  }

  
  UpdateBodyPart(){
    var val = {
      id: this.id,
      body_part: this.body_part
    };
    
    this.service.UpdateBodyPart(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successAddUpdate.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.bodyPartErrorList = err.error.errors.body_part;
        } else {
          alert('Something went wrong, please try again later!');
        }
      }
    );
  }

  isError() {
    return (this.bodyPartErrorList.length > 0);
  }

}
