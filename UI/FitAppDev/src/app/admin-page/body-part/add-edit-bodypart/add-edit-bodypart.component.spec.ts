import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditBodypartComponent } from './add-edit-bodypart.component';

describe('AddEditBodypartComponent', () => {
  let component: AddEditBodypartComponent;
  let fixture: ComponentFixture<AddEditBodypartComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditBodypartComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditBodypartComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
