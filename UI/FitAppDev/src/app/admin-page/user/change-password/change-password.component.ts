import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';
@Component({
  selector: 'app-change-password',
  templateUrl: './change-password.component.html',
  styleUrls: ['./change-password.component.css']
})
export class ChangePasswordComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() user: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();
  
  password: string = '';
  repeatPass: string = '';

  changePassErrors: any = []

  ngOnInit(): void {
    this.password = '';
  }

  ChangePassword() {
    
    if (this.password === this.repeatPass) {
      var userData = {
        person_id: this.user.person_id,
        username: this.user.username,
        password: this.password,
        authorizations_id: this.user.authorizations_id
      };
    
      this.service.PutUser(userData).subscribe(
        (res: any) => {
        if (res == 'Success')  {
          this.successForm.emit(false);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.changePassErrors = err.error.errors.password;
        } else {
          alert('Unknown Server Error');
        }
      }
      );
    } else {
      this.changePassErrors = [];
      this.changePassErrors.push("the passwords doesen't match!");
    }
  }

  isError() {
    return this.changePassErrors.length > 0;
  }
}
