import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShorUsrConditionComponent } from './shor-usr-condition.component';

describe('ShorUsrConditionComponent', () => {
  let component: ShorUsrConditionComponent;
  let fixture: ComponentFixture<ShorUsrConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShorUsrConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShorUsrConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
