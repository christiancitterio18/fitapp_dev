import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrConditionComponent } from './usr-condition.component';

describe('UsrConditionComponent', () => {
  let component: UsrConditionComponent;
  let fixture: ComponentFixture<UsrConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsrConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
