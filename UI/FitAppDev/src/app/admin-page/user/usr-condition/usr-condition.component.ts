import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usr-condition',
  templateUrl: './usr-condition.component.html',
  styleUrls: ['./usr-condition.component.css']
})
export class UsrConditionComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }

}
