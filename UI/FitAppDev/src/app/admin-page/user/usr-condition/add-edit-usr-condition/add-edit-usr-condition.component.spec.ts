import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditUsrConditionComponent } from './add-edit-usr-condition.component';

describe('AddEditUsrConditionComponent', () => {
  let component: AddEditUsrConditionComponent;
  let fixture: ComponentFixture<AddEditUsrConditionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditUsrConditionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditUsrConditionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
