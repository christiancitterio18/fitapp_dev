import { Component, Input, OnInit, Output, EventEmitter  } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-usr-condition',
  templateUrl: './add-edit-usr-condition.component.html',
  styleUrls: ['./add-edit-usr-condition.component.css']
})
export class AddEditUsrConditionComponent implements OnInit {

  constructor(private service: SharedService) { }
  
  @Input() condition: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  health_condition_id: string = '';
  user_person_id: string = '';
  is_actual: string = ''

  conditionsList: any = [];

  userHeathConditionErrors: any = []

  ngOnInit(): void {
    this.GetConditionList();
    this.health_condition_id = this.condition.health_condition_id;
    this.user_person_id = this.condition.user_person_id;
    this.is_actual = this.condition.is_actual;
  }

  AddCondition(){
    var data = {
      health_condition_id: this.health_condition_id,
      user_person_id: this.user_person_id,
      is_actual: this.is_actual
    }

    this.service.PostUserHealthCondition(data).subscribe(
      (res: any) => {
        if (res == 'Success')  {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.userHeathConditionErrors.push('Please select a valid otpion!')
        } else {
          alert('Unknown Server Error');
        }
      }
    )
  }

  GetConditionList() {
    this.service.GethealthconditionList().subscribe(
      (data: any) => {
      this.conditionsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError() {
    return this.userHeathConditionErrors.length > 0;
  }

}
