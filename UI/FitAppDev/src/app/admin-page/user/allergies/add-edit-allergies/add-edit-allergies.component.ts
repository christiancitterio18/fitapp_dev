import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-allergies',
  templateUrl: './add-edit-allergies.component.html',
  styleUrls: ['./add-edit-allergies.component.css']
})
export class AddEditAllergiesComponent implements OnInit {

  constructor(private service: SharedService) { }
  
  @Input() allergy: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  user_person_id: string = '';
  ingredient_id: string = '';

  ingredientsList: any = [];

  allergiesErrors: any = [];

  ngOnInit(): void {
    this.GetIngredientList();
    this.user_person_id = this.allergy.user_person_id;
    this.ingredient_id = this.allergy.ingredient_id;
  }

  AddAllergy(){
    var data = {
      user_person_id: this.user_person_id,
      ingredient_id: this.ingredient_id
    }

    this.service.PostUserAllergy(data).subscribe(
      (res: any) => {
        if (res == 'Success')  {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.allergiesErrors.push('Please select a valid otpion!')
        } else {
          alert('Unknown Server Error');
        }
      }
    )
  }

  GetIngredientList() {
    this.service.GetIngredientList().subscribe(
      (data: any) => {
        this.ingredientsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError() {
    return this.allergiesErrors.length > 0;
  }

}
