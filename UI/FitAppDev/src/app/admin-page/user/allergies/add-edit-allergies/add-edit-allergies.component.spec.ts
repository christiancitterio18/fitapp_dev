import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditAllergiesComponent } from './add-edit-allergies.component';

describe('AddEditAllergiesComponent', () => {
  let component: AddEditAllergiesComponent;
  let fixture: ComponentFixture<AddEditAllergiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditAllergiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditAllergiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
