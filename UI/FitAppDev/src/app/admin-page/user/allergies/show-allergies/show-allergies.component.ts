import { Component, OnInit, Input } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-allergies',
  templateUrl: './show-allergies.component.html',
  styleUrls: ['./show-allergies.component.css']
})
export class ShowAllergiesComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() id !: string;

  userId: string = '';
  
  allergiesList: any = [];
  ingredientsList: any = [];

  modalTitle: string = '';
  activateAddEditAllergies: boolean = false;
  allergy: any;

  ngOnInit(): void {
    this.userId = this.id;
    this.RefreshDataList();
  }

  AddClick(){
    this.allergy = {
      user_person_id: this.userId,
      ingredient_id: ''
    };
    this.modalTitle = 'Add allergy';
    this.activateAddEditAllergies = true;
  }

  CloseClick(){
    this.activateAddEditAllergies = false;
    this.RefreshDataList();
  }

  RefreshDataList() {
    var dataSet: any = [];
    this.service.GetIngredientList().subscribe(
      (data: any) => {
        this.ingredientsList = data;
        this.service.GetUserAllergiesList().subscribe(
          (data: any) => {
            dataSet = data;
            this.allergiesList = dataSet.filter((element: any) => {
              return element.user_person_id == this.userId;
            });
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }
  
  DeleteClick(item: any){ 
    let val = {
      user_person_id: item.user_person_id,
      ingredient_id: item.ingredient_id
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteUserAllergy(val).subscribe(
        (data: any) => {
          if(data == 'Success'){
            alert('Deleted');
            this.RefreshDataList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetAllergyName(item: any) {
    return this.ingredientsList.find((element: any) => {
      return element.id == item.ingredient_id;
    }).ingredient;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissAllergies')?.click();
  }
}
