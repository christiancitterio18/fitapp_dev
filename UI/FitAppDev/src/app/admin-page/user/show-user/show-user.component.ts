import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-user',
  templateUrl: './show-user.component.html',
  styleUrls: ['./show-user.component.css']
})
export class ShowUserComponent implements OnInit {

  constructor(private service: SharedService) { }

  usersList: any = [];
  usersListWithoutFilter: any = [];

  modalTitle: string = "";
  activateAddEditUser: Boolean = false;
  changePass: boolean = true;
  
  user: any = {};

  usernameFilter:string="";
  nameFilter:string="";
  surnameFilter:string="";
  emailFilter:string="";
  phoneFilter:string="";


  ngOnInit(): void {
    this.RefreshUserList();
  }

  AddClick(){
    this.user = {
      person_id: 0,
      username: '',
      password: '',
      authorization_id: '',
      person: {
        id: 0,
        name: '',
        surname: '',
        birth_date: '',
        sex: '',
        email: '',
        phone: ''
      }
    };

    this.modalTitle = 'Add User';
    this.activateAddEditUser = true;
    this.changePass = false;
  }

  EditClick(item: any) {
    this.activateAddEditUser = true;
    this.user = item;
    this.modalTitle = 'Edit User';
  }

  ChangePassClick(item: any) {
    this.changePass = true;
    this.user = item;
    this.modalTitle = 'Change Password'
  }

  CloseClick(){
    this.activateAddEditUser = false;
    this.changePass = false;
    this.RefreshUserList();
  }

  RefreshUserList() {
    var users: any = [];
    var persons: any = [];
    var list: any = [];
    this.service.GetPersonList().subscribe(
      (data: any) => {
        persons = data
        this.service.GetUserList().subscribe(
          (data: any) => {
            users = data;
            users.forEach((element: any) => {
                var prs = persons.find((obj:any) => {
                  return obj.id === element.person_id;
                });
                var userData = {
                  person_id: element.person_id,
                  username: element.username,
                  password: element.password,
                  authorization_id: element.authorization_id,
                  person: {}
                };
              userData.person = prs;
              list.push(userData);
              });
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
        this.usersList = list;
        this.usersListWithoutFilter = list;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){
    var val = {
      person_id: item.person_id,
      username: item.username,
      password: item.password,
      authorization_id: item.authorization_id,
    };
    var personData = {
        id: item.person.id,
        name: item.person.name,
        surname: item.person.surname,
        birth_date: item.person.birth_date,
        sex: item.person.sex,
        email: item.person.email,
        phone: (item.person.phone) ? item.person.phone : ''
    };
    
    if (confirm('Attention!! This will delete only the person data,\n'
      + 'that are linked to the deleted user\n'
      + 'Continue?')) {
      this.service.DeleteUser(val).subscribe(
        (data: any) => {
          if (data == 'Success') {
            this.service.DeletePerson(personData).subscribe(
              (res: any) => {
                if (res == 'Success') {
                  alert('Deleted');
                  this.RefreshUserList();
                } else {
                  alert('Something went wrong, please try again later!');
                }
              }
            );
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  FilterFn(){

    var usernameFilter = this.usernameFilter;
   
    this.usersList = this.usersListWithoutFilter.filter(function 
      (el: any){
        return el.username.toString().toLowerCase().includes(
          usernameFilter.toString().trim().toLowerCase()
        )||
        el.person.name.toString().toLowerCase().includes(
          usernameFilter.toString().trim().toLowerCase()
        )||
        el.person.surname.toString().toLowerCase().includes(
          usernameFilter.toString().trim().toLowerCase()
        )||
        el.person.email.toString().toLowerCase().includes(
          usernameFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: boolean) {
    if(tf)
      {document.getElementById('modalDismissUser')?.click();}
    else
      {document.getElementById('modalDismissPass')?.click();}
  }

}