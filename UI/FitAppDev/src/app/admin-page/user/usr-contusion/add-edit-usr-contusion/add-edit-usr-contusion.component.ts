import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-usr-contusion',
  templateUrl: './add-edit-usr-contusion.component.html',
  styleUrls: ['./add-edit-usr-contusion.component.css']
})
export class AddEditUsrContusionComponent implements OnInit {

  constructor(private service: SharedService) { }
  
  @Input() contusion: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  contusion_id: string = '';
  user_person_id: string = '';
  body_part_id: string = '';
  contusion_date: string = '';
  description: string = '';
  is_actual: string = '';

  contusionsList: any = [];
  bodyPartsList: any = []

  userContusionErrors: any = {
    body_part_id: new Array(),
    contusion_date: new Array(),
    description: [],
    contusion_id: new Array(),
  }

  ngOnInit(): void {
    this.GetContusionList();
    this.GetBodyPartList();
    this.contusion_id = this.contusion.contusion_id;
    this.user_person_id = this.contusion.user_person_id;
    this.body_part_id = this.contusion.body_part_id;
    this.contusion_date = this.contusion.contusion_date;
    this.description = this.contusion.description;
    this.is_actual = this.contusion.is_actual;
  }

  AddContusion(){
    var data = {
      contusion_id: this.contusion_id,
      user_person_id: this.user_person_id,
      body_part_id: this.body_part_id,
      contusion_date: this.contusion_date,
      description: this.description,
      is_actual: this.is_actual
    }

    this.service.PostUserContusion(data).subscribe(
      (res: any) => {
        if (res == 'Success')  {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors.body_part_id) {
            this.userContusionErrors.body_part_id = [];
            this.userContusionErrors.body_part_id.push('Please select a valid otpion!');
          }
          if (err.error.errors.contusion_date) {
            this.userContusionErrors.contusion_date = [];
            this.userContusionErrors.contusion_date.push('Please select a valid date!');
          }
          if (err.error.errors.contusion_id) {
            this.userContusionErrors.contusion_id = [];
            this.userContusionErrors.contusion_id.push('Please select a valid otpion!');
          }
          if(err.error.errors.description){
            this.userContusionErrors.description = err.error.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    )
  }

 UpdateContusion(){
    var data = {
      contusion_id: this.contusion.contusion_id,
      user_person_id: this.user_person_id,
      body_part_id: this.contusion.body_part_id,
      contusion_date: this.contusion_date,
      description: this.description,
      is_actual: this.is_actual
    }

   this.service.PostUserContusion(data).subscribe(
     (res: any) => {
       if (res == 'Success') {
         this.successForm.emit(true);
       } else {
         alert('Something went wrong, please try again later!');
       }
     },
     (err: any) => {
       if (err.error.status == 400) {
         if (err.error.errors.body_part_id) {
           this.userContusionErrors.body_part_id = [];
           this.userContusionErrors.body_part_id.push('Please select a valid otpion!');
         }
         if (err.error.errors.contusion_date) {
           this.userContusionErrors.contusion_date = [];
           this.userContusionErrors.contusion_date.push('Please select a valid date!');
         }
         if (err.error.errors.contusion_id) {
           this.userContusionErrors.contusion_id = [];
           this.userContusionErrors.contusion_id.push('Please select a valid otpion!');
         }
         if (err.error.errors.description) {
           this.userContusionErrors.description = err.error.errors.description;
         }
       } else {
         alert('Unknown Server Error');
       }
     }
   );
  }

  GetContusionList() {
    this.service.GetContusionsList().subscribe(
      (data: any) => {
        this.contusionsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetBodyPartList() {
    this.service.GetBodyPartList().subscribe(
      (dataList: any) => {
        this.bodyPartsList = dataList;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
