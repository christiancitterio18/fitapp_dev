import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditUsrContusionComponent } from './add-edit-usr-contusion.component';

describe('AddEditUsrContusionComponent', () => {
  let component: AddEditUsrContusionComponent;
  let fixture: ComponentFixture<AddEditUsrContusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditUsrContusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditUsrContusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
