import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsrContusionComponent } from './usr-contusion.component';

describe('UsrContusionComponent', () => {
  let component: UsrContusionComponent;
  let fixture: ComponentFixture<UsrContusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsrContusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(UsrContusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
