import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-usr-contusion',
  templateUrl: './usr-contusion.component.html',
  styleUrls: ['./usr-contusion.component.css']
})
export class UsrContusionComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }

}
