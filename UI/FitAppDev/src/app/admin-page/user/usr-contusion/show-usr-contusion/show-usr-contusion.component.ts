import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-usr-contusion',
  templateUrl: './show-usr-contusion.component.html',
  styleUrls: ['./show-usr-contusion.component.css']
})
export class ShowUsrContusionComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() id !: string;

  userId: string = '';
  
  contusionsList: any = [];
  bodyPartsList: any = [];
  dataList: any = [];

  modalTitle: string = '';
  activateAddEditContusion: boolean = false;
  contusion: any;

  ngOnInit(): void {
    this.userId = this.id;
    this.RefreshDataList();
  }

  AddClick(){
    this.contusion = {
      contusion_id: '',
      user_person_id: this.userId,
      body_part_id: '',
      contusion_date: '',
      description: '',
      is_actual: 1
    };

    this.modalTitle = 'Add contusion';
    this.activateAddEditContusion = true;
  }

  EditClick(item: any){
    this.contusion = {
      contusion_id: item.contusion_id,
      user_person_id: item.user_person_id,
      body_part_id: item.body_part_id,
      contusion_date: item.contusion_date,
      description: item.description,
      is_actual: item.is_actual
    };
    this.modalTitle = 'Add contusion';
    this.activateAddEditContusion = true;
  }

  CloseClick(){
    this.activateAddEditContusion = false;
    this.RefreshDataList();
  }

  RefreshDataList() {
    var dataSet: any = [];

    this.service.GetBodyPartList().subscribe(
      (data: any) => {
        this.bodyPartsList = data;
      },
      (err: any) => {

      }
    );

    this.service.GetContusionsList().subscribe(
      (data: any) => {
        this.contusionsList = data;
      
        this.service.GetUserContusionsList().subscribe(
          (data2: any) => {
            dataSet = data2;
            this.dataList = dataSet.filter((element: any) => {
              return (element.user_person_id == this.userId && element.is_actual == 1);
            });
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }
  
  DeleteClick(item: any){ 
    let val = {
      contusion_id: item.contusion_id,
      user_person_id: item.user_person_id,
      body_part_id: item.body_part_id,
      contusion_date: item.contusion_date,
      description: item.description,
      is_actual: item.is_actual
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteUserContusion(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert('Deleted');
            this.RefreshDataList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetContusionName(item: any) {
    return this.contusionsList.find((element: any) => {
      return (element.id == item.contusion_id);
    }).contusion;
  }

  GetBodyPartName(item: any) {
    return this.bodyPartsList.find((element: any) => {
      return (element.id == item.body_part_id);
    }).body_part;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissUserContusion')?.click();
  }

}
