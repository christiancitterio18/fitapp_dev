import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowUsrContusionComponent } from './show-usr-contusion.component';

describe('ShowUsrContusionComponent', () => {
  let component: ShowUsrContusionComponent;
  let fixture: ComponentFixture<ShowUsrContusionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowUsrContusionComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowUsrContusionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
