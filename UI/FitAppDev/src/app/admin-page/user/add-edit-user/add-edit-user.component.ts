import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-user',
  templateUrl: './add-edit-user.component.html',
  styleUrls: ['./add-edit-user.component.css']
})
export class AddEditUserComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() user: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  person_id: string = '';
  username: string = '';
  password: string = '';
  authorization_id: string = '';
  id: string = '';
  name: string = '';
  surname: string = '';
  birth_date: string = '';
  sex: string = '';
  email: string = '';
  phone: string = '';
  repeatPass: string = '';

  authorizationsList: any = [];

  userErrors: any = {
    username: [],
    password: [],
    authorization_id: new Array(),
    name: [],
    surname: [],
    birth_date: [],
    sex: new Array(),
    email: [],
    phone: [],
    repeatPass: new Array()
  }

  ngOnInit(): void {
    this.GetAuthorizationList();
    this.person_id = this.user.person_id;
    this.username = this.user.username;
    this.password = this.user.password;
    this.authorization_id = this.user.authorization_id;
    this.id = this.user.person.id;
    this.name = this.user.person.name;
    this.surname = this.user.person.surname;
    this.birth_date = this.user.person.birth_date;
    this.sex = this.user.person.sex;
    this.email = this.user.person.email;
    this.phone = this.user.person.phone;
  }
  
  AddUser() {
    if (this.password === this.repeatPass) {
      var userData = {
        person_id: this.id,
        username: this.email,
        password: this.password,
        authorization_id: this.authorization_id
      };

      var personData = {
        id: this.id,
        name: this.name,
        surname: this.surname,
        birth_date: this.birth_date,
        sex: this.sex,
        email: this.email,
        phone: (this.phone) ? this.phone : null
      };
      this.service.PostPerson(personData).subscribe(
        (res: any) => {
          if(res == 'Success'){
            this.service.PostUser(userData).subscribe(
              (res2: any) => {
                if (res2 == 'Success') {
                  this.successForm.emit(true);
                } else {
                  alert('Something went wrong adding the user account, check the input data and try again later!');
                }
              },
              (err: any) => {
                if (err.error.status == 400) {
                  if (err.error.errors.username) {
                    this.userErrors.username = err.error.errors.username;
                  }
                  if (err.error.errors.password) {
                    this.userErrors.password = err.error.errors.password;
                  }
                  if (err.error.errors.authorization_id) {
                    this.userErrors.authorization_id.push('Please select a valid option!');
                  }
                } else {
                  alert('Unknown Server Error');
                }
              }
            );
          } else {
            if (res.contains("date")) {
              alert(res);
              this.userErrors.birth_date = []
              this.userErrors.birth_date.push('Please select a valid date!');
            } else {
              alert('Something went wrong, check the input data and try again!');
            }
          }
        },
        (err: any) => {
          if (err.error.status == 400) {
            if (err.error.errors.name) {
              this.userErrors.name = err.error.errors.name;
            }
            if (err.error.errors.surname) {
              this.userErrors.surname = err.error.errors.surname;
            }
            if (err.error.errors.birth_date) {
              this.userErrors.birth_date = []
              this.userErrors.birth_date.push('Please select a valid date!');
            }
            if (err.error.errors.sex) {
              this.userErrors.sex = []
              this.userErrors.sex.push('Please select a valid option!');
            }
            if (err.error.errors.email) {
              this.userErrors.email = err.error.errors.email;
            }
            if (err.error.errors.phone) {
              this.userErrors.phone = err.error.errors.phone;
            }
          } else {
            alert('Unknown Server Error');
          }
        }
      );
    }
    else {
      this.userErrors.repeatPass.push('Passwords doesn\'t match!');
    }
  }

  UpdateUser() {
    var userData = {
      person_id: this.person_id,
      username: this.username,
      password: this.password,
      authorization_id: this.authorization_id};
    
    var personData = {
      id: this.id,
      name: this.name,
      surname: this.surname,
      birth_date: this.birth_date,
      sex: this.sex,
      email: this.email,
      phone: (this.phone) ? this.phone : null
    };
    
    this.service.PutUser(userData).subscribe(
      (res: any) => {
          if(res == 'Success'){
            this.service.PutPerson(personData).subscribe(
              (res2: any) => {
                if (res2 == 'Success') {
                  this.successForm.emit(true);
                } else {
                  if (res) {
                    alert(res);
                  } else {
                    alert('Something went wrong adding the user account, check the input data and try again later!');
                  }
                }
              },
              (err: any) => {
                if (err.error.status == 400) {
                  if (err.error.errors.name) {
                    this.userErrors.name = err.error.errors.name;
                  }
                  if (err.error.errors.surname) {
                    this.userErrors.surname = err.error.errors.surname;
                  }
                  if (err.error.errors.birth_date) {
                    this.userErrors.birth_date.push('Please select a valid date!');
                  }
                  if (err.error.errors.sex) {
                    this.userErrors.sex.push('Please select a valid option!');
                  }
                  if (err.error.errors.email) {
                    this.userErrors.email = err.error.errors.email;
                  }
                  if (err.error.errors.phone) {
                    this.userErrors.phone = err.error.errors.phone;
                  }
                } else {
                  alert('Unknown Server Error');
                }
              }
            );
          } else {
            alert('Something went wrong, check the input data and try again!');
          }
        },
        (err: any) => {
          if (err.error.status == 400) {
            if (err.error.errors.username) {
              this.userErrors.username = err.error.errors.username;
            }
            if (err.error.errors.password) {
              this.userErrors.password = err.error.errors.password;
            }
            if (err.error.errors.authorization_id) {
              this.userErrors.authorization_id.push('Please select a valid option!');
            }
          } else {
            alert('Unknown Server Error');
          }
        }
    );
  }

  GetAuthorizationList() {
    this.service.GetAuthorizationList().subscribe(
      (res: any) => {
        this.authorizationsList = res;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }
}
