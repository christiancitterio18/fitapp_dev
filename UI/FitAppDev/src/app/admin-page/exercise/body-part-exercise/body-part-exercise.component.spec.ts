import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BodyPartExerciseComponent } from './body-part-exercise.component';

describe('BodyPartExerciseComponent', () => {
  let component: BodyPartExerciseComponent;
  let fixture: ComponentFixture<BodyPartExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BodyPartExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BodyPartExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
