import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-body-part-exercise',
  templateUrl: './body-part-exercise.component.html',
  styleUrls: ['./body-part-exercise.component.css']
})
export class BodyPartExerciseComponent implements OnInit {

  constructor(private route: ActivatedRoute) { }

  id: any;

  ngOnInit(): void {
    this.id = this.route.snapshot.paramMap.get('id');
  }
}
