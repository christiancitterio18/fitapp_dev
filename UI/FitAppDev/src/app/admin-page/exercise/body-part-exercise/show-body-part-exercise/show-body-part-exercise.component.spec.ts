import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowBodyPartExerciseComponent } from './show-body-part-exercise.component';

describe('ShowBodyPartExerciseComponent', () => {
  let component: ShowBodyPartExerciseComponent;
  let fixture: ComponentFixture<ShowBodyPartExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowBodyPartExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowBodyPartExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
