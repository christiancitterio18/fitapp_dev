import { Component, Input, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-body-part-exercise',
  templateUrl: './show-body-part-exercise.component.html',
  styleUrls: ['./show-body-part-exercise.component.css']
})
export class ShowBodyPartExerciseComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() id: any;

  exerciseId: string = '';

  bodyPartExerciseList: any = [];
  bodyPartList: any = [];

  modalTitle: string = '';
  activateAddEditBodyPartExercise: Boolean = false;
  bodyPartExercise: any;


  ngOnInit(): void {
    this.exerciseId = this.id;
    this.RefreshBodyPartExerciseList();
  }

  AddClick(){
    this.bodyPartExercise = {
      exercise_id: this.exerciseId,
      body_part_id: ''
    };
    this.modalTitle = 'Add BodyPartExercise';
    this.activateAddEditBodyPartExercise = true;
  }

  CloseClick(){
    this.activateAddEditBodyPartExercise = false;
    this.RefreshBodyPartExerciseList();
  }

  RefreshBodyPartExerciseList() {
    var dataList: any = [];
    this.service.GetBodyPartList().subscribe(
      (dataRes: any) => {
        this.bodyPartList = dataRes;
        this.service.GetBodyPartExerciseList().subscribe(
          (data: any) => {
            dataList = data;
            this.bodyPartExerciseList = dataList.filter((element: any) => {
              return element.exercise_id == this.exerciseId;
            });
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );

  }

  DeleteClick(item: any){
    var val = {
      exercise_id: item.exercise_id,
      body_part_id: item.body_part_id
    };
      
    if (confirm('Are you sure??')){
      this.service.DeleteBodyPartExercise(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshBodyPartExerciseList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetBodyPartName(item: any) {
    return this.bodyPartList.find((element: any) => {
      return element.id == item.body_part_id;
    }).body_part;
  }

  SuccessForm(tf: any) {
    document.getElementById('modalDismissBDExercise')?.click();
  }
}
