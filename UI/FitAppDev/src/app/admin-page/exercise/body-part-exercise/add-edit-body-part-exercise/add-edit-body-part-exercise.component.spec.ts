import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditBodyPartExerciseComponent } from './add-edit-body-part-exercise.component';

describe('AddEditBodyPartExerciseComponent', () => {
  let component: AddEditBodyPartExerciseComponent;
  let fixture: ComponentFixture<AddEditBodyPartExerciseComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddEditBodyPartExerciseComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddEditBodyPartExerciseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
