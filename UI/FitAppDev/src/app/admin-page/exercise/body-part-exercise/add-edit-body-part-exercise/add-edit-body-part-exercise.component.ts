import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-body-part-exercise',
  templateUrl: './add-edit-body-part-exercise.component.html',
  styleUrls: ['./add-edit-body-part-exercise.component.css']
})
export class AddEditBodyPartExerciseComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() bodyPartExercise: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  bodyPartList: any = [];

  exercise_id: string = '';
  body_part_id: string = '';

  bpExerciseErrors: any = [];

  ngOnInit(): void {
    this.GetBodyPartList();
    this.exercise_id = this.bodyPartExercise.exercise_id;
    this.body_part_id = this.bodyPartExercise.body_part_id;
  }

  AddBodyPartExercise(){
    var val = {
      exercise_id: this.exercise_id,
      body_part_id: this.body_part_id
    };
    
    this.service.AddBodyPartExercise(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.bpExerciseErrors = [];
          this.bpExerciseErrors.push('Please select a valid option!');
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetBodyPartList() {
    this.service.GetBodyPartList().subscribe(
      (data: any) => {
        this.bodyPartList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    )
  }

  isError() {
    return (this.bpExerciseErrors.length > 0);
  }
}
