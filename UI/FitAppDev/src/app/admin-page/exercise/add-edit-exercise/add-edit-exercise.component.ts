import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-exercise',
  templateUrl: './add-edit-exercise.component.html',
  styleUrls: ['./add-edit-exercise.component.css']
})
export class AddEditExerciseComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() Exercise: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  exercise_category_id: string = '';
  exercise: string = '';
  duration: string = '';
  description: string = '';

  categoriesList: any = [];

  excerciseErrors: any = {
    exercise_category_id: new Array(),
    exercise: [],
    duration: new Array(),
    description: []
  }

  ngOnInit(): void {
    this.GetCategoryList();
    this.id = this.Exercise.id;
    this.exercise_category_id = this.Exercise.exercise_category_id;
    this.exercise = this.Exercise.exercise;
    this.duration = this.Exercise.duration;
    this.description = this.Exercise.description;
  }
 
  AddExercise(){

    let val = {
      id: this.id,
      exercise_category_id: this.exercise_category_id,
      duration: this.duration,
      description: this.description,
      exercise: this.exercise,
    };
    
    this.service.AddExercise(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if(err.error.status == 400){
          if (err.error.errors.exercise_category_id) {
            this.excerciseErrors.exercise_category_id = [];
            this.excerciseErrors.exercise_category_id.push('Please select a valid option!');
          }
          if (err.error.errors.exercise) {
            this.excerciseErrors.exercise = err.error.errors.exercise;
          }
          if (err.error.errors.duration) {
            this.excerciseErrors.duration = [];
            this.excerciseErrors.duration.push('Please enter valid time format! (hh:mm:ss)');
          }
          if (err.error.errors.description) {
            this.excerciseErrors.description = err.errer.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateExercise(){
    let val = {
      id: this.id,
      exercise_category_id: this.exercise_category_id,
      exercise: this.exercise,
      duration: this.duration,
      description: this.description
    };

    this.service.UpdateExercise(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if(err.error.status == 400){
          if (err.error.errors.exercise_category_id) {
            this.excerciseErrors.exercise_category_id = [];
            this.excerciseErrors.exercise_category_id.push('Please select a valid option!');
          }
          if (err.error.errors.exercise) {
            this.excerciseErrors.exercise = err.error.errors.exercise;
          }
          if (err.error.errors.duration) {
            this.excerciseErrors.duration = [];
            this.excerciseErrors.duration.push('Please enter valid time format! (hh:mm:ss)');
          }
          if (err.error.errors.description) {
            this.excerciseErrors.description = err.errer.errors.description;
          }
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  GetCategoryList() {
    this.service.GetExerciseCategoryList().subscribe(
      (data: any) => {
        this.categoriesList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    )
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
