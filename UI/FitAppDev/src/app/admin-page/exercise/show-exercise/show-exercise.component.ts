import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-exercise',
  templateUrl: './show-exercise.component.html',
  styleUrls: ['./show-exercise.component.css']
})
export class ShowExerciseComponent implements OnInit {

  constructor(private service: SharedService) { }

  excercisesList: any = [];
  categoriesList: any = [];
  excerciseListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditExercise: boolean = false;
  Exercise: any;

  exerciseFilter:string="";
 
  ngOnInit(): void {
    this.RefreshExerciseList();
  }

  AddClick(){
    this.Exercise = {
      id: 0,
      exercise_category_id: '',
      exercise: '',
      duration: '',
      description: ''
    };
    this.modalTitle = 'Add Exercise';
    this.activateAddEditExercise = true;
  }

  EditClick(item: any){
    this.Exercise = item;
    this.modalTitle = 'Edit Exercise';
    this.activateAddEditExercise = true;
  }

  CloseClick(){
    this.activateAddEditExercise = false;
    this.RefreshExerciseList();
  }

  RefreshExerciseList() {
    this.service.GetExerciseCategoryList().subscribe(
      (data: any) => {
        this.categoriesList = data;
        this.service.GetExerciseList().subscribe(
          (dataList: any) => {
            this.excercisesList = dataList;
            this.excerciseListWithoutFilter = dataList;
          },
          (err: any) => {
            alert('Unknown Server Error');
          }
        );
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){
    let val = {
      id: item.id,
      exercise_category_id: item.exercise_category_id,
      exercise: item.exercise,
      description: item.description,
      duration: item.duration
    };
      
    if (confirm('Are you sure??')) {
      this.service.DeleteExercise(val).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert("Deleted!");
            this.RefreshExerciseList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  GetCategoryName(item: any) {
    return this.categoriesList.find((element: any) => {
      return element.id == item.exercise_category_id;
    }).category;
  }

  FilterFn(){

    var exerciseFilter = this.exerciseFilter;
  
    
    this.excercisesList = this.excerciseListWithoutFilter.filter((exc: any) =>{
        return exc.exercise.toString().toLowerCase().includes(
          exerciseFilter.toString().trim().toLowerCase()
        ) ||
        this.GetCategoryName(exc).toString().toLowerCase().includes(
          exerciseFilter.toString().trim().toLowerCase()
        ) ||
        exc.description.toString().toLowerCase().includes(
          exerciseFilter.toString().trim().toLowerCase()
        ) ||
        exc.duration.toString().toLowerCase().includes(
          exerciseFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: any) {
    document.getElementById('modalDismissExcercise')?.click();
  }

}


