import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowMealCategoryComponent } from './show-meal-category.component';

describe('ShowMealCategoryComponent', () => {
  let component: ShowMealCategoryComponent;
  let fixture: ComponentFixture<ShowMealCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowMealCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowMealCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
