import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-meal-category',
  templateUrl: './show-meal-category.component.html',
  styleUrls: ['./show-meal-category.component.css']
})
export class ShowMealCategoryComponent implements OnInit {

  constructor(private service: SharedService) { }

  
  mealCategoryList: any = [];
  mealCategoryListWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditMealCategory: boolean = false;
  mealCategory: any;

  mealCategoryFilter:string="";

  ngOnInit(): void {
    this.RefresMealCategoryList();
  }

  AddClick(){
    this.mealCategory = {
      id: 0,
      category: ''
    };
    this.modalTitle = 'Add Meal_Category';
    this.activateAddEditMealCategory = true;
  }

  EditClick(item: any){
    this.mealCategory = item;
    this.modalTitle = 'Edit Meal_Category';
    this.activateAddEditMealCategory = true;
  }

  CloseClick(){
    this.activateAddEditMealCategory = false;
    this.RefresMealCategoryList();
  }

  RefresMealCategoryList() {
    this.service.GetMealCategoryList().subscribe(
      (data: any) => {
        this.mealCategoryList = data;
        this.mealCategoryListWithoutFilter = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  DeleteClick(item: any){
    let val = {
      id: item.id,
      category: item.category
    };
      
    if (confirm('Are you sure??')) {
      this.service.DeleteMealCategory(val).subscribe(
        (data: any) => {
          if (data == 'Success') {
            alert('Deleted');
            this.RefresMealCategoryList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  FilterFn(){
    var mealCategoryFilter = this.mealCategoryFilter;
    
    this.mealCategoryList = this.mealCategoryListWithoutFilter.filter(function (el: any){
        return el.category.toString().toLowerCase().includes(
          mealCategoryFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissMealCategory')?.click();
  }
}

