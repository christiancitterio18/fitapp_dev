import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-meal-category',
  templateUrl: './add-edit-meal-category.component.html',
  styleUrls: ['./add-edit-meal-category.component.css']
})
export class AddEditMealCategoryComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() mealCategory: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  category: string = '';

  mealCategoryErrors: any = [];

  ngOnInit(): void {
    this.id = this.mealCategory.id;
    this.category = this.mealCategory.category;
  }

  AddMealCategory(){
    var val = {
      id: this.id,
      category: this.category
    };
    console.log(val)           
    this.service.AddMealCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.mealCategoryErrors = err.error.errors.category;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  UpdateMealCategory(){
    var val = {id: this.id,
      category: this.category};
    this.service.UpdateMealCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          this.mealCategoryErrors = err.error.errors.category;
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  isError() {
    return this.mealCategoryErrors.length > 0;
  }

}
