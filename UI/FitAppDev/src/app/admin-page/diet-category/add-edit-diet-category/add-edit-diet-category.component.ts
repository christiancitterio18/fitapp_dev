import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-edit-diet-category',
  templateUrl: './add-edit-diet-category.component.html',
  styleUrls: ['./add-edit-diet-category.component.css']
})
export class AddEditDietCategoryComponent implements OnInit {

  constructor(private service:SharedService) { }

  @Input() dietCategory: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  id: string = '';
  category: string = '';

  dietCategoryErrors: any = [];

  ngOnInit(): void {
    this.id = this.dietCategory.id;
    this.category = this.category;
  }

  AddDietCategory(){
    var val = {
      id: this.id,
      category: this.category
    };
    
    this.service.AddDietCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          this.dietCategoryErrors = err.error.errors.category;
        } else {
          alert('Something went wrong, please try again later!');
        }
      }
    );
  }

  UpdateDietCategory(){
    var val = {
      id: this.id,
      category: this.category
    };
    
    this.service.UpdateDietCategory(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          alert('Something went wrong, please try again later!');
        }
      },
      (err: any) => {
        if (err.error.status == 400){
          this.dietCategoryErrors = err.error.errors.category;
        } else {
          alert('Something went wrong, please try again later!');
        }
      }
    );
  }

  isError() {
    return (this.dietCategoryErrors.length > 0);
  }
}
