import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDietCategoryComponent } from './show-diet-category.component';

describe('ShowDietCategoryComponent', () => {
  let component: ShowDietCategoryComponent;
  let fixture: ComponentFixture<ShowDietCategoryComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowDietCategoryComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDietCategoryComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
