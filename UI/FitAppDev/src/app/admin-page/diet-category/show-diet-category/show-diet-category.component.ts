import { Component, OnInit } from '@angular/core';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-show-diet-category',
  templateUrl: './show-diet-category.component.html',
  styleUrls: ['./show-diet-category.component.css']
})
export class ShowDietCategoryComponent implements OnInit {

  constructor(private service:SharedService) { }

  dietCategoriesList:any=[];
  dietCategoriesListWithoutFilter:any=[];
  
  modalTitle: string = '';
  activateAddEditDietCategory:boolean=false;
  dietCategory:any;


  dietCategorFilter:string="";
  

  ngOnInit(): void {
    this.RefreshDietCategoryList();
  }

  AddClick(){
    this.dietCategory={
      id:0,
      category:''
    }
    this.modalTitle='Add Diet Category';
    this.activateAddEditDietCategory=true;
  }

  EditClick(item: any){
    this.dietCategory=item;
    this.modalTitle='Edit Diet Category';
    this.activateAddEditDietCategory=true;
  }

  DeleteClick(item: { id: any; }){
    if(confirm('Are you sure?')){
      this.service.DeleteDietCategory(item.id).subscribe(
        (data: any) => {
          if (data == 'Success') {
            alert("Deleted!");
            this.RefreshDietCategoryList();
          } else {
            alert('Something went wrong, please try again later!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  CloseClick(){
    this.activateAddEditDietCategory=false;
    this.RefreshDietCategoryList();
  }

  RefreshDietCategoryList(){
    this.service.GetDietCategoryList().subscribe(
      (data: any) => {
        this.dietCategoriesList=data;
        this.dietCategoriesListWithoutFilter=data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  FilterFn(){
    var dietCategorFilter = this.dietCategorFilter;
    
    this.dietCategoriesList = this.dietCategoriesListWithoutFilter.filter(function (el: { category: { toString: () => string; }; }){
        return el.category.toString().toLowerCase().includes(
          dietCategorFilter.toString().trim().toLowerCase()
        )
    });
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissDietCategory')?.click();
  }
}
