import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DietCategoryComponent } from './admin-page/diet-category/diet-category.component';
import { ShowDietCategoryComponent } from './admin-page/diet-category/show-diet-category/show-diet-category.component';
import { AddEditDietCategoryComponent } from './admin-page/diet-category/add-edit-diet-category/add-edit-diet-category.component';
import { ShowTrainingCategoryComponent } from './admin-page/training-category/show-training-category/show-training-category.component';
import { AddEditTrainingCategoryComponent } from './admin-page/training-category/add-edit-training-category/add-edit-training-category.component';
import { TrainingCategoryComponent } from './admin-page/training-category/training-category.component';
import { SharedService } from './shared.service';
import { HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import { TrainingComponent } from './admin-page/training/training.component';
import { ShowTrainingComponent } from './admin-page/training/show-training/show-training.component';
import { AddEditTrainingComponent } from './admin-page/training/add-edit-training/add-edit-training.component';
import { ExerciseComponent } from './admin-page/exercise/exercise.component';
import { ShowExerciseComponent } from './admin-page/exercise/show-exercise/show-exercise.component';
import { AddEditExerciseComponent } from './admin-page/exercise/add-edit-exercise/add-edit-exercise.component';
import { TrainingExerciseComponent } from './admin-page/training/training-exercise/training-exercise.component';
import { AddEditTrainingExerciseComponent } from './admin-page/training/training-exercise/add-edit-training-exercise/add-edit-training-exercise.component';
import { ShowTrainingExerciseComponent } from './admin-page/training/training-exercise/show-training-exercise/show-training-exercise.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { DietMealComponent } from './admin-page/diet/diet-meal/diet-meal.component';
import { AddEditDietMealComponent } from './admin-page/diet/diet-meal/add-edit-diet-meal/add-edit-diet-meal.component';
import { ShowDietMealComponent } from './admin-page/diet/diet-meal/show-diet-meal/show-diet-meal.component';
import { BodyPartComponent } from './admin-page/body-part/body-part.component';
import { ShowBodypartComponent } from './admin-page/body-part/show-bodypart/show-bodypart.component';
import { AddEditBodypartComponent } from './admin-page/body-part/add-edit-bodypart/add-edit-bodypart.component';
import { ContusionComponent } from './admin-page/contusion/contusion.component';
import { ShowContusionComponent } from './admin-page/contusion/show-contusion/show-contusion.component';
import { AddEditContusionComponent } from './admin-page/contusion/add-edit-contusion/add-edit-contusion.component';
import { HealthConditionComponent } from './admin-page/health-condition/health-condition.component';
import { ShowHealthConditionComponent } from './admin-page/health-condition/show-health-condition/show-health-condition.component';
import { AddEditHealthConditionComponent } from './admin-page/health-condition/add-edit-health-condition/add-edit-health-condition.component';
import { MicroelementComponent } from './admin-page/microelement/microelement.component';
import { ShowMicroelementComponent } from './admin-page/microelement/show-microelement/show-microelement.component';
import { AddEditMicroelementComponent } from './admin-page/microelement/add-edit-microelement/add-edit-microelement.component';
import { MacroelementComponent } from './admin-page/macroelement/macroelement.component';
import { ShowMacroelementComponent } from './admin-page/macroelement/show-macroelement/show-macroelement.component';
import { AddEditMacroelementComponent } from './admin-page/macroelement/add-edit-macroelement/add-edit-macroelement.component';
import { MealCategoryComponent } from './admin-page/meal-category/meal-category.component';
import { ShowMealCategoryComponent } from './admin-page/meal-category/show-meal-category/show-meal-category.component';
import { AddEditMealCategoryComponent } from './admin-page/meal-category/add-edit-meal-category/add-edit-meal-category.component';
import { MealComponent } from './admin-page/meal/meal.component';
import { ShowMealComponent } from './admin-page/meal/show-meal/show-meal.component';
import { AddEditMealComponent } from './admin-page/meal/add-edit-meal/add-edit-meal.component';
import { BodyPartExerciseComponent } from './admin-page/exercise/body-part-exercise/body-part-exercise.component';
import { ShowBodyPartExerciseComponent } from './admin-page/exercise/body-part-exercise/show-body-part-exercise/show-body-part-exercise.component';
import { AddEditBodyPartExerciseComponent } from './admin-page/exercise/body-part-exercise/add-edit-body-part-exercise/add-edit-body-part-exercise.component';
import { UserComponent } from './admin-page/user/user.component';
import { ShowUserComponent } from './admin-page/user/show-user/show-user.component';
import { AddEditUserComponent } from './admin-page/user/add-edit-user/add-edit-user.component';
import { AuthorizationComponent } from './admin-page/authorization/authorization.component';
import { ShowAuthorizationComponent } from './admin-page/authorization/show-authorization/show-authorization.component';
import { AddAuthorizationComponent } from './admin-page/authorization/add-authorization/add-authorization.component';
import { ChangePasswordComponent } from './admin-page/user/change-password/change-password.component';
import { DietComponent } from './admin-page/diet/diet.component';
import { ShowDietComponent } from './admin-page/diet/show-diet/show-diet.component';
import { AddEditDietComponent } from './admin-page/diet/add-edit-diet/add-edit-diet.component';
import { IngredientComponent } from './admin-page/ingredient/ingredient.component';
import { ShowIngredientComponent } from './admin-page/ingredient/show-ingredient/show-ingredient.component';
import { AddEditIngredientComponent } from './admin-page/ingredient/add-edit-ingredient/add-edit-ingredient.component';
import { IngredientMacronutrientComponent } from './admin-page/ingredient/ingredient-macronutrient/ingredient-macronutrient.component';
import { ShowIngredientMacroComponent } from './admin-page/ingredient/ingredient-macronutrient/show-ingredient-macro/show-ingredient-macro.component';
import { AddEditIngredientMacroComponent } from './admin-page/ingredient/ingredient-macronutrient/add-edit-ingredient-macro/add-edit-ingredient-macro.component';
import { IngredientMicronutrientComponent } from './admin-page/ingredient/ingredient-micronutrient/ingredient-micronutrient.component';
import { ShowIngredientMicroComponent } from './admin-page/ingredient/ingredient-micronutrient/show-ingredient-micro/show-ingredient-micro.component';
import { AddEditIngredientMicroComponent } from './admin-page/ingredient/ingredient-micronutrient/add-edit-ingredient-micro/add-edit-ingredient-micro.component';
import { IngredientmealComponent } from './admin-page/meal/ingredientmeal/ingredientmeal.component';
import { ShowIngredientmealComponent } from './admin-page/meal/ingredientmeal/show-ingredientmeal/show-ingredientmeal.component';
import { AddEditIngredientmealComponent } from './admin-page/meal/ingredientmeal/add-edit-ingredientmeal/add-edit-ingredientmeal.component';
import { AllergiesComponent } from './admin-page/user/allergies/allergies.component';
import { UsrContusionComponent } from './admin-page/user/usr-contusion/usr-contusion.component';
import { UsrConditionComponent } from './admin-page/user/usr-condition/usr-condition.component';
import { ShowAllergiesComponent } from './admin-page/user/allergies/show-allergies/show-allergies.component';
import { AddEditAllergiesComponent } from './admin-page/user/allergies/add-edit-allergies/add-edit-allergies.component';
import { ShowUsrContusionComponent } from './admin-page/user/usr-contusion/show-usr-contusion/show-usr-contusion.component';
import { AddEditUsrContusionComponent } from './admin-page/user/usr-contusion/add-edit-usr-contusion/add-edit-usr-contusion.component';
import { ShorUsrConditionComponent } from './admin-page/user/usr-condition/shor-usr-condition/shor-usr-condition.component';
import { AddEditUsrConditionComponent } from './admin-page/user/usr-condition/add-edit-usr-condition/add-edit-usr-condition.component';
import { PlanTrainingComponent } from './admin-page/plan-training/plan-training.component';
import { ShowPlanTrainingComponent } from './admin-page/plan-training/show-plan-training/show-plan-training.component';
import { AddEditPlanTrainingComponent } from './admin-page/plan-training/add-edit-plan-training/add-edit-plan-training.component';
import { PlanDietComponent } from './admin-page/plan-diet/plan-diet.component';
import { ShowPlanDietComponent } from './admin-page/plan-diet/show-plan-diet/show-plan-diet.component';
import { AddEditPlanDietComponent } from './admin-page/plan-diet/add-edit-plan-diet/add-edit-plan-diet.component';
import { ExcerciseCategoryComponent } from './admin-page/excercise-category/excercise-category.component';
import { ShowExerciseCategoryComponent } from './admin-page/excercise-category/show-exercise-category/show-exercise-category.component';
import { AddEditExerciseCategoryComponent } from './admin-page/excercise-category/add-edit-exercise-category/add-edit-exercise-category.component';
import { CalendarComponent } from './calendar/calendar.component';
import { CalendarModule, DateAdapter } from 'angular-calendar';
import { adapterFactory } from 'angular-calendar/date-adapters/date-fns';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { Data } from './Providers/Data/data';
import { DayViewComponent } from './calendar/day-view/day-view.component';
import { DietViewComponent } from './calendar/day-view/diet-view/diet-view.component';
import { TrainingViewComponent } from './calendar/day-view/training-view/training-view.component';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { AuthService } from './auth.service';
import { LoginComponent } from './nav-bar/login/login.component';
import { RegisterComponent } from './nav-bar/register/register.component';
import { NavBarComponent } from './nav-bar/nav-bar.component';
import { HomepageComponent } from './homepage/homepage.component';
import { ContentComponentUpComponent } from './homepage/content-component-up/content-component-up.component';
import { ContentComponentDownComponent } from './homepage/content-component-down/content-component-down.component';
import { UserPagesComponent } from './user-pages/user-pages.component';
import { AddTrainingCalendarComponent } from './user-pages/add-training-calendar/add-training-calendar.component';
import { AddDietCalendarComponent } from './user-pages/add-diet-calendar/add-diet-calendar.component';
import { AddToComponent } from './user-pages/add-training-calendar/add-to/add-to.component';
import { AddDateComponent } from './user-pages/add-diet-calendar/add-date/add-date.component';
import { UserProfileComponent } from './user-pages/user-profile/user-profile.component';
import { AccountComponent } from './user-pages/user-profile/account/account.component';
import { ProfileContusionsComponent } from './user-pages/user-profile/profile-contusions/profile-contusions.component';
import { ProfileAllergiesComponent } from './user-pages/user-profile/profile-allergies/profile-allergies.component';
import { ProfileConditionsComponent } from './user-pages/user-profile/profile-conditions/profile-conditions.component';
import { ContentComponentLowerComponent } from './homepage/content-component-lower/content-component-lower.component';
import { ShowDietMealsComponent } from './user-pages/add-diet-calendar/show-diet-meals/show-diet-meals.component';
import { ShowTrainingExercisesComponent } from './user-pages/add-training-calendar/show-training-exercises/show-training-exercises.component';



@NgModule({
  declarations: [
    AppComponent,
    DietCategoryComponent,
    ShowDietCategoryComponent,
    AddEditDietCategoryComponent,
    ShowTrainingCategoryComponent,
    AddEditTrainingCategoryComponent,
    TrainingCategoryComponent,
    TrainingComponent,
    ShowTrainingComponent,
    AddEditTrainingComponent,
    ExerciseComponent,
    ShowExerciseComponent,
    AddEditExerciseComponent,
    TrainingExerciseComponent,
    AddEditTrainingExerciseComponent,
    ShowTrainingExerciseComponent,
    AdminPageComponent,
    DietMealComponent,
    AddEditDietMealComponent,
    ShowDietMealComponent,
    BodyPartComponent,
    ShowBodypartComponent,
    AddEditBodypartComponent,
    ContusionComponent,
    ShowContusionComponent,
    AddEditContusionComponent,
    HealthConditionComponent,
    ShowHealthConditionComponent,
    AddEditHealthConditionComponent,
    MicroelementComponent,
    ShowMicroelementComponent,
    AddEditMicroelementComponent,
    MacroelementComponent,
    ShowMacroelementComponent,
    AddEditMacroelementComponent,
    MealCategoryComponent,
    ShowMealCategoryComponent,
    AddEditMealCategoryComponent,
    MealComponent,
    ShowMealComponent,
    AddEditMealComponent,
    BodyPartExerciseComponent,
    ShowBodyPartExerciseComponent,
    AddEditBodyPartExerciseComponent,
    UserComponent,
    ShowUserComponent,
    AddEditUserComponent,
    AuthorizationComponent,
    ShowAuthorizationComponent,
    AddAuthorizationComponent,
    ChangePasswordComponent,
    DietComponent,
    ShowDietComponent,
    AddEditDietComponent,
    IngredientComponent,
    ShowIngredientComponent,
    AddEditIngredientComponent,
    IngredientMacronutrientComponent,
    ShowIngredientMacroComponent,
    AddEditIngredientMacroComponent,
    IngredientMicronutrientComponent,
    ShowIngredientMicroComponent,
    AddEditIngredientMicroComponent,
    IngredientmealComponent,
    ShowIngredientmealComponent,
    AddEditIngredientmealComponent,
    AllergiesComponent,
    UsrContusionComponent,
    UsrConditionComponent,
    ShowAllergiesComponent,
    AddEditAllergiesComponent,
    ShowUsrContusionComponent,
    AddEditUsrContusionComponent,
    ShorUsrConditionComponent,
    AddEditUsrConditionComponent,
    PlanTrainingComponent,
    ShowPlanTrainingComponent,
    AddEditPlanTrainingComponent,
    PlanDietComponent,
    ShowPlanDietComponent,
    AddEditPlanDietComponent,
    ExcerciseCategoryComponent,
    ShowExerciseCategoryComponent,
    AddEditExerciseCategoryComponent,
    CalendarComponent,
    DayViewComponent,
    DietViewComponent,
    TrainingViewComponent,
    LoginComponent,
    RegisterComponent,
    NavBarComponent,
    HomepageComponent,
    ContentComponentUpComponent,
    ContentComponentDownComponent,
    UserPagesComponent,
    AddTrainingCalendarComponent,
    AddDietCalendarComponent,
    AddToComponent,
    AddDateComponent,
    UserProfileComponent,
    AccountComponent,
    ProfileContusionsComponent,
    ProfileAllergiesComponent,
    ProfileConditionsComponent,
    ContentComponentLowerComponent,
    ShowDietMealsComponent,
    ShowTrainingExercisesComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    CalendarModule.forRoot({
      provide: DateAdapter,
      useFactory: adapterFactory,
    })
  ],
  providers: [SharedService, AuthService, Data, {provide: LocationStrategy, useClass: HashLocationStrategy}],
  bootstrap: [AppComponent]
})
export class AppModule { }
