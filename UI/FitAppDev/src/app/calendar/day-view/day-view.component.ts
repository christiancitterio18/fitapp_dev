import { Component, OnInit } from '@angular/core';
import { Data } from '../../Providers/Data/data'

@Component({
  selector: 'app-day-view',
  templateUrl: './day-view.component.html',
  styleUrls: ['./day-view.component.css']
})
export class DayViewComponent implements OnInit {

  isDiet: boolean = false;
  isTraining: boolean = false;

  objectData: any = {};

  constructor(private data: Data) {
    if (data.storage.type == 'diet'){
      this.isDiet = true;
    }
    if (data.storage.type == 'training') {
      this.isTraining = true;
    }

    this.objectData = {
      id: data.storage.id,
      name:  data.storage.title,
      day: data.storage.day
    }
  }

  ngOnInit(): void {
  }

}
