import { Component, Input, OnInit } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-training-view',
  templateUrl: './training-view.component.html',
  styleUrls: ['./training-view.component.css']
})
export class TrainingViewComponent implements OnInit {

  @Input() trainingData: any;

  trainingList: any = [];
  name: string = '';
  day: string = '';

  constructor(private service : SharedService) { }

  ngOnInit(): void {
    this.RefreshList();
    this.name = this.trainingData.name;
    this.day = this.trainingData.day;
  }

  GetExercises() {
    this.service.GetTrainingDayView(this.trainingData).subscribe(
      (data: any) => {
        this.trainingList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  RefreshList() {
    this.GetExercises();
  }

}
