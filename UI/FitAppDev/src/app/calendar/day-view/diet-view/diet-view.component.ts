import { Component, Input, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-diet-view',
  templateUrl: './diet-view.component.html',
  styleUrls: ['./diet-view.component.css']
})
export class DietViewComponent implements OnInit {

  @Input() dietData: any;

  mealsList: any = [];

  name: string = '';
  day: string = '';

  constructor(private service : SharedService) { }

  ngOnInit(): void {
    this.RefreshData();
    this.name = this.dietData.name;
    this.day = this.dietData.day;
  }

  GetMeals(): void {
    this.service.GetDietDayView(this.dietData).subscribe(
      (data: any) => {
        data.sort((a: any, b: any) => {
          return a.category - b.category;
        });
        data.forEach((element:any) => {
          element['ingredients'] = this.GetIngredients(element.id);
          element['nutrients'] = this.GetElements(element.id);
        });
        this.mealsList = data;
        console.log(this.mealsList);
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
  }

  RefreshData(): void {
    this.GetMeals();
  }

  GetIngredients(id: number): any[]{
    let ingredients: any = [];

    this.service.GetMealIngredients(id).subscribe(
      (data: any) => {
        data.forEach((element:any) => {
          ingredients.push(element);
        });
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );

    return ingredients;
  }

  GetElements(id: number): any[]{
    let elements: any = [];
    
    this.service.GetMealElements(id).subscribe(
      (data: any) => {
        data.forEach((element:any) => {
          elements.push(element);
        });
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
    return elements;
  }

}
