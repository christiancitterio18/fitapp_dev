import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarMonthViewBeforeRenderEvent,
  CalendarView
} from 'angular-calendar';
import { SharedService } from '../shared.service';
import { isSameDay, isSameMonth } from 'date-fns';
import { Data } from '../Providers/Data/data';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.css']
})
  
export class CalendarComponent implements OnInit {


  viewDate: Date = new Date();
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  refresh: Subject<any> = new Subject();
  events: CalendarEvent<{ id: number, type: string, event_id: number }>[] = [];
  activeDayIsOpen: boolean = false;

  userTrainings: any = [];
  userDiets: any = [];
  userId: string = this.auth.GetUserID();

  actions: CalendarEventAction[] = [
    {
      label: '<i class="fa fa-trash"></i>',
      a11yLabel: 'Delete',
      onClick: ({ event }: { event: CalendarEvent }): void => {
        if (event.meta.type == 'diet') {
          this.DeleteDiet(event.meta.event_id);
          this.refreshView();
        }
        if (event.meta.type == 'training') {
          this.DeleteTraining(event.meta.event_id);
          this.refreshView();
        }
      },
    }
  ];

  constructor(private service: SharedService, private router: Router, private data: Data , private auth: AuthService) {
  }

  ngOnInit(): void {
    this.refreshView();
  }

  dayClicked({ date, events }: { date: Date; events: CalendarEvent[] }): void {
    if (isSameMonth(date, this.viewDate)) {
      if (
        (isSameDay(this.viewDate, date) && this.activeDayIsOpen === true) ||
        events.length === 0
      ) {
        this.activeDayIsOpen = false;
      } else {
        this.activeDayIsOpen = true;
      }
      this.viewDate = date;
    }
  }

  eventClicked({ event }: { event: CalendarEvent }): void {
    let dataInfo = this.GetData(event.meta.id, event.start, event.meta.type, event.title);
    this.data.storage = dataInfo;
    this.router.navigate(["calendar/day/", dataInfo.day, dataInfo.type]);
  }

  GetData(id: any, start: Date, type: any, title: string): any {
    let diff: number = Math.abs(this.viewDate.valueOf() - start.valueOf());
    diff = Math.ceil(diff / (1000 * 60 * 60 * 24));
    let dataInfo = {
      id: id,
      type: type,
      title: title,
      day: diff + 1
    }
    return dataInfo;
  }

  closeOpenMonthViewDay(): void {
    this.activeDayIsOpen = false;
  }

  refreshView(): void {
    this.events = [];
    this.GetUserDiets();
    this.GetUserTrainings();
    this.refresh.next();
  }

  GetUserDiets(): void{
    this.service.GetUserDietCalendar(this.userId).subscribe(
      (data: any) => {
        this.userDiets = data;
        this.GetDietEvent();
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetUserTrainings(): void{
    this.service.GetUserTrainingCalendar(this.userId).subscribe(
      (data: any) => {
        this.userTrainings = data;
        this.GetTrainingEvent();
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetDietEvent(): void{
    this.userDiets.forEach((element: any) => {
      let diet = {
        title: 'Diet: ' + element.diet,
        start: new Date(element.date_from),
        end: new Date(element.date_to),
        actions: this.actions,
        color: {
          primary: '#46c432',
          secondary: '#b9e9b2'
        },
        allDay: true,
        meta: {
          id: element.diet_id,
          type: 'diet',
          event_id: element.id
        }
      }
      this.events.push(diet);
    });
  }

  GetTrainingEvent(): void{
    this.userTrainings.forEach((element: any) => {
      let training = {
        title: 'Training: ' + element.training,
        start: new Date(element.date_from),
        end: new Date(element.date_to),
        actions: this.actions,
        color: {
          primary: '#1e90ff',
          secondary: '#D1E8FF'
        },
        allDay: true,
        meta: {
          id: element.training_id,
          type: 'training',
          event_id: element.id
        }
      }
      this.events.push(training);
    });
  }

  DeleteDiet(id: any): void {
    if (confirm('This will delete the subscribed diet.\nAre you sure?')) {
      this.service.DeleteDietPlan(id).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert('Deleted');
          } else {
            alert('Something wen\'t wrong, please try again!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }

  DeleteTraining(id: any): void {
    if (confirm('This will delete the subscribed training.\nAre you sure?')) {
      this.service.DeleteTrainingPlan(id).subscribe(
        (data: any) => {
          if (data == 'Success'){
            alert('Deleted');
          } else {
            alert('Something wen\'t wrong, please try again!');
          }
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }
}

