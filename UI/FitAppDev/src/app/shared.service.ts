import { Injectable } from '@angular/core';
import { HttpClient, HttpRequest, HttpHeaders } from '@angular/common/http';
import { Observable} from 'rxjs';
import { validateEvents } from 'angular-calendar/modules/common/util';

@Injectable({
  providedIn: 'root'
})

export class SharedService {
  readonly APIUrl = 'http://54.38.52.4:53431/api';
  readonly tocken = localStorage.getItem('token');
  readonly header = new HttpHeaders().append('Authorization', 'bearer ' + this.tocken);
  readonly options = { headers: this.header }

  constructor( private http: HttpClient) { }

  //Diet Category shared functions
  GetDietCategoryList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/dietCategories/', this.options);
  }

  AddDietCategory(val: any){
    return this.http.post(this.APIUrl + '/dietCategories/', val, this.options);
  }

  UpdateDietCategory(val: any){
    return this.http.put(this.APIUrl + '/dietCategories/', val, this.options);
  }

  DeleteDietCategory(val: any){
    return this.http.delete(this.APIUrl + '/dietCategories/' + val, this.options);
  }

  //Diet Meal shared functions
  GetDietMealList():Observable<any[]>{
    return this.http.get<any>(this.APIUrl+'/dietMeals/', this.options);
  }

  AddDietMeal(val:any){
    return this.http.post(this.APIUrl+'/dietMeals/',val, this.options);
  }

  DeleteDietMeal(val:any){
    return this.http.delete(this.APIUrl+'/dietMeals/'+val.diet_id+'/'+val.meal_id+'/'+val.day, this.options);
  }

  //Training category shared functions
  GetTrainingCategoryList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/trainingCategories/', this.options);
  }

  AddTrainingCategory(val: any){
    return this.http.post(this.APIUrl + '/trainingCategories/', val, this.options);
  }

  UpdateTrainingCategory(val: any){
    return this.http.put(this.APIUrl + '/trainingCategories/', val, this.options);
  }

  DeleteTrainingCategory(val: any){
    return this.http.delete(this.APIUrl + '/trainingCategories/' + val, this.options);
  }

  //Exercise category shared functions
  GetExerciseCategoryList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/exerciseCategories/', this.options);
  }

  AddExerciseCategory(val: any){
    return this.http.post(this.APIUrl + '/exerciseCategories/', val, this.options);
  }

  UpdateExerciseCategory(val: any){
    return this.http.put(this.APIUrl + '/exerciseCategories/', val, this.options);
  }

  DeleteExerciseCategory(val: any){
    return this.http.delete(this.APIUrl + '/exerciseCategories/' + val.id, this.options);
  }

  //Training shared functions
  GetTrainingList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/trainings/', this.options);
  }

  GetTrainingListUser(id: any): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/trainings/get/'+ id, this.options);
  }

  AddTraining(val: any){
    return this.http.post(this.APIUrl + '/trainings/', val, this.options);
  }

  UpdateTraining(val: any){
    return this.http.put(this.APIUrl + '/trainings/', val, this.options);
  }

  DeleteTraining(val: any){
    return this.http.delete(this.APIUrl + '/trainings/delete/' + val, this.options);
  }

  //Exercise shared functions
  GetExerciseList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/exercises/', this.options);
  }

  AddExercise(val: any){
    return this.http.post(this.APIUrl + '/exercises/', val, this.options);
  }

  UpdateExercise(val: any){
    return this.http.put(this.APIUrl + '/exercises/', val, this.options);
  }

  DeleteExercise(val: any){
    return this.http.delete(this.APIUrl + '/exercises/' + val.id, this.options);
  }

  //Training exercise shared functions
  GetTrainingExerciseList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/trainingExercises/', this.options);
  }

  AddTrainingExercise(val: any){
    return this.http.post(this.APIUrl + '/trainingExercises/', val, this.options);
  }

  UpdateTrainingExercise(val: any){
    return this.http.put(this.APIUrl + '/trainingExercises/', val, this.options);
  }

  DeleteTrainingExercise(val: any){
    return this.http.delete(this.APIUrl + '/trainingExercises/' + val.training_id +'/'+ val.exercise_id +'/'+ val.day, this.options);
  }

  //Body part shared functions
  GetBodyPartList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/bodyParts/', this.options);
  }

  AddBodyPart(val: any){
    return this.http.post(this.APIUrl + '/bodyParts/', val, this.options);
  }

  UpdateBodyPart(val: any){
    return this.http.put(this.APIUrl + '/bodyParts/', val, this.options);
  }

  DeleteBodyPart(val: any){
    return this.http.delete(this.APIUrl + '/bodyParts/' + val.id, this.options);
  }

  //Contusion shared functions
  GetContusionsList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/contusions/', this.options);
  }

  AddContusion(val: any){
    return this.http.post(this.APIUrl + '/contusions/', val, this.options);
  }

  UpdateContusion(val: any){
    return this.http.put(this.APIUrl + '/contusions/', val, this.options);
  }

  DeleteContusion(val: any){
    return this.http.delete(this.APIUrl + '/contusions/' + val.id, this.options);
  }

  //Health condition shared functions
  GethealthconditionList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/healthConditions/', this.options);
  }

  AddHealthCondition(val: any){
    return this.http.post(this.APIUrl + '/healthConditions/', val, this.options);
  }

  UpdateHealthCondition(val: any){
    return this.http.put(this.APIUrl + '/healthConditions/', val, this.options);
  }

  DeleteHealthCondition(val: any){
    return this.http.delete(this.APIUrl + '/healthConditions/' + val.id, this.options);
  }

  //Microelemnt shared functions
  GetMicroelementList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/microelements/', this.options);
  }

  AddMicroelement(val: any){
    return this.http.post(this.APIUrl + '/microelements/', val, this.options);
  }

  UpdateMicroelement(val: any){
    return this.http.put(this.APIUrl + '/microelements/', val, this.options);
  }

  DeleteMicroelement(val: any){
    return this.http.delete(this.APIUrl + '/microelements/' + val.id, this.options);
  }

 //Macroelement shared functions
  GetMacroelementList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/macroelements/', this.options);
  }

  AddMacroelement(val: any){
    return this.http.post(this.APIUrl + '/macroelements/', val, this.options);
  }

  UpdateMacroelement(val: any) {
    return this.http.put(this.APIUrl + '/macroelements/', val, this.options);
  }

  DeleteMacroelement(val: any){
    return this.http.delete(this.APIUrl + '/macroelements/' + val.id, this.options);
  }

  //Meal category shared functions
  GetMealCategoryList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/mealCategories/', this.options);
  }

  AddMealCategory(val: any){
    return this.http.post(this.APIUrl + '/mealCategories/', val, this.options);
  }

  UpdateMealCategory(val: any){
    return this.http.put(this.APIUrl + '/mealCategories/', val, this.options);
  }

  DeleteMealCategory(val: any){
    return this.http.delete(this.APIUrl + '/mealCategories/' + val.id, this.options);
  }

  //Meal shared functions
  GetMealList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/meals/', this.options);
  }

  AddMeal(val: any){
    return this.http.post(this.APIUrl + '/meals/', val, this.options);
  }

  UpdateMeal(val: any){
    return this.http.put(this.APIUrl + '/meals/', val, this.options);
  }

  DeleteMeal(val: any){
    return this.http.delete(this.APIUrl + '/meals/' + val.id, this.options);
  }

  //Body_part exercise shared functions
  GetBodyPartExerciseList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/bodyPartExercises/', this.options);
  }

  AddBodyPartExercise(val: any){
    return this.http.post(this.APIUrl + '/bodyPartExercises/', val, this.options);
  }

  DeleteBodyPartExercise(val: any){
    return this.http.delete(this.APIUrl + '/bodyPartExercises/' + val.exercise_id + '/' + val.body_part_id, this.options);
  }

  //User shared functions
  GetUserList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/users/', this.options);
  }

  GetUser(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/users/' + val, this.options);
  }

  PostUser(val: any) {
    return this.http.post(this.APIUrl + '/users/', val, this.options);
  }

  PutUser(val: any) {
    return this.http.put(this.APIUrl + '/users/', val, this.options);
  }

  DeleteUser(val: any) {
    return this.http.delete(this.APIUrl + '/users/' + val.person_id, this.options);
  }

  //Person  shared functions
  GetPersonList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/persons/', this.options);
  }

  GetPerson(val: any): Observable<any> {
    return this.http.get(this.APIUrl + '/persons/' + val, this.options);
  }

  PostPerson(val: any) {
    return this.http.post(this.APIUrl + '/persons/', val, this.options);
  }

  PutPerson(val: any) {
    return this.http.put(this.APIUrl + '/persons/', val, this.options);
  }

  DeletePerson(val: any) {
    return this.http.delete(this.APIUrl + '/persons/' + val.id, this.options);
  }

  //Authorization shared functions
  GetAuthorizationList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/authorizations/', this.options);
  }

  PostAuthorization(val: any) {
    return this.http.post(this.APIUrl + '/authorizations/', val, this.options);
  }

  //Diet shared functions
  GetDietList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/diets/', this.options);
  }
  GetDietListUser(id: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/diets/'+ id , this.options);
  }

  PostDiet(val: any) {
    return this.http.post(this.APIUrl + '/diets/', val, this.options);
  }

  PutDiet(val: any) {
    return this.http.put(this.APIUrl + '/diets/', val, this.options);
  }

  DeleteDiet(val: any) {
    return this.http.delete(this.APIUrl + '/diets/delete/' + val, this.options);
  }

  //Ingredient shared functions
  GetIngredientList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/ingredients/', this.options);
  }

  PostIngredient(val: any) {
    return this.http.post(this.APIUrl + '/ingredients/', val, this.options);
  }

  PutIngredient(val: any) {
    return this.http.put(this.APIUrl + '/ingredients/', val, this.options);
  }

  DeleteIngredient(val: any) {
    return this.http.delete(this.APIUrl + '/ingredients/' + val.id, this.options);
  }

  //Ingredient_meal shared functions
  GetIngredientMealList(): Observable<any[]>{
    return this.http.get<any>(this.APIUrl + '/mealIngredients/', this.options);
  }


  AddIngredientMeal(val: any){
    return this.http.post(this.APIUrl + '/mealIngredients/', val, this.options);
  }


  UpdateIngredientMeal(val: any){
    return this.http.put(this.APIUrl + '/mealIngredients/', val, this.options);
  }


  DeleteIngredientMeal(val: any){
    return this.http.delete(this.APIUrl + '/mealIngredients/' + val.meal_id + '/' + val.ingredient_id, this.options);
  }

  //Ingredient-macro shared functions
  GetIngredientMacronutrientsList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/ingredientMacroelements/', this.options);
  }

  PostIngredientMacronutrient(val: any) {
    return this.http.post(this.APIUrl + '/ingredientMacroelements/', val, this.options);
  }

  PutIngredientMacronutrient(val: any) {
    return this.http.put(this.APIUrl + '/ingredientMacroelements/', val, this.options);
  }

  DeleteIngredientMacronutrient(val: any) {
    return this.http.delete(this.APIUrl + '/ingredientMacroelements/' + val.macroelement_id + '/' + val.ingredient_id, this.options);
  }

  //Ingredient-micro shared functions
  GetIngredientMicronutrientsList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/ingredientMicroelements/', this.options);
  }

  PostIngredientMicronutrient(val: any) {
    return this.http.post(this.APIUrl + '/ingredientMicroelements/', val, this.options);
  }

  PutIngredientMicronutrient(val: any) {
    return this.http.put(this.APIUrl + '/ingredientMicroelements/', val, this.options);
  }

  DeleteIngredientMicronutrient(val: any) {
    return this.http.delete(this.APIUrl + '/ingredientMicroelements/' + val.microelement_id + '/' + val.ingredient_id, this.options);
  }

  //Training plan shared functions
  GetTrainingPlanList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/trainingPlans/', this.options);
  }

  PostTrainingPlan (val: any) {
    return this.http.post(this.APIUrl + '/trainingPlans/', val, this.options);
  }

  PutTrainingPlan (val: any) {
    return this.http.put(this.APIUrl + '/trainingPlans/', val, this.options);
  }

  DeleteTrainingPlan (val: any) {
    return this.http.delete(this.APIUrl + '/trainingPlans/' + val, this.options);
  }

  //Training plan shared functions
  GetDietPlanList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/dietPlans/', this.options);
  }

  PostDietPlan (val: any) {
    return this.http.post(this.APIUrl + '/dietPlans/', val, this.options);
  }

  PutDietPlan (val: any) {
    return this.http.put(this.APIUrl + '/dietPlans/', val, this.options);
  }

  DeleteDietPlan (val: any) {
    return this.http.delete(this.APIUrl + '/dietPlans/' + val, this.options);
  }

  //User allergies shared functions
  GetUserAllergiesList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/userAllergies/', this.options);
  }

  PostUserAllergy(val: any) {
    return this.http.post(this.APIUrl + '/userAllergies/', val, this.options);
  }

  DeleteUserAllergy(val: any) {
    return this.http.delete(this.APIUrl + '/userAllergies/' + val.user_person_id +'/' + val.ingredient_id, this.options);
  }

  //User health conditions shared functions
  GetUserHealthConditionsList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/userHealthConditions/', this.options);
  }

  PostUserHealthCondition(val: any) {
    return this.http.post(this.APIUrl + '/userHealthConditions/', val, this.options);
  }

  DeleteUserHealthCondition(val: any) {
    return this.http.delete(this.APIUrl + '/userHealthConditions/' + val.health_condition_id +'/' + val.user_person_id, this.options);
  }

  //User contusions shared functions
  GetUserContusionsList(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/userContusions/', this.options);
  }

  PostUserContusion(val: any) {
    return this.http.post(this.APIUrl + '/userContusions/', val, this.options);
  }

  PutUserContusion(val: any) {
    return this.http.put(this.APIUrl + '/userContusions/', val, this.options);
  }

  DeleteUserContusion(val: any) {
    return this.http.delete(this.APIUrl + '/userContusions/' + val.contusion_id +'/' + val.user_person_id + '/' +  val.body_part_id, this.options);
  }

  //Custom API for calendar

  GetUserDietCalendar(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/dietPlans/calendar/' + val, this.options);
  }

  GetUserTrainingCalendar(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/trainingPlans/calendar/' + val, this.options);
  }

  //Custom API for DayView
  GetDietDayView(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/dietMeals/' + val.id + '/' + val.day, this.options);
  }
  GetTrainingDayView(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/trainingExercises/' + val.id + '/' + val.day, this.options);
  }
  GetMealIngredients(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/mealIngredients/' + val, this.options);
  }
  GetMealElements(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/meals/' + val, this.options);
  }

  GetDietView(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/diet/' + val , this.options);
  }
  GetTrainingView(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/training/' + val, this.options);
  }

  //Custom API for homepage
  GetHomeDayMeals(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/dietMealsHome/' + val, this.options);
  }

  GetHomeDayExercise(val: any): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/trainingExercisesHome/' + val, this.options);
  }

  GetFooterDiet(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/dietsStat/', this.options);
  }

  GetFooterTrainings(): Observable<any[]> {
    return this.http.get<any>(this.APIUrl + '/trainingsStat/', this.options);
  }

  //API for authentication
  AppLogin(val: any) {
    return this.http.post(this.APIUrl + '/login', val);
  }

  AppRegister(val: any) {
    return this.http.post(this.APIUrl + "/register", val);
  }

}
