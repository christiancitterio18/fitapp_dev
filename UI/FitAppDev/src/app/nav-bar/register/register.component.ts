import { Component, EventEmitter, OnInit, Output } from '@angular/core';
import { AuthService } from '../../auth.service';
import { SharedService } from '../../shared.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @Output() successForm: EventEmitter<any> = new EventEmitter();

  username: string = '';
  password: string = '';
  repeatPass: string = '';

  name: string = '';
  surname: string = '';
  birth_date: string = '';
  sex: string = '';
  email: string = '';
  phone: string = '';

  
  labelText: string = '';
  
  repeatPassError: any = [];


  constructor(public auth: AuthService, private service: SharedService) { }

  ngOnInit(): void {
  }

  SignUp() {
    
    let data = this.PrepareData();

    if (this.repeatPass === this.password) {

      this.auth.Register(data);
      
    } else {
      this.repeatPassError = [];
      this.repeatPassError.push('Passwords doesn\'t match!');
    }
    
  }

  PrepareData(): any {
    let user = {
      person_id: '0',
      username: this.email,
      password: this.password,
      authorization_id: '4'
    };

    let person = {
      id: '0',
      name: this.name,
      surname: this.surname,
      birth_date: this.birth_date,
      sex: this.sex,
      email: this.email,
      phone: this.phone
    }

    return { person: person, user: user };
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }
}
