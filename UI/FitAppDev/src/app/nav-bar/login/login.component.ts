import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { AuthService } from '../../auth.service';
import { Md5 } from 'ts-md5/dist/md5';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  @Output() successForm: EventEmitter<any> = new EventEmitter();

  username: string = '';
  password: string = '';

  
  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

    
  LogIn() {

    let userData: any = {
      username: this.username,
      password: Md5.hashStr(this.password).toString().toUpperCase()
    }

    this.auth.Login(userData);
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }

}
