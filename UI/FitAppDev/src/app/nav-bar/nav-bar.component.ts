import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from '../auth.service';

@Component({
  selector: 'app-nav-bar',
  templateUrl: './nav-bar.component.html',
  styleUrls: ['./nav-bar.component.css']
})
export class NavBarComponent implements OnInit {

  title: string = 'FitAppDev';
  activateLogin: boolean = false;
  activateRegister: boolean = false;
  modalTitle: string = '';

  constructor(public auth: AuthService, private router: Router) {
  }

  ngOnInit(): void {
  }

  
  SignInClick() {
    this.activateRegister = false;
    this.activateLogin = true;
    this.modalTitle = 'Login';
  }

  SignUpClick() {
    this.activateLogin = false;
    this.activateRegister = true;
    this.modalTitle = 'Register';
  }

  CloseClick() {
    this.activateLogin = false;
    this.activateRegister = false;
    document.location.reload();
  }

  SignOutClick() {
    this.auth.Logout();
    document.location.reload();
  }

  SuccessForm(tf: boolean) {
    if(this.auth.IsLoggedIn()) {
      document.getElementById('modalDimissLogSignIn')?.click();
    }
  }

}
