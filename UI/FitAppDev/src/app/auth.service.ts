import { Injectable } from '@angular/core';
import { SharedService } from './shared.service';
import { JwtHelperService } from '@auth0/angular-jwt';
import { renderFlagCheckIfStmt } from '@angular/compiler/src/render3/view/template';

@Injectable({
  providedIn: 'root'
})
  
export class AuthService {
  
  constructor(private service: SharedService) { }

  
  public loginError: any = {
    username: new Array(),
    password: new Array()
  }

  public registerError: any = {
    password: new Array(),
    name: new Array(),
    surname: new Array(),
    birth_date: new Array(),
    sex: new Array(),
    email: new Array(),
    phone: new Array()
  }
  
  Register(data: any): void {
    let token: string = '';
    
    this.service.AppRegister(data).subscribe(
      (res: any) => {
        token = res;
        if (token.split('.').length === 3) {
          localStorage.setItem('token', token);
          document.getElementById('modalDimissLogSignIn')?.click();
        } else {
          if (res) {
            alert(res);
            this.registerError.birth_date = [];
            this.registerError.birth_date.push('Please select a valid date!');
          } else { 
            alert('Something when\'t wrong, please try again later');
          }
        }
      },
      (err: any) => {
        if (err.error.status == 400) {
          if (err.error.errors['app_user.password']) {
            this.registerError.password = err.error.errors['app_user.password'];
          }
          if (err.error.errors['person.name']) {
            this.registerError.name = err.error.errors['person.name'];
          }
          if (err.error.errors['person.surname']) {
            this.registerError.surname = err.error.errors['person.surname'];
          }
          if (err.error.errors['person.birth_date']) {
            this.registerError.birth_date = [];
            this.registerError.birth_date.push('Please select a valid date!');
          }
          if (err.error.errors['person.sex']) {
            this.registerError.sex = [];
            this.registerError.sex.push('Please select a valid option!');
          }
          if (err.error.errors['person.email']) {
            this.registerError.email = err.error.errors['person.email'];
          }
          if (err.error.errors['person.phone']) {
            this.registerError.phone = err.error.errors['person.phone'];
          }
        } else if (err.error.message) {
          this.registerError.username = [err.error.message];
          this.registerError.password = [err.error.message];
          this.registerError.name = [err.error.message];
          this.registerError.surname = [err.error.message];
          this.registerError.birth_date = [err.error.message];
          this.registerError.sex = [err.error.message];
          this.registerError.email = [err.error.message];
          this.registerError.phone = [err.error.message];
        } else {
          alert('Unknown Server Error');
        }
      }
    );
  }

  Login(data: any): void {
    
    let token: string = '';

    this.service.AppLogin(data).subscribe(
      (res: any) => {
        token = res;
        if (token.split('.').length === 3) {
          localStorage.setItem('token', token);
          document.getElementById('modalDimissLogSignIn')?.click();
        } else {
          alert('Something when\'t wrong, please try again later')
        }
      },
      (err: any) => {
        console.log('err');
        if (err.error.status == 400) {
          if (err.error.errors.username) {
            this.loginError.username = err.error.errors.username;
          }
          if (err.error.errors.password) {
            this.loginError.password = err.error.errors.password;
          }
        } else if (err.error.message) {
          this.loginError.username = [err.error.message];
          this.loginError.password = [err.error.message];
        } else {
          alert('Unknown Server Error');
        }
      }

    );
  }
  
  Logout(): void {
    localStorage.removeItem('token');
  }

  GetUserID(): string {
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');

    if (!token) {
      return '';
    }

    let decoded = helper.decodeToken(token);
    let result = decoded['id'];

    return result;
  }

  IsLoggedIn(): boolean {

    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');

    if (!token) {
      return false;
    }

    let expired = helper.isTokenExpired(token);

    return !expired;
  }

  IsAdmin(): boolean{
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');

    if (!token) {
      return false;
    }

    let decoded = helper.decodeToken(token);

    return ((this.IsLoggedIn() && decoded.auth == 'Administrator')? true : false);
  }

  IsTrainer(): boolean {
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');
    
    if (!token) {
      return false;
    }

    let decoded = helper.decodeToken(token);

    return ((this.IsLoggedIn() && (decoded.auth == 'Trainer')) ? true : false);
  }

  IsDietetician(): boolean {
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');
    
    if (!token) {
      return false;
    }
    
    let decoded = helper.decodeToken(token);

    return ((this.IsLoggedIn() && (decoded.auth == 'Dietician')) ? true : false);
  }

  CanManage() {
    
    let helper = new JwtHelperService();
    let token = localStorage.getItem('token');
    
    if (!token) {
      return false;
    }
    
    let decoded = helper.decodeToken(token);

    let auth = ((decoded.auth == 'Dietician') ||
      (decoded.auth == 'Trainer'));
    
    if (!this.IsLoggedIn()) {
      return false;
    }

    return auth;
  }

  isLogInError() {
    
  }
}
