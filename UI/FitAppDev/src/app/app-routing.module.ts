import { Component, NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { DietCategoryComponent } from './admin-page/diet-category/diet-category.component';
import { TrainingCategoryComponent } from './admin-page/training-category/training-category.component';
import { ExcerciseCategoryComponent } from './admin-page/excercise-category/excercise-category.component';
import { TrainingComponent } from './admin-page/training/training.component';
import { ExerciseComponent } from './admin-page/exercise/exercise.component';
import { TrainingExerciseComponent } from './admin-page/training/training-exercise/training-exercise.component';
import { AdminPageComponent } from './admin-page/admin-page.component';
import { BodyPartComponent } from './admin-page/body-part/body-part.component';
import { ContusionComponent } from './admin-page/contusion/contusion.component';
import { HealthConditionComponent } from './admin-page/health-condition/health-condition.component';
import { MicroelementComponent } from './admin-page/microelement/microelement.component';
import { MacroelementComponent } from './admin-page/macroelement/macroelement.component';
import { MealCategoryComponent } from './admin-page/meal-category/meal-category.component';
import { MealComponent } from './admin-page/meal/meal.component';
import { BodyPartExerciseComponent } from './admin-page/exercise/body-part-exercise/body-part-exercise.component';
import { UserComponent } from './admin-page/user/user.component';
import { AuthorizationComponent } from './admin-page/authorization/authorization.component';
import { DietComponent } from './admin-page/diet/diet.component';
import { DietMealComponent } from './admin-page/diet/diet-meal/diet-meal.component';
import { IngredientComponent } from './admin-page/ingredient/ingredient.component';
import { IngredientmealComponent } from './admin-page/meal/ingredientmeal/ingredientmeal.component';
import { IngredientMicronutrientComponent } from './admin-page/ingredient/ingredient-micronutrient/ingredient-micronutrient.component';
import { IngredientMacronutrientComponent } from './admin-page/ingredient/ingredient-macronutrient/ingredient-macronutrient.component';
import { AllergiesComponent } from './admin-page/user/allergies/allergies.component';
import { UsrConditionComponent } from './admin-page/user/usr-condition/usr-condition.component';
import { UsrContusionComponent } from './admin-page/user/usr-contusion/usr-contusion.component';
import { PlanTrainingComponent } from './admin-page/plan-training/plan-training.component';
import { PlanDietComponent } from './admin-page/plan-diet/plan-diet.component';
import { CalendarComponent } from './calendar/calendar.component';
import { DayViewComponent } from './calendar/day-view/day-view.component';
import { AuthGuard } from './auth.guard';
import { AdminGuard } from './admin.guard';
import { DietiticianGuard } from './dietitician.guard';
import { TrainerGuard } from './trainer.guard';
import { UserGuard } from './user.guard';
import { ManageGuard } from './manage.guard';
import { HomepageComponent } from './homepage/homepage.component';
import { AddDietCalendarComponent } from './user-pages/add-diet-calendar/add-diet-calendar.component';
import { AddTrainingCalendarComponent } from './user-pages/add-training-calendar/add-training-calendar.component';
import { UserProfileComponent } from './user-pages/user-profile/user-profile.component';
import { ShowDietMealsComponent } from './user-pages/add-diet-calendar/show-diet-meals/show-diet-meals.component';
import { ShowTrainingExercisesComponent } from './user-pages/add-training-calendar/show-training-exercises/show-training-exercises.component';



const routes: Routes = [

 
  { 
    path: '',
    component: HomepageComponent 
  },
  { 
    path: 'dietList',
    component: AddDietCalendarComponent
  },
  { 
    path: 'dietList/meals/:id',
    component: ShowDietMealsComponent
  },
  { 
    path: 'trainingList',
    component: AddTrainingCalendarComponent
  },
  { 
    path: 'trainingList/exercises/:id',
    component: ShowTrainingExercisesComponent
  },
  {
    path: 'admin/dietCategories',
    component: DietCategoryComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/trainingCategories',
    component: TrainingCategoryComponent,
    canActivate: [AuthGuard, TrainerGuard]
  },
  {
    path: 'admin/exerciseCategories',
    component: ExcerciseCategoryComponent,
    canActivate: [AuthGuard, TrainerGuard]
  },
  {
    path: 'admin/trainings',
    component: TrainingComponent,
    canActivate: [AuthGuard, TrainerGuard]
  },
  {
    path: 'admin/trainings/exercises/:id',
    component: TrainingExerciseComponent,
    canActivate: [AuthGuard, TrainerGuard]
  },
  {
    path: 'admin/exercises',
    component: ExerciseComponent,
    canActivate: [AuthGuard, TrainerGuard]
  },
  {
    path: 'admin/exercises/bodyParts/:id',
    component: BodyPartExerciseComponent,
    canActivate: [AuthGuard, TrainerGuard]
  },
  {
    path: 'admin',
    component: AdminPageComponent,
    canActivate: [AuthGuard, ManageGuard]
  },
  { 
    path: 'admin/bodyParts',
    component: BodyPartComponent,
    canActivate: [AuthGuard, ManageGuard]
  },
  {
    path: 'admin/contusions',
    component: ContusionComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/healthConditions',
    component: HealthConditionComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/microelements',
    component: MicroelementComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/macroelements',
    component: MacroelementComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/mealCategories',
    component: MealCategoryComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/users',
    component: UserComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/authorizations',
    component: AuthorizationComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/diets',
    component: DietComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/diets/meals/:id',
    component: DietMealComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/ingredients',
    component: IngredientComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/ingredients/macronutrients/:id',
    component: IngredientMacronutrientComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/ingredients/micronutrients/:id',
    component: IngredientMicronutrientComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/meals',
    component: MealComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/meals/ingredients/:id',
    component: IngredientmealComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'admin/diets',
    component: DietComponent,
    canActivate: [AuthGuard, DietiticianGuard]
  },
  {
    path: 'user/allergies/:id',
    component: AllergiesComponent,
    canActivate: [AuthGuard, UserGuard]
  },
  {
    path: 'user/contusions/:id',
    component: UsrContusionComponent,
    canActivate: [AuthGuard, UserGuard]
  },
  {
    path: 'user/healthConditions/:id',
    component: UsrConditionComponent,
    canActivate: [AuthGuard, UserGuard]
  },
  {
    path: 'user/account',
    component: UserProfileComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'admin/trainingPlans',
    component: PlanTrainingComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'admin/dietPlans',
    component: PlanDietComponent,
    canActivate: [AuthGuard, AdminGuard]
  },
  {
    path: 'calendar',
    component: CalendarComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'calendar/day/:day/:type',
    component: DayViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
