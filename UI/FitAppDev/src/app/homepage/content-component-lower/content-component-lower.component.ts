import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-content-component-lower',
  templateUrl: './content-component-lower.component.html',
  styleUrls: ['./content-component-lower.component.css']
})
export class ContentComponentLowerComponent implements OnInit {

  constructor(private service: SharedService, public auth: AuthService) { }

  dietList: any = [];
  trainingsList: any = [];

  ngOnInit(): void {
    this.RefreshDataList();
  }

  RefreshDataList() {
    this.service.GetFooterDiet().subscribe(
      (data: any) => {
        this.dietList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    this.service.GetFooterTrainings().subscribe(
      (data: any) => {
        this.trainingsList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }
}
