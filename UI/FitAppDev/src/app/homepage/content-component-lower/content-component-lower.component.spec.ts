import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentComponentLowerComponent } from './content-component-lower.component';

describe('ContentComponentLowerComponent', () => {
  let component: ContentComponentLowerComponent;
  let fixture: ComponentFixture<ContentComponentLowerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentComponentLowerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentComponentLowerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
