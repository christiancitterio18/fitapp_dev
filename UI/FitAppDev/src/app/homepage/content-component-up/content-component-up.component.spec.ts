import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ContentComponentUpComponent } from './content-component-up.component';

describe('ContentComponentUpComponent', () => {
  let component: ContentComponentUpComponent;
  let fixture: ComponentFixture<ContentComponentUpComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentComponentUpComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentComponentUpComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
