import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';

@Component({
  selector: 'app-content-component-up',
  templateUrl: './content-component-up.component.html',
  styleUrls: ['./content-component-up.component.css']
})
export class ContentComponentUpComponent implements OnInit {

  constructor(public auth: AuthService) { }

  ngOnInit(): void {
  }

}
