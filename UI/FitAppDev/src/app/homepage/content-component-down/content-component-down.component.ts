import { Component, OnInit } from '@angular/core';
import {
  CalendarEvent,
  CalendarEventAction,
  CalendarMonthViewBeforeRenderEvent,
  CalendarView
} from 'angular-calendar';
import { Subject } from 'rxjs';
import { AuthService } from 'src/app/auth.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-content-component-down',
  templateUrl: './content-component-down.component.html',
  styleUrls: ['./content-component-down.component.css']
})
export class ContentComponentDownComponent implements OnInit {

  constructor(public auth: AuthService, private service: SharedService) { }

  viewDate: Date = new Date();
  view: CalendarView = CalendarView.Month;
  CalendarView = CalendarView;
  refresh: Subject<any> = new Subject();
  activeDayIsOpen: boolean = false;

  exerciseDayList: any = [];
  mealsDayList: any = [];

  ngOnInit(): void {
    this.RefreshDataLists();
  }

  RefreshDataLists() {
    if (this.auth.IsLoggedIn()){
      this.service.GetHomeDayExercise(this.auth.GetUserID()).subscribe(
        (data: any) => {
          this.exerciseDayList = data;
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
      this.service.GetHomeDayMeals(this.auth.GetUserID()).subscribe(
        (data: any) => {
          this.mealsDayList = data;
        },
        (err: any) => {
          alert('Unknown Server Error');
        }
      );
    }
  }
}
