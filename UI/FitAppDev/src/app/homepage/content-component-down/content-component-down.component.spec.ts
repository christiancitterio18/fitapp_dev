import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ContentComponentDownComponent } from './content-component-down.component';

describe('ContentComponentDownComponent', () => {
  let component: ContentComponentDownComponent;
  let fixture: ComponentFixture<ContentComponentDownComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ContentComponentDownComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ContentComponentDownComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
