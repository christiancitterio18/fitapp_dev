import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowTrainingExercisesComponent } from './show-training-exercises.component';

describe('ShowTrainingExercisesComponent', () => {
  let component: ShowTrainingExercisesComponent;
  let fixture: ComponentFixture<ShowTrainingExercisesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowTrainingExercisesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowTrainingExercisesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
