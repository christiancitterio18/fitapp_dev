import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-training-exercises',
  templateUrl: './show-training-exercises.component.html',
  styleUrls: ['./show-training-exercises.component.css']
})
export class ShowTrainingExercisesComponent implements OnInit {

  constructor(private service : SharedService, private route: ActivatedRoute) { }

  trainingId: any;
  trainingList: any = [];

  ngOnInit(): void {
    this.trainingId = this.route.snapshot.paramMap.get('id');
    this.RefreshList();
  }

  GetExercises() {
    this.service.GetTrainingView(this.trainingId).subscribe(
      (data: any) => {
        this.trainingList = data;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  RefreshList() {
    this.GetExercises();
  }

}
