import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddTrainingCalendarComponent } from './add-training-calendar.component';

describe('AddTrainingCalendarComponent', () => {
  let component: AddTrainingCalendarComponent;
  let fixture: ComponentFixture<AddTrainingCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddTrainingCalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddTrainingCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
