import { Component,Input, OnInit, Output, EventEmitter } from '@angular/core';
import {SharedService} from 'src/app/shared.service';


@Component({
  selector: 'app-add-to',
  templateUrl: './add-to.component.html',
  styleUrls: ['./add-to.component.css']
})
export class AddToComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() trainingPlan: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  dateNow = new Date();

  id: string = '';
  training_id: string = '';
  app_user_id: string = '';
  date_from: string = '';
  date_to: string = '';
  is_deleted: string = '';
  last_updated: string = '';

  trainingsList: any = [];
  usersList: any = [];

  addTrainingError: any = [];

  ngOnInit(): void {
    this.GetTrainingList();
    this.GetUserList();
    this.id = this.trainingPlan.id;
    this.training_id = this.trainingPlan.training_id;
    this.app_user_id = this.trainingPlan.app_user_id;
    this.date_from = this.trainingPlan.date_from;
    this.date_to = this.trainingPlan.date_to;
    this.is_deleted = this.trainingPlan.is_deleted;
    this.last_updated = this.dateNow.toDateString();
  }

  AddTrainingPlan(){
     let val = {
      id: this.id,
      training_id: this.training_id,
      app_user_id: this.app_user_id,
      date_from: this.date_from,
      date_to: this.GetEndDate(this.training_id),
      is_deleted: 0,
      last_updated: this.last_updated
    };
    
    this.service.PostTrainingPlan(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          if (res) {
            alert(res);
            this.addTrainingError = [];
            this.addTrainingError.push('Please select a valid date!');
          } else {
            alert('Something went wrong adding the user account, check the input data and try again later!');
          }
        }
      },
      (error: any) => {
        if (error['status'] == 400) {
          this.addTrainingError = [];
          this.addTrainingError.push('Please select a valid date!');
        } else {
          alert('Unknown server error!');
        }
      }
    );
  }

  GetTrainingList() {
    this.service.GetTrainingList().subscribe(
      (trainingData: any) => {
        this.trainingsList = trainingData;
      },
      (error: any) => {
        alert('Unknown server error!');
      }
    );
  }

  GetUserList() {
    this.service.GetUserList().subscribe(
      (userData: any) => {
        this.usersList = userData;
      },
      (error: any) => {
        alert('Unknown server error!');
      }
    );
  }

  GetEndDate(trainingId: any) {
    let training = this.trainingsList.filter((el: any) => {
      return el.id == trainingId
    });

    let date = new Date(this.date_from);
    this.dateNow.setDate(date + training[0].duration);

    return date;
  }

  isError() {
    return (this.addTrainingError.length > 0);
  }
}
