import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { Data } from 'src/app/Providers/Data/data';
import {SharedService} from 'src/app/shared.service';

@Component({
  selector: 'app-add-training-calendar',
  templateUrl: './add-training-calendar.component.html',
  styleUrls: ['./add-training-calendar.component.css']
})
export class AddTrainingCalendarComponent implements OnInit {
  constructor(private service: SharedService, public auth: AuthService,
    private data: Data, private router: Router) { }

  trainingsList: any = [];
  categoriesList: any = [];
  trainingsWithoutFilter:any=[];

  modalTitle: string = '';
  activateAddEditTraining: boolean = false;
  Training: any;


  trainingFilter: string = '';
  contusionFilter: boolean = true;
  categoryFilter: string = '';
  durationFilter: string = '';

  activateAddEditPlanTraining: boolean = false;
  trainingPlan: any;

  ngOnInit(): void {
    this.RefreshTrainingList();
  }
    AddToCalendar(item: any){
      this.trainingPlan = {
        id: 0,
        training_id: item.id,
        app_user_id: this.auth.GetUserID(),
        date_from: '',
        date_to: '',
        is_deleted: 0,
        last_updated: ''
      };
    this.modalTitle = 'Add Plan Training';
    this.activateAddEditTraining = true;
  }

  CloseClick(){
    this.activateAddEditTraining = false;
    this.RefreshTrainingList();
  }

  RefreshTrainingList() {
    this.service.GetTrainingCategoryList().subscribe(
      (data: any) => {
        this.categoriesList = data;
        
        if(this.contusionFilter && this.auth.IsLoggedIn()){
          this.service.GetTrainingListUser(this.auth.GetUserID()).subscribe(
            (dataTable: any) => {
              this.trainingsList = dataTable;
              this.trainingsWithoutFilter = dataTable;
            },
            (err: any) => {
              alert('Unknown Server Error');
            } 
          );
        } else {
          this.service.GetTrainingList().subscribe(
            (dataTable: any) => {
              this.trainingsList = dataTable;
              this.trainingsWithoutFilter = dataTable;
            },
            (err: any) => {
              alert('Unknown Server Error');
            } 
          );
        }
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }  

  GetCategoryName(item: any) {
    return this.categoriesList.find((element: any) => {
      return element.id == item.training_category_id;
    }).category;
  }

  FilterByText(){
    var trainingFilter = this.trainingFilter;
    
    this.trainingsList = this.trainingsWithoutFilter.filter((el: any) =>
    {
        return el.training.toString().toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )||
        this.GetCategoryName(el).toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )||
        el.description.toString().toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )||
        el.duration.toString().toLowerCase().includes(
          trainingFilter.toString().trim().toLowerCase()
        )
    });
  }

  FilertByCategory() {
    var category = this.categoryFilter;

    if (category) {
      this.trainingsList = this.trainingsWithoutFilter.filter(
        (el: any) => {
          return this.GetCategoryName(el).toLowerCase() ==
            category.toString().trim().toLowerCase();
        }
      );
    } else {
      this.trainingsList = this.trainingsWithoutFilter.filter(
        (el: any) => {
          return this.GetCategoryName(el).toLowerCase().includes(
            category.toString().trim().toLowerCase()
          );
        }
      );
    }
  }

  FilterByContusion() {
    this.RefreshTrainingList();
  }

  FilterByDuration() {
    
    var duration = this.durationFilter;
    
    if (duration){
      this.trainingsList = this.trainingsWithoutFilter.filter(
        (el: any) => {
          return (el.duration.toString().toLowerCase() ==
            duration.toString().trim().toLowerCase());
        }
      );
    } else {
      this.trainingsList = this.trainingsWithoutFilter.filter(
        (el: any) => {
          return el.duration.toString().toLowerCase().includes(
            duration.toString().trim().toLowerCase());
        }
      );
    }
  }

  Show(item: any) {
    this.router.navigate(["/trainingList/exercises/", item.id]);
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissAddTraining')?.click();
  }

}
