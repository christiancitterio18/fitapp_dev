import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileAllergiesComponent } from './profile-allergies.component';

describe('ProfileAllergiesComponent', () => {
  let component: ProfileAllergiesComponent;
  let fixture: ComponentFixture<ProfileAllergiesComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileAllergiesComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileAllergiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
