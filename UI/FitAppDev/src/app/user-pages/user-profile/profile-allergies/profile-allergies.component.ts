import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-profile-allergies',
  templateUrl: './profile-allergies.component.html',
  styleUrls: ['./profile-allergies.component.css']
})
export class ProfileAllergiesComponent implements OnInit {

  constructor(private service: SharedService, public auth: AuthService) { }

  userId: string = '';
  allergiesList: any = [];
  ingredientsList: any = [];

  modalTitle: string = '';
  activateAddEditAllergies: boolean = false;
  allergy: any;

  ngOnInit(): void {
    this.userId = this.auth.GetUserID();
    this.RefreshDataList();
  }

  AddClick(){
    this.allergy = {
      user_person_id: this.userId,
      ingredient_id: ''
    };
    this.modalTitle = 'Add allergy';
    this.activateAddEditAllergies = true;
  }

  CloseClick(){
    this.activateAddEditAllergies = false;
    this.RefreshDataList();
  }

  RefreshDataList() {
    var dataSet: any = [];
    this.service.GetIngredientList().subscribe((data: any) => {
      this.ingredientsList = data;
      this.service.GetUserAllergiesList().subscribe((data: any) => {
        dataSet = data;
        this.allergiesList = dataSet.filter((element: any) => {
          return element.user_person_id == this.userId;
        });
      }); 
    });  
  }
  
  DeleteClick(item: any){ 
    let val = {
      user_person_id: item.user_person_id,
      ingredient_id: item.ingredient_id
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteUserAllergy(val).subscribe(
        (data: any) => {
          alert(data.toString());
          this.RefreshDataList();
        }
      );
    }
  }

  GetAllergyName(item: any) {
    return this.ingredientsList.find((element: any) => {
      return element.id == item.ingredient_id;
    }).ingredient;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissProfileAllergies')?.click()
  }

}
