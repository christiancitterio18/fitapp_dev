import { Component, Input, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-account',
  templateUrl: './account.component.html',
  styleUrls: ['./account.component.css']
})
export class AccountComponent implements OnInit {
  

  constructor(private service: SharedService, public auth: AuthService) { }

  user: any;
  changePass: boolean = false;
  modalTitle: string = '';
  repeatPass: string = '';

  accountErrors: any = {
    username: new Array(),
    password: new Array(),
    name: new Array(),
    surname: new Array(),
    birth_date: new Array(),
    sex: new Array(),
    email: new Array(),
    phone: new Array()
  }

  ngOnInit(): void {
    this.GetUser()
  }

  GetUser() {
    this.service.GetUser(this.auth.GetUserID()).subscribe(
      (data: any) => {
        this.user = data[0];
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  UpdateUser(usrData: any) {

    if (usrData.password !== this.repeatPass && this.changePass) {
      this.accountErrors.password.push('Passwords doesn\'t match!');
    } else {
      var userData = {
        person_id: usrData.person_id,
        username: usrData.username,
        password: usrData.password,
        authorization_id: usrData.authorization_id
      };
    
      var personData = {
        id: usrData.id,
        name: usrData.name,
        surname: usrData.surname,
        birth_date: usrData.birth_date,
        sex: usrData.sex,
        email: usrData.email,
        phone: (usrData.phone) ? usrData.phone : null
      };

      this.service.PutPerson(personData).subscribe(
        (res: any) => {
          if(res == 'Success'){
            this.service.PutUser(userData).subscribe(
              (res2: any) => {
                if (res2 == 'Success') {
                  alert('Data changed!');
                  this.ResetErrors();
                } else {
                  alert('Something went wrong adding the user account, check the input data and try again later!');
                }
              },
              (err: any) => {
                if (err.error.status == 400) {
                  if (err.error.errors.username) {
                    this.accountErrors.username = err.error.errors.username;
                  }
                  if (err.error.errors.password) {
                    this.accountErrors.password = err.error.errors.password;
                  }
                  if (err.error.errors.authorization_id) {
                    this.accountErrors.authorization_id.push('Please select a valid option!');
                  }
                } else {
                  alert('Unknown Server Error');
                }
              }
            );
          } else {
            alert('Something went wrong, check the input data and try again!');
          }
        },
        (err: any) => {
          this.ResetErrors();
          if (err.error.status == 400) {
            if (err.error.errors.name) {
              this.accountErrors.name = err.error.errors.name;
            }
            if (err.error.errors.surname) {
              this.accountErrors.surname = err.error.errors.surname;
            }
            if (err.error.errors.birth_date) {
              this.accountErrors.birth_date.push('Please select a valid date!');
            }
            if (err.error.errors.sex) {
              this.accountErrors.sex.push('Please select a valid option!');
            }
            if (err.error.errors.email) {
              this.accountErrors.email = err.error.errors.email;
            }
            if (err.error.errors.phone) {
              this.accountErrors.phone = err.error.errors.email;
            }
          } else {
            alert('Unknown Server Error');
          }
        }
      );
    }
  }

  ChangePassClick() {
    if (this.changePass){
      this.changePass = false;
    } else {
      this.changePass = true;
    }
  }

  ResetErrors() {
    this.accountErrors = {
      username: new Array(),
      password: new Array(),
      name: new Array(),
      surname: new Array(),
      birth_date: new Array(),
      sex: new Array(),
      email: new Array(),
      phone: new Array()
    }
  }

  isError(dataList: any[]) {
    return (dataList.length > 0);
  }
}
