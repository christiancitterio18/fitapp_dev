import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileContusionsComponent } from './profile-contusions.component';

describe('ProfileContusionsComponent', () => {
  let component: ProfileContusionsComponent;
  let fixture: ComponentFixture<ProfileContusionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileContusionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileContusionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
