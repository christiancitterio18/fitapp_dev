import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-profile-contusions',
  templateUrl: './profile-contusions.component.html',
  styleUrls: ['./profile-contusions.component.css']
})
export class ProfileContusionsComponent implements OnInit {

  constructor(private service: SharedService, public auth: AuthService) { }

  userId: string = '';
  
  contusionsList: any = [];
  bodyPartsList: any = [];
  dataList: any = [];

  modalTitle: string = '';
  activateAddEditContusion: boolean = false;
  contusion: any;

  ngOnInit(): void {
    this.userId = this.auth.GetUserID();
    this.RefreshDataList();
  }

  AddClick() {
    this.contusion = {
      contusion_id: '',
      user_person_id: this.auth.GetUserID(),
      body_part_id: '',
      contusion_date: '',
      description: '',
      is_actual: 1
    };

    this.modalTitle = 'Add contusion';
    this.activateAddEditContusion = true;
    console.log(this.activateAddEditContusion + " " + this.modalTitle);
  }

  EditClick(item: any){
    this.contusion = {
      contusion_id: item.contusion_id,
      user_person_id: item.user_person_id,
      body_part_id: item.body_part_id,
      contusion_date: item.contusion_date,
      description: item.description,
      is_actual: item.is_actual
    };
    this.modalTitle = 'Edit contusion';
    this.activateAddEditContusion = true;
    console.log(this.activateAddEditContusion + " " + this.modalTitle);
  }

  CloseClick(){
    this.activateAddEditContusion = false;
    this.RefreshDataList();
  }

  RefreshDataList() {
    var dataSet: any = [];

    this.service.GetBodyPartList().subscribe((data: any) => {
      this.bodyPartsList = data;
    });

    this.service.GetContusionsList().subscribe((data: any) => {
      this.contusionsList = data;
      
      this.service.GetUserContusionsList().subscribe((data2: any) => {
        dataSet = data2;
        this.dataList = dataSet.filter((element: any) => {
          return (element.user_person_id == this.userId &&  element.is_actual == 1);
        });
      }); 
    });  
  }
  
  DeleteClick(item: any){ 
    let val = {
      contusion_id: item.contusion_id,
      user_person_id: item.user_person_id,
      body_part_id: item.body_part_id,
      contusion_date: item.contusion_date,
      description: item.description,
      is_actual: item.is_actual
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteUserContusion(val).subscribe(data => {
        alert(data.toString());
        this.RefreshDataList();
      });
    }
  }

  GetContusionName(item: any) {
    return this.contusionsList.find((element: any) => {
      return (element.id == item.contusion_id);
    }).contusion;
  }

  GetBodyPartName(item: any) {
    return this.bodyPartsList.find((element: any) => {
      return (element.id == item.body_part_id);
    }).body_part;
  }

  GetDate(str: string) {
    let date = new Date(str)
    return date.toLocaleDateString();
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissProfileContusion')?.click()
  }

}
