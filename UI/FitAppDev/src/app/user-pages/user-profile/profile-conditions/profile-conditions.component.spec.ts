import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ProfileConditionsComponent } from './profile-conditions.component';

describe('ProfileConditionsComponent', () => {
  let component: ProfileConditionsComponent;
  let fixture: ComponentFixture<ProfileConditionsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ProfileConditionsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ProfileConditionsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
