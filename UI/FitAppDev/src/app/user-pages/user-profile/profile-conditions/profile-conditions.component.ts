import { Component, OnInit } from '@angular/core';
import { AuthService } from 'src/app/auth.service';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-profile-conditions',
  templateUrl: './profile-conditions.component.html',
  styleUrls: ['./profile-conditions.component.css']
})
export class ProfileConditionsComponent implements OnInit {

  constructor(private service: SharedService, public auth: AuthService) { }

  userId: string = '';
  
  conditionsList: any = [];
  dataList: any = [];

  modalTitle: string = '';
  activateAddEditCondition: boolean = false;
  condition: any;

  ngOnInit(): void {
    this.userId = this.auth.GetUserID();
    this.RefreshDataList();
  }

  AddClick(){
    this.condition = {
      health_condition_id: '',
      user_person_id: this.userId,
      is_actual: 1
    };
    this.modalTitle = 'Add health condition';
    this.activateAddEditCondition = true;
  }

  CloseClick(){
    this.activateAddEditCondition = false;
    this.RefreshDataList();
  }

  RefreshDataList() {
    var dataSet: any = [];
    this.service.GethealthconditionList().subscribe((data: any) => {
      this.conditionsList = data;
      this.service.GetUserHealthConditionsList().subscribe((data: any) => {
        dataSet = data;
        this.dataList = dataSet.filter((element: any) => {
          return (element.user_person_id == this.userId  &&  element.is_actual == 1);
        });
      }); 
    });  
  }
  
  DeleteClick(item: any){    
    let val = {
      health_condition_id: item.health_condition_id,
      user_person_id: item.user_person_id,
      is_actual: item.is_actual
    };
    
    if (confirm('Are you sure?')){
      this.service.DeleteUserHealthCondition(val).subscribe(data => {
        alert(data.toString());
        this.RefreshDataList();
      });
    }
  }

  GetConditionName(item: any) {
    return this.conditionsList.find((element: any) => {
      return (element.id == item.health_condition_id);
    }).health_condition;
  }

  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissProfileHealth')?.click()
  }

}
