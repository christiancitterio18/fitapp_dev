import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddDietCalendarComponent } from './add-diet-calendar.component';

describe('AddDietCalendarComponent', () => {
  let component: AddDietCalendarComponent;
  let fixture: ComponentFixture<AddDietCalendarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ AddDietCalendarComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(AddDietCalendarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
