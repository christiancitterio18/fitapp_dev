import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-date',
  templateUrl: './add-date.component.html',
  styleUrls: ['./add-date.component.css']
})
export class AddDateComponent implements OnInit {

  constructor(private service: SharedService) { }

  @Input() planDiet: any;
  @Output() successForm: EventEmitter<any> = new EventEmitter();

  dateNow = new Date();

  id: string = '';
  diet_id: string = '';
  app_user_id: string = '';
  date_from: string = '';
  date_to: string = '';
  is_deleted: string = '';
  last_updated: string = '';

  dietsList: any = [];
  usersList: any = [];

  addDietError: any = [];

  ngOnInit(): void {
    this.GetDietList();
    this.GetUserList();
    this.id = this.planDiet.id;
    this.diet_id = this.planDiet.diet_id;
    this.app_user_id = this.planDiet.app_user_id;
    this.date_from = this.planDiet.date_from;
    this.date_to = this.planDiet.date_to;
    this.is_deleted = this.planDiet.is_deleted;
    this.last_updated = this.dateNow.toDateString();
  }

  AddPlanDiet(){
    let val = {
      id: this.id,
      diet_id: this.diet_id,
      app_user_id: this.app_user_id,
      date_from: this.date_from,
      date_to: this.GetEndDate(this.diet_id),
      is_deleted: 0,
      last_updated: this.last_updated
    };

    this.service.PostDietPlan(val).subscribe(
      (res: any) => {
        if (res == 'Success') {
          this.successForm.emit(true);
        } else {
          if (res) {
            alert(res);
            this.addDietError = [];
            this.addDietError.push('Please select a valid date!');
          } else {
            alert('Something went wrong adding the user account, check the input data and try again later!');
          }
        }
      },
      (error: any) => {
        if (error['status'] == 400) {
          this.addDietError = [];
          this.addDietError.push('Please select a valid date!');
        } else {
          alert('Unknown server error!');
        }
      }
    );
  }

  GetDietList() {
    this.service.GetDietList().subscribe(
      (dietData: any) => {
        this.dietsList = dietData;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetUserList() {
    this.service.GetUserList().subscribe(
      (userData: any) => {
        this.usersList = userData;
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetEndDate(dietId: any) {
    let diet = this.dietsList.filter((el: any) => {
      return el.id == dietId
    });

    console.log(diet)

    let date = new Date(this.date_from);
    this.dateNow.setDate(date + diet[0].duration);

    return date;
  }

  isError() {
    return (this.addDietError.length > 0);
  }

}
