import { ComponentFixture, TestBed } from '@angular/core/testing';

import { ShowDietMealsComponent } from './show-diet-meals.component';

describe('ShowDietMealsComponent', () => {
  let component: ShowDietMealsComponent;
  let fixture: ComponentFixture<ShowDietMealsComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ ShowDietMealsComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(ShowDietMealsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
