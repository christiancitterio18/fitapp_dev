import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-show-diet-meals',
  templateUrl: './show-diet-meals.component.html',
  styleUrls: ['./show-diet-meals.component.css']
})
export class ShowDietMealsComponent implements OnInit {

  constructor(private service : SharedService, private route: ActivatedRoute) { }

  dietId: any;
  mealsList: any = [];

  ngOnInit(): void {
    this.dietId = this.route.snapshot.paramMap.get('id');
    this.RefreshData();
  }

  GetMeals(): void {
    this.service.GetDietView(this.dietId).subscribe(
      (data: any) => {
        data.sort((a: any, b: any) => {
          return a.category - b.category;
        });
        data.forEach((element:any) => {
          element['ingredients'] = this.GetIngredients(element.id);
          element['nutrients'] = this.GetElements(element.id);
        });
        this.mealsList = data;
        console.log(this.mealsList);
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
  }

  RefreshData(): void {
    this.GetMeals();
  }

  GetIngredients(id: number): any[]{
    let ingredients: any = [];

    this.service.GetMealIngredients(id).subscribe(
      (data: any) => {
        data.forEach((element:any) => {
          ingredients.push(element);
        });
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );

    return ingredients;
  }

  GetElements(id: number): any[]{
    let elements: any = [];
    
    this.service.GetMealElements(id).subscribe(
      (data: any) => {
        data.forEach((element:any) => {
          elements.push(element);
        });
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
    
    return elements;
  }

}
