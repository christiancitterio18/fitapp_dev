import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/auth.service';
import { Data } from 'src/app/Providers/Data/data';
import { SharedService } from 'src/app/shared.service';

@Component({
  selector: 'app-add-diet-calendar',
  templateUrl: './add-diet-calendar.component.html',
  styleUrls: ['./add-diet-calendar.component.css']
})
export class AddDietCalendarComponent implements OnInit {

  constructor(private service: SharedService, public auth: AuthService,
    private data: Data, private router: Router) { }

  dietList: any = [];
  dietCategoryList: any = [];
  dietListWithoutFilter:any=[];

  modalTitle: string = '';
  Diet: any;

  
  dietFilter: string = '';
  categoryFilter: string = '';
  allergiesFilter: boolean = true;
  durationFilter: string = '';
    
  activateAddEditPlanDiet: boolean = false;

  planDiet: any;


  ngOnInit(): void {
    this.RefreshDietList();
  }

  AddToCalendar(item: any){
    this.planDiet = {
      id: 0,
      diet_id: item.id,
      app_user_id: this.auth.GetUserID(),
      date_from: '',
      date_to: '',
      is_deleted: 0,
      last_updated: ''
    };

    this.modalTitle = 'Add Plan Diet';
    this.activateAddEditPlanDiet = true;
  }

  CloseClick(){
    this.activateAddEditPlanDiet = false;
    this.RefreshDietList();
  }

  RefreshDietList() {
    this.service.GetDietCategoryList().subscribe(
      (data: any) => {
        this.dietCategoryList = data;
      
        if(this.allergiesFilter && this.auth.IsLoggedIn()){
          this.service.GetDietListUser(this.auth.GetUserID()).subscribe(
            (data: any) => {
              this.dietList = data;
              this.dietListWithoutFilter = data;
            },
            (err: any) => {
              alert('Unknown Server Error');
            }
          );
        } else {
          this.service.GetDietList().subscribe(
            (data: any) => {
              this.dietList = data;
              this.dietListWithoutFilter = data;
            },
            (err: any) => {
              alert('Unknown Server Error');
            }
          );
        }
      },
      (err: any) => {
        alert('Unknown Server Error');
      }
    );
  }

  GetCategory(item: any) {
    return this.dietCategoryList.find((element: any) => {
      return element.id == item.diet_category_id;
    }).category;
  }

  FilterByText(){
    var dietFilter = this.dietFilter;
    
    this.dietList = this.dietListWithoutFilter.filter(
      (el: any) => {
        return el.diet.toString().toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        ) ||
        this.GetCategory(el).toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        ) ||
        el.description.toString().toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        ) ||
        el.duration.toString().toLowerCase().includes(
          dietFilter.toString().trim().toLowerCase()
        );
      },
    );
  }

  FilertByCategory() {
    var category = this.categoryFilter;

    if (category) {
      this.dietList = this.dietListWithoutFilter.filter(
        (el: any) => {
          return this.GetCategory(el).toLowerCase() ==
            category.toString().trim().toLowerCase();
        }
      );
    } else {
      this.dietList = this.dietListWithoutFilter.filter(
        (el: any) => {
          return this.GetCategory(el).toLowerCase().includes(
            category.toString().trim().toLowerCase()
          );
        }
      );
    }
  }

  FilterByAllergies() {
    this.RefreshDietList();
  }

  FilterByDuration() {
    
    var duration = this.durationFilter;
    
    if (duration){
      this.dietList = this.dietListWithoutFilter.filter(
        (el: any) => {
          return (el.duration.toString().toLowerCase() ==
            duration.toString().trim().toLowerCase());
        }
      );
    } else {
      this.dietList = this.dietListWithoutFilter.filter(
        (el: any) => {
          return el.duration.toString().toLowerCase().includes(
            duration.toString().trim().toLowerCase());
        }
      );
    }
  }

  Show(item: any) {
    this.router.navigate(["/dietList/meals/", item.id]);
  }


  SuccessForm(tf: boolean) {
    document.getElementById('modalDismissAddDiet')?.click();
  }
}
