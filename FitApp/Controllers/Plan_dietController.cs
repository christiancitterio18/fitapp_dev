﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Plan_dietController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Plan_dietController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/dietPlans/")]
        public JsonResult Get()
        {
            string query = @"Plan_dietGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //GET for calendar
        [Authorize]
        [HttpGet]
        [Route("/api/dietPlans/calendar/{user_id}")]
        public JsonResult GetCalendar(int user_id)
        {

            //change to procedure
            string query = @"Calendar_diet_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = user_id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }
            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/dietPlans/")]
        public JsonResult Post(Plan_diet planDiet)
        {
            try
            {
                string query = @"Plan_dietPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@diet_id", SqlDbType.Int).Value = planDiet.diet_id;
                        cmd.Parameters.Add("@app_user_id", SqlDbType.Int).Value = planDiet.app_user_id;
                        cmd.Parameters.Add("@date_from", SqlDbType.Date).Value = planDiet.date_from;
                        cmd.Parameters.Add("@date_to", SqlDbType.Date).Value = planDiet.date_to;
                        cmd.Parameters.Add("@is_deleted", SqlDbType.Int).Value = planDiet.is_deleted;
                        cmd.Parameters.Add("@last_updated", SqlDbType.Date).Value = planDiet.last_updated;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/dietPlans/")]
        public JsonResult Put(Plan_diet planDiet)
        {
            try
            {
                string query = @"Plan_dietPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@diet_id", SqlDbType.Int).Value = planDiet.diet_id;
                        cmd.Parameters.Add("@app_user_id", SqlDbType.Int).Value = planDiet.app_user_id;
                        cmd.Parameters.Add("@date_from", SqlDbType.Date).Value = planDiet.date_from;
                        cmd.Parameters.Add("@date_to", SqlDbType.Date).Value = planDiet.date_to;
                        cmd.Parameters.Add("@is_deleted", SqlDbType.Int).Value = planDiet.is_deleted;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = planDiet.id;
                        cmd.Parameters.Add("@last_updated", SqlDbType.Date).Value = planDiet.last_updated;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/dietPlans/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"Plan_dietDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
