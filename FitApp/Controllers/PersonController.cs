﻿using FitApp.Interfaces;
using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class PersonController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUserMethods _userMethods;

        public PersonController(IConfiguration configuration, IUserMethods userMethods)
        {
            _configuration = configuration;
            _userMethods = userMethods;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/persons/")]
        public JsonResult Get()
        {

            string query = @"PersonGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //GET 1 person
        [Authorize]
        [HttpGet]
        [Route("/api/persons/{id}")]
        public JsonResult GetPerson(int id)
        {
            string query = @"Person_select";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/persons/")]
        public JsonResult Post(Person person)
        {
            try
            {
                DataTable table = _userMethods.AddPerson(person, _configuration);

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/persons/")]
        public JsonResult Put(Person person)
        {
            try
            {
                string query = @"PersonPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@name", SqlDbType.NVarChar, 30).Value = person.name;
                        cmd.Parameters.Add("@surname", SqlDbType.NVarChar, 30).Value = person.surname;
                        cmd.Parameters.Add("@birth_Date", SqlDbType.Date).Value = person.birth_Date;
                        cmd.Parameters.Add("@sex", SqlDbType.NVarChar, 1).Value = person.sex;
                        cmd.Parameters.Add("@email", SqlDbType.NVarChar, 30).Value = person.email;
                        cmd.Parameters.Add("@phone", SqlDbType.NVarChar, 30).Value = (person.phone == null) ? "" : person.phone;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = person.id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/persons/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"PersonDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
