﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Ingredient_macroelementController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Ingredient_macroelementController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/ingredientMacroelements/")]
        public JsonResult Get()
        {
            string query = @"Ingredient_macroelementGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/ingredientMacroelements/")]
        public JsonResult Post(Ingredient_macroelement ingredientMacroelement)
        {
            try
            {
                string query = @"Ingredient_macroelementPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@macroelement_id", SqlDbType.Int).Value = ingredientMacroelement.macroelement_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredientMacroelement.ingredient_id;
                        cmd.Parameters.Add("@grams", SqlDbType.NVarChar).Value = ingredientMacroelement.grams;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/ingredientMacroelements/")]
        public JsonResult Put(Ingredient_macroelement ingredientMacroelement)
        {
            try
            {
                string query = @"Ingredient_macroelementPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@macroelement_id", SqlDbType.Int).Value = ingredientMacroelement.macroelement_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredientMacroelement.ingredient_id;
                        cmd.Parameters.Add("@grams", SqlDbType.NVarChar).Value = ingredientMacroelement.grams;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/ingredientMacroelements/{macroelement_id}/{ingredient_id}")]
        public JsonResult Delete(int macroelement_id, int ingredient_id)
        {
            try
            {
                string query = @"Ingredient_macroelementDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@macroelement_id", SqlDbType.Int).Value = macroelement_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredient_id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
