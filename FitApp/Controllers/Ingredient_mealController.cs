﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Ingredient_mealController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Ingredient_mealController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/mealIngredients/")]
        public JsonResult Get()
        {
            string query = @"Ingredient_mealGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/mealIngredients/{meal_id}")]
        public JsonResult GetMealIngredients(int meal_id)
        {
            //Change to procedure
            string query = @"MealID_ingredient_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@meal_id", SqlDbType.Int).Value = meal_id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }



        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/mealIngredients/")]
        public JsonResult Post(Ingredient_meal meal)
        {
            try
            {
                string query = @"Ingredient_mealPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@meal_id", SqlDbType.Int).Value = meal.meal_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = meal.ingredient_id;
                        cmd.Parameters.Add("@grams", SqlDbType.NVarChar).Value = meal.grams;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/mealIngredients/")]
        public JsonResult Put(Ingredient_meal meal)
        {
            try
            {
                string query = @"Ingredient_mealPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@meal_id", SqlDbType.Int).Value = meal.meal_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = meal.ingredient_id;
                        cmd.Parameters.Add("@grams", SqlDbType.NVarChar).Value = meal.grams;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/mealIngredients/{meal_id}/{ingredient_id}")]
        public JsonResult Delete(int meal_id, int ingredient_id)
        {
            try
            {
                string query = @"Ingredient_mealDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@meal_id", SqlDbType.Int).Value = meal_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredient_id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
