﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class MealController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public MealController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/meals/")]
        public JsonResult Get()
        {
            string query = @"MealGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/meals/")]
        public JsonResult Post(Meal meal)
        {
            try
            {
                string query = @"MealPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@meal_category_id", SqlDbType.Int).Value = meal.meal_category_id;
                        cmd.Parameters.Add("@meal", SqlDbType.NVarChar, 32).Value = meal.meal;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar, 4000).Value = meal.description;
                        cmd.Parameters.Add("@calories", SqlDbType.Decimal, 8).Value = meal.calories;
                        cmd.Parameters["@calories"].Precision = 8;
                        cmd.Parameters["@calories"].Scale = 2;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //Custom GET for day view
        [Authorize]
        [HttpGet]
        [Route("/api/meals/{meal_id}")]
        public JsonResult GetMealElements(int meal_id)
        {
            string query = @"Meal_day_elements_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@meal_id", SqlDbType.Int).Value = meal_id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/meals/")]
        public JsonResult Put(Meal meal)
        {
            try
            {
                string query = @"MealPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@meal_category_id", SqlDbType.Int).Value = meal.meal_category_id;
                        cmd.Parameters.Add("@meal", SqlDbType.NVarChar, 32).Value = meal.meal;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar, 4000).Value = meal.description;
                        cmd.Parameters.Add("@calories", SqlDbType.Decimal, 8).Value = meal.calories;
                        cmd.Parameters["@calories"].Precision = 8;
                        cmd.Parameters["@calories"].Scale = 2;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = meal.id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/meals/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"MealDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
