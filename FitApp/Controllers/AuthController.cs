﻿using FitApp.DTOs;
using FitApp.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class AuthController : ControllerBase
    {
        private readonly ITockenService _tockenService;
        private readonly IConfiguration _configuration;
        private readonly IUserMethods _userMethods;
        private readonly IJsonErrorService _jsonErrors;

        public AuthController(ITockenService tockenService, IConfiguration configuration, IUserMethods userMethods, IJsonErrorService jsonError)
        {
            _tockenService = tockenService;
            _configuration = configuration;
            _userMethods = userMethods;
            _jsonErrors = jsonError;
        }

        [AllowAnonymous]
        [HttpPost]
        [Route("/api/login/")]
        public JsonResult Login(LoginDTO loginDto)
        {

            DataTable table = _userMethods.GetAccount(loginDto, _configuration);

            var account = new UserDTO();

            if (table.Rows.Count > 0)
            {
                if (table.Rows[0]["password"].ToString().ToUpper().Equals(loginDto.password))
                {
                    account.id = Int32.Parse(table.Rows[0]["person_id"].ToString());
                    account.username = table.Rows[0]["username"].ToString();
                    account.authorization = table.Rows[0]["authorization"].ToString();

                    return new JsonResult(_tockenService.CreateTocken(account));
                }
                else
                {
                    return _jsonErrors.badRequestResult("Password not correct!");
                }
            }
            else
            {
                return _jsonErrors.badRequestResult("User not found!");
            }

        }

        [AllowAnonymous]
        [HttpPost]
        [Route("/api/register/")]
        public JsonResult Register(RegisterDTO registerDTO)
        {
            try
            {
                DataTable pers = _userMethods.AddPerson(registerDTO.person, _configuration);
                DataTable usr = _userMethods.AddUsers(registerDTO.user, _configuration);

                DataTable table = _userMethods.GetAccount(
                    new LoginDTO
                    {
                        username = registerDTO.user.username,
                        password = _userMethods.GetHash(registerDTO.user.password)
                    },
                    _configuration);

                var account = new UserDTO();

                account.id = Int32.Parse(table.Rows[0]["person_id"].ToString());
                account.username = table.Rows[0]["username"].ToString();
                account.authorization = table.Rows[0]["authorization"].ToString();

                return new JsonResult(_tockenService.CreateTocken(account));

            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
