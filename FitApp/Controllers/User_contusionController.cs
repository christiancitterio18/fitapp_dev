﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class User_contusionController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public User_contusionController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/userContusions/")]
        public JsonResult Get()
        {
            string query = @"User_contusionGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/userContusions/")]
        public JsonResult Post(User_contusion contusion)
        {
            try
            {
                string query = @"User_contusionPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@user_person_id", SqlDbType.Int).Value = contusion.user_person_id;
                        cmd.Parameters.Add("@contusion_id", SqlDbType.Int).Value = contusion.contusion_id;
                        cmd.Parameters.Add("@body_part_id", SqlDbType.Int).Value = contusion.body_part_id;
                        cmd.Parameters.Add("@contusion_date", SqlDbType.Date).Value = contusion.contusion_date;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar, 4000).Value = contusion.description;
                        cmd.Parameters.Add("@is_actual", SqlDbType.SmallInt).Value = contusion.is_actual;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/userContusions/")]
        public JsonResult Put(User_contusion contusion)
        {
            try
            {
                string query = @"User_contusionPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@user_person_id", SqlDbType.Int).Value = contusion.user_person_id;
                        cmd.Parameters.Add("@contusion_id", SqlDbType.Int).Value = contusion.contusion_id;
                        cmd.Parameters.Add("@body_part_id", SqlDbType.Int).Value = contusion.body_part_id;
                        cmd.Parameters.Add("@contusion_date", SqlDbType.Date).Value = contusion.contusion_date;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar, 4000).Value = contusion.description;
                        cmd.Parameters.Add("@is_actual", SqlDbType.SmallInt).Value = contusion.is_actual;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/userContusions/{contusion_id}/{user_person_id}/{body_part_id}")]
        public JsonResult Delete(int contusion_id, int user_person_id, int body_part_id)
        {
            try
            {
                string query = @"User_contusionDEL ";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@user_Person_id", SqlDbType.Int).Value = user_person_id;
                        cmd.Parameters.Add("@contusion_id", SqlDbType.Int).Value = contusion_id;
                        cmd.Parameters.Add("@body_part_Id", SqlDbType.Int).Value = body_part_id;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
