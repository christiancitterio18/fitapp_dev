﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Training_CategoryController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Training_CategoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/trainingCategories/")]
        public JsonResult Get()
        {
            string query = @"Training_categoryGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/trainingCategories/")]
        public JsonResult Post(Training_Category trainingCategory)
        {
            try
            {
                string query = @"Training_categoryPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 32).Value = trainingCategory.category;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/trainingCategories/")]
        public JsonResult Put(Training_Category trainingCategory)
        {
            try
            {
                string query = @"Training_categoryPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 32).Value = trainingCategory.category;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = trainingCategory.id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }

        }

        //DELETE
        [HttpDelete]
        [Route("/api/trainingCategories/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"Training_categoryDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }
    }
}
