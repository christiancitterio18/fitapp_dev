﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Ingredient_microelementController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Ingredient_microelementController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/ingredientMicroelements/")]
        public JsonResult Get()
        {
            string query = @"Ingredient_microelementGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/ingredientMicroelements/")]
        public JsonResult Post(Ingredient_microelement ingredientMicroelement)
        {
            try
            {
                string query = @"Ingredient_microelementPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@microelement_id", SqlDbType.Int).Value = ingredientMicroelement.microelement_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredientMicroelement.ingredient_id;
                        cmd.Parameters.Add("@grams", SqlDbType.NVarChar).Value = ingredientMicroelement.grams;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/ingredientMicroelements/")]
        public JsonResult Put(Ingredient_microelement ingredientMicroelement)
        {
            try
            {
                string query = @"Ingredient_microelementPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@microelement_id", SqlDbType.Int).Value = ingredientMicroelement.microelement_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredientMicroelement.ingredient_id;
                        cmd.Parameters.Add("@grams", SqlDbType.NVarChar).Value = ingredientMicroelement.grams;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/ingredientMicroelements/{microelement_id}/{ingredient_id}")]
        public JsonResult Delete(int microelement_id, int ingredient_id)
        {
            try
            {
                string query = @"Ingredient_microelementDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@microelement_id", SqlDbType.Int).Value = microelement_id;
                        cmd.Parameters.Add("@ingredient_id", SqlDbType.Int).Value = ingredient_id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
