﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Exercise_categoryController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Exercise_categoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/exerciseCategories/")]
        public JsonResult Get()
        {
            string query = @"Exercise_categoryGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/exerciseCategories/")]
        public JsonResult Post(Exercise_category excerciseCategory)
        {
            try
            {
                string query = @"Exercise_categoryPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 32).Value = excerciseCategory.category;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/exerciseCategories/")]
        public JsonResult Put(Exercise_category excerciseCategory)
        {
            try
            {
                string query = @"Exercise_categoryPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = excerciseCategory.id;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 32).Value = excerciseCategory.category;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }

        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/exerciseCategories/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"Exercise_categoryDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }
    }
}
