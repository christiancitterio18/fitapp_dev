﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Plan_trainingController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Plan_trainingController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/trainingPlans/")]
        public JsonResult Get()
        {
            string query = @"Plan_trainingGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/trainingPlans/")]
        public JsonResult Post(Plan_training trainingPlan)
        {
            try
            {
                string query = @"Plan_trainingPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@training_id", SqlDbType.Int).Value = trainingPlan.training_id;
                        cmd.Parameters.Add("@app_user_id", SqlDbType.Int).Value = trainingPlan.app_user_id;
                        cmd.Parameters.Add("@date_from", SqlDbType.Date).Value = trainingPlan.date_from;
                        cmd.Parameters.Add("@date_to", SqlDbType.Date).Value = trainingPlan.date_to;
                        cmd.Parameters.Add("@is_deleted", SqlDbType.SmallInt).Value = trainingPlan.is_deleted;
                        cmd.Parameters.Add("@last_updated", SqlDbType.Date).Value = trainingPlan.last_updated;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //GET for calendar
        [Authorize]
        [HttpGet]
        [Route("/api/trainingPlans/calendar/{user_id}")]
        public JsonResult GetCalendar(int user_id)
        {

            //Change to procedure
            string query = @"Calendar_training_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = user_id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }
            return new JsonResult(table);
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/trainingPlans/")]
        public JsonResult Put(Plan_training trainingPlan)
        {
            try
            {
                string query = @"Plan_trainingPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = trainingPlan.id;
                        cmd.Parameters.Add("@training_id", SqlDbType.Int).Value = trainingPlan.training_id;
                        cmd.Parameters.Add("@app_user_id", SqlDbType.Int).Value = trainingPlan.app_user_id;
                        cmd.Parameters.Add("@date_from", SqlDbType.Date).Value = trainingPlan.date_from;
                        cmd.Parameters.Add("@date_to", SqlDbType.Date).Value = trainingPlan.date_to;
                        cmd.Parameters.Add("@is_deleted", SqlDbType.SmallInt).Value = trainingPlan.is_deleted;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }

        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/trainingPlans/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"Plan_trainingDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }
    }
}
