﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Training_exerciseController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Training_exerciseController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/trainingExercises/")]
        public JsonResult Get()
        {
            string query = @"Training_exerciseGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //Custom GET for dayView
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/trainingExercises/{id}/{day}")]
        public JsonResult GetDay(int id, int day)
        {

            string query = @"Day_Exercise_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@training_id", SqlDbType.Int).Value = id;
                    cmd.Parameters.Add("@day", SqlDbType.Int).Value = day;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //custom GET for home page user
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/trainingExercisesHome/{user_id}")]
        public JsonResult GetHomeExercise(int user_id)
        {

            string query = @"Home_Day_Exercise_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@user_id", SqlDbType.Int).Value = user_id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/trainingExercises/")]
        public JsonResult Post(Training_exercise training_exercise)
        {
            try
            {
                string query = @"Training_exercisePOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@training_id", SqlDbType.Int).Value = training_exercise.training_id;
                        cmd.Parameters.Add("@exercise_id", SqlDbType.Int).Value = training_exercise.exercise_id;
                        cmd.Parameters.Add("@day", SqlDbType.Int).Value = training_exercise.day;
                        cmd.Parameters.Add("@series_number", SqlDbType.Int).Value = training_exercise.series_number;
                        cmd.Parameters.Add("@repetition", SqlDbType.Int).Value = training_exercise.repetition;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/trainingExercises/")]
        public JsonResult Put(Training_exercise training_exercise)
        {
            try
            {
                string query = @"Training_exercisePUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@training_id", SqlDbType.Int).Value = training_exercise.training_id;
                        cmd.Parameters.Add("@exercise_id", SqlDbType.Int).Value = training_exercise.exercise_id;
                        cmd.Parameters.Add("@day", SqlDbType.Int).Value = training_exercise.day;
                        cmd.Parameters.Add("@series_number", SqlDbType.Int).Value = training_exercise.series_number;
                        cmd.Parameters.Add("@repetition", SqlDbType.Int).Value = training_exercise.repetition;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }

        }

        [Authorize]
        [HttpDelete]
        [Route("/api/trainingExercises/{training_id}/{exercise_id}/{day}")]
        public JsonResult Delete(int training_id, int exercise_id, int day)
        {
            try
            {
                string query = @"Training_exerciseDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@training_id", SqlDbType.Int).Value = training_id;
                        cmd.Parameters.Add("@exercise_id", SqlDbType.Int).Value = exercise_id;
                        cmd.Parameters.Add("@day", SqlDbType.Int).Value = day;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }
    }
}
