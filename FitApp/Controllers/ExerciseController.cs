﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class ExerciseController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public ExerciseController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/exercises/")]
        public JsonResult Get()
        {
            string query = @"ExerciseGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/exercises/")]
        public JsonResult Post(Exercise excercise)
        {

            try
            {
                string query = @"ExercisePOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@exercise_category_id", SqlDbType.Int).Value = excercise.exercise_category_id;
                        cmd.Parameters.Add("@duration", SqlDbType.Time, 7).Value = excercise.duration;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar, 4000).Value = excercise.description;
                        cmd.Parameters.Add("@exercise", SqlDbType.NVarChar, 32).Value = excercise.exercise;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/exercises/")]
        public JsonResult Put(Exercise excercise)
        {

            try
            {
                string query = @"ExercisePUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = excercise.id;
                        cmd.Parameters.Add("@duration", SqlDbType.Time, 7).Value = excercise.duration;
                        cmd.Parameters.Add("@description", SqlDbType.NVarChar, 4000).Value = excercise.description;
                        cmd.Parameters.Add("@exercise", SqlDbType.NVarChar, 32).Value = excercise.exercise;
                        cmd.Parameters.Add("@exercise_category_id", SqlDbType.Int).Value = excercise.exercise_category_id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }

        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/exercises/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"ExerciseDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }
    }
}
