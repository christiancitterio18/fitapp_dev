﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class User_healthController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public User_healthController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/userHealthConditions/")]
        public JsonResult Get()
        {
            string query = @"User_healthGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/userHealthConditions/")]
        public JsonResult Post(User_health health)
        {
            try
            {
                string query = @"User_healthPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@health_condition_id", SqlDbType.Int).Value = health.health_condition_id;
                        cmd.Parameters.Add("@user_person_id", SqlDbType.Int).Value = health.user_person_id;
                        cmd.Parameters.Add("@is_actual", SqlDbType.SmallInt).Value = health.is_actual;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/userHealthConditions/")]
        public JsonResult Put(User_health health)
        {
            try
            {
                string query = @"User_healthPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@health_condition_id", SqlDbType.Int).Value = health.health_condition_id;
                        cmd.Parameters.Add("@user_person_id", SqlDbType.Int).Value = health.user_person_id;
                        cmd.Parameters.Add("@is_actual", SqlDbType.SmallInt).Value = health.is_actual;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/userHealthConditions/{health_condition_id}/{user_person_id}")]
        public JsonResult Delete(int health_condition_id, int user_person_id)
        {
            try
            {
                string query = @"User_healthDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@health_condition_id", SqlDbType.Int).Value = health_condition_id;
                        cmd.Parameters.Add("@user_person_id", SqlDbType.Int).Value = user_person_id;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
