﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Body_part_exerciseController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Body_part_exerciseController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/bodyPartExercises/")]
        public JsonResult Get()
        {
            string query = @"Body_part_exerciseGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }

        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/bodyPartExercises/")]
        public JsonResult Post(Body_part_exercise bodyPartExercise)
        {
            try
            {
                string query = @"Body_part_exercisePOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Exercise_id", SqlDbType.Int).Value = bodyPartExercise.exercise_id;
                        cmd.Parameters.Add("@Body_part_Id", SqlDbType.Int).Value = bodyPartExercise.body_part_id;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/bodyPartExercises/{exercise_id}/{body_part_id}")]
        public JsonResult Delete(int exercise_id, int body_part_id)
        {
            try
            {
                string query = @"Body_part_exerciseDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@Exercise_id", SqlDbType.Int).Value = exercise_id;
                        cmd.Parameters.Add("@Body_part_Id", SqlDbType.Int).Value = body_part_id;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }

        }
    }
}
