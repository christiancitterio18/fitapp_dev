﻿using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class Diet_CategoryController : ControllerBase
    {
        private readonly IConfiguration _configuration;

        public Diet_CategoryController(IConfiguration configuration)
        {
            _configuration = configuration;
        }

        //GET
        [AllowAnonymous]
        [HttpGet]
        [Route("/api/dietCategories/")]
        public JsonResult Get()
        {
            string query = @"Diet_categoryGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/dietCategories/")]
        public JsonResult Post(Diet_category dietCategory)
        {
            try
            {
                string query = @"Diet_categoryPOST";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 25).Value = dietCategory.category;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }


        //UPDATE
        [Authorize]
        [HttpPut]
        [Route("/api/dietCategories/")]
        public JsonResult Put(Diet_category dietCategory)
        {
            try
            {
                string query = @"Diet_categoryPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 25).Value = dietCategory.category;
                        cmd.Parameters.Add("@id", SqlDbType.Int).Value = dietCategory.id;

                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }


        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/dietCategories/{id}")]
        public JsonResult Delete(int id)
        {
            try
            {
                string query = @"Diet_categoryDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@category", SqlDbType.NVarChar, 25).Value = id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
