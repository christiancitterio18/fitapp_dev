﻿using FitApp.Interfaces;
using FitApp.Models;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Controllers
{
    [Route("/api/[controller]")]
    [ApiController]
    public class App_userController : ControllerBase
    {
        private readonly IConfiguration _configuration;
        private readonly IUserMethods _userMethods;

        public App_userController(IConfiguration configuration, IUserMethods userMethods)
        {
            _configuration = configuration;
            _userMethods = userMethods;
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/users/")]
        public JsonResult Get()
        {
            string query = @"App_userGET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }
            return new JsonResult(table);
        }

        //GET
        [Authorize]
        [HttpGet]
        [Route("/api/users/{person_id}")]
        public JsonResult GetPerson(int person_id)
        {
            string query = @"User_account_GET";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }
            return new JsonResult(table);
        }


        //POST
        [Authorize]
        [HttpPost]
        [Route("/api/users/")]
        public JsonResult Post(App_user user)
        {
            try
            {

                DataTable table = _userMethods.AddUsers(user, _configuration);

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }

        //PUT
        [Authorize]
        [HttpPut]
        [Route("/api/users/")]
        public JsonResult Put(App_user user)
        {
            try
            {
                string query = @"App_userPUT";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = user.person_id;
                        cmd.Parameters.Add("@authorization_id", SqlDbType.Int).Value = user.authorization_id;
                        cmd.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = user.username;
                        cmd.Parameters.Add("@password", SqlDbType.NVarChar, 32).Value = user.password;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {

                return new JsonResult(e.Message);
            }
        }

        //DELETE
        [Authorize]
        [HttpDelete]
        [Route("/api/users/{person_id}")]
        public JsonResult Delete(int person_id)
        {
            try
            {
                string query = @"App_userDEL";

                DataTable table = new DataTable();
                string SqlDataSource = _configuration.GetConnectionString("FitAPP");
                SqlDataReader reader;
                using (SqlConnection con = new SqlConnection(SqlDataSource))
                {
                    con.Open();
                    using (SqlCommand cmd = new SqlCommand(query, con))
                    {
                        cmd.CommandType = CommandType.StoredProcedure;
                        cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = person_id;
                        reader = cmd.ExecuteReader();
                        table.Load(reader);

                        reader.Close();
                        con.Close();
                    }
                }

                return new JsonResult("Success");
            }
            catch (Exception e)
            {
                return new JsonResult(e.Message);
            }
        }
    }
}
