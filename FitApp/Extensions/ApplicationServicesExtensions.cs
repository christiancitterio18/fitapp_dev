﻿using FitApp.Interfaces;
using FitApp.Services;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Extensions
{
    public static class ApplicationServicesExtensions
    {
        public static IServiceCollection AddApplicationServices(this IServiceCollection services, IConfiguration config)
        {
            //Add TockenService
            services.AddScoped<ITockenService, TockenService>();

            //Add UserMethods
            services.AddScoped<IUserMethods, UserMethods>();

            //Add JsonErrorService
            services.AddScoped<IJsonErrorService, JsonErrorService>();

            return services;
        }
    }
}
