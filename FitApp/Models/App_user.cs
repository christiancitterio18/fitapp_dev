﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class App_user
    {
        [Required]
        public int person_id { get; set; }
        [Required]
        [StringLength(50, MinimumLength = 3,
        ErrorMessage = "username should be at least 3 character long, and maximum 50 character long!")]
        public string username { get; set; }
        [Required]
        [MinLength(8, ErrorMessage = "Password should contains at least 8 character!")]
        public string password { get; set; }
        [Required]
        public int authorization_id { get; set; }
    }
}
