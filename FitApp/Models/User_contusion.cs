﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class User_contusion
    {
        [Required]
        public int contusion_id { get; set; }
        [Required]
        public int user_person_id { get; set; }
        [Required]
        public int body_part_id { get; set; }
        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid date format!")]
        public DateTime contusion_date { get; set; }
        [Required]
        public string description { get; set; }
        [Required]
        [Range(0, 1, ErrorMessage = "Should be 0 or 1!")]
        public int is_actual { get; set; }
    }
}
