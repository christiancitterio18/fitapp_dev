﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Ingredient
    {
        [Required]
        public int id { get; set; }
        [Required]
        [StringLength(32, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 32 letters long!")]
        public string ingredient { get; set; }
        [Required]
        [Range(0, 1, ErrorMessage = "Should be only 0 or 1!")]
        public int is_alergenic { get; set; }
    }
}
