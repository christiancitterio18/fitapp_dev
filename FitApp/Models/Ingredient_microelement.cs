﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Ingredient_microelement
    {
        [Required]
        public int microelement_id { get; set; }
        [Required]
        public int ingredient_id { get; set; }
        [Required]
        [StringLength(12, MinimumLength = 1, ErrorMessage = "Should be minimum 1 and maximum 12 character long!")]
        [RegularExpression(@"([0-9]+|0).?[0-9]{0,2}", ErrorMessage = "Grams should have 2 decimal places and at least 1 number before '.'")]
        public string grams { get; set; }
    }
}
