﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Diet_meal
    {
        [Required]
        public int diet_id { get; set; }
        [Required]
        public int meal_id { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Should be greather than 0!")]
        public int day { get; set; }
    }
}
