﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Person
    {
        [Required]
        public int id { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 30 letters long!")]
        public string name { get; set; }
        [Required]
        [StringLength(30, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 30 letters long!")]
        public string surname { get; set; }
        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid date format!")]
        public DateTime birth_Date { get; set; }
        [Required]
        [RegularExpression(@"[M|F]", ErrorMessage = "Should be only M or F!")]
        public string sex { get; set; }
        [Required]
        [EmailAddress]
        public string email { get; set; }
        [Phone]
        public string phone { get; set; }
    }
}
