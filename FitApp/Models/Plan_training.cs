﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Plan_training
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int training_id { get; set; }
        [Required]
        public int app_user_id { get; set; }
        [Required]
        [DataType(DataType.DateTime, ErrorMessage = "Invalid date format!")]
        public DateTime date_from { get; set; }
        [Required]
        public DateTime date_to { get; set; }
        [Required]
        [Range(0, 1, ErrorMessage = "Should be 0 or 1!")]
        public int is_deleted { get; set; }
        [Required]
        public DateTime last_updated { get; set; }
    }
}
