﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Exercise
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int exercise_category_id { get; set; }
        [Required]
        public System.TimeSpan duration { get; set; }
        [StringLength(4000, ErrorMessage = "Description should not be longher than 4000 characters!")]
        public string description { get; set; }
        [Required]
        [StringLength(32, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 32 letters long!")]
        public string exercise { get; set; }
    }
}
