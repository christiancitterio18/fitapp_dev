﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Meal
    {
        [Required]
        public int id { get; set; }
        [Required]
        public int meal_category_id { get; set; }
        [Required]
        [StringLength(32, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 32 letters long!")]
        public string meal { get; set; }
        [StringLength(4000, ErrorMessage = "Description should not be longher than 4000 characters!")]
        public string description { get; set; }
        [Required]
        [RegularExpression(@"([0-9]+|0),?[0-9]{0,2}", ErrorMessage = "Calories should have 2 decimal places and at least 1 number before ','")]
        public decimal calories { get; set; }
    }
}
