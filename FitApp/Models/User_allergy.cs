﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class User_allergy
    {
        [Required]
        public int user_person_id { get; set; }
        [Required]
        public int ingredient_id { get; set; }
    }
}
