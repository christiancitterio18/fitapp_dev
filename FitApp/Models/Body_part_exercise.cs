﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Body_part_exercise
    {
        [Required]
        public int exercise_id { get; set; }
        [Required]
        public int body_part_id { get; set; }
    }
}
