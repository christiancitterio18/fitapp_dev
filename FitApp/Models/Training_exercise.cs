﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Training_exercise
    {
        [Required]
        public int training_id { get; set; }
        [Required]
        public int exercise_id { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Should be greather than 0!")]
        public int day { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Should be greather than 0!")]
        public int series_number { get; set; }
        [Required]
        [Range(1, int.MaxValue, ErrorMessage = "Should be greather than 0!")]
        public int repetition { get; set; }
    }
}
