﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class User_health
    {
        [Required]
        public int health_condition_id { get; set; }
        [Required]
        public int user_person_id { get; set; }
        [Required]
        [Range(0, 1, ErrorMessage = "Should be 0 or 1!")]
        public int is_actual { get; set; }
    }
}
