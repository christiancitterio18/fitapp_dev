﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Authorization
    {
        [Required]
        public int id { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 5, ErrorMessage = "Should be at least 5 and maximum 20 letters long!")]
        public string authorization { get; set; }
    }
}
