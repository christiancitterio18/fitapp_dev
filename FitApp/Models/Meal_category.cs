﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Meal_category
    {
        [Required]
        public int id { get; set; }
        [Required]
        [StringLength(25, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 25 letters long!")]
        public string category { get; set; }
    }
}
