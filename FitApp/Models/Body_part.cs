﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Models
{
    public class Body_part
    {
        [Required]
        public int id { get; set; }
        [Required]
        [StringLength(32, MinimumLength = 3, ErrorMessage = "Should be at least 3 and maximum 32 letters long!")]
        public string body_part { get; set; }
    }
}
