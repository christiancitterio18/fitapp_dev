﻿using FitApp.DTOs;
using FitApp.Interfaces;
using FitApp.Models;
using Microsoft.Extensions.Configuration;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FitApp.Services
{
    public class TockenService : ITockenService
    {
        private readonly SymmetricSecurityKey _key;

        public TockenService(IConfiguration config)
        {
            _key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(config["TockenKey"]));
        }


        public string CreateTocken(UserDTO appUser)
        {
            var claims = new List<Claim>
            {
                new Claim("id", appUser.id.ToString()),
                new Claim("name", appUser.username),
                new Claim("auth", appUser.authorization)
            };

            var credentials = new SigningCredentials(_key, SecurityAlgorithms.HmacSha512Signature);
            var tockenDesc = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.Now.AddDays(3),
                SigningCredentials = credentials
            };

            var tockenHandler = new JwtSecurityTokenHandler();

            var tocken = tockenHandler.CreateToken(tockenDesc);

            return tockenHandler.WriteToken(tocken);


        }
    }
}
