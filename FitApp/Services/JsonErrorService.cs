﻿using FitApp.Interfaces;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Services
{
    public class JsonErrorService : IJsonErrorService
    {
        public JsonResult badRequestResult(string message)
        {
            var res = new JsonResult(new { message = message });
            res.StatusCode = StatusCodes.Status400BadRequest;
            return res;
        }
    }
}
