﻿using FitApp.DTOs;
using FitApp.Interfaces;
using FitApp.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace FitApp.Services
{
    public class UserMethods : IUserMethods
    {
        public DataTable AddPerson(Person person, IConfiguration _configuration)
        {
            string query = @"PersonPOST";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@name", SqlDbType.NVarChar, 30).Value = person.name;
                    cmd.Parameters.Add("@surname", SqlDbType.NVarChar, 30).Value = person.surname;
                    cmd.Parameters.Add("@birth_Date", SqlDbType.Date).Value = person.birth_Date;
                    cmd.Parameters.Add("@sex", SqlDbType.NVarChar, 1).Value = person.sex;
                    cmd.Parameters.Add("@email", SqlDbType.NVarChar, 30).Value = person.email;
                    cmd.Parameters.Add("@phone", SqlDbType.NVarChar, 30).Value = (person.phone != null) ? person.phone : "";
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return table;
        }

        public DataTable AddUsers(App_user user, IConfiguration _configuration)
        {
            string query = @"App_userPOST";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@person_id", SqlDbType.Int).Value = user.person_id;
                    cmd.Parameters.Add("@authorization_id", SqlDbType.Int).Value = user.authorization_id;
                    cmd.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = user.username;
                    cmd.Parameters.Add("@password", SqlDbType.NVarChar, 32).Value = user.password;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);

                    reader.Close();
                    con.Close();
                }
            }

            return table;
        }

        public DataTable GetAccount(LoginDTO loginDto, IConfiguration _configuration)
        {

            string query = @"USER_LOGIN";

            DataTable table = new DataTable();
            string SqlDataSource = _configuration.GetConnectionString("FitAPP");
            SqlDataReader reader;
            using (SqlConnection con = new SqlConnection(SqlDataSource))
            {
                con.Open();
                using (SqlCommand cmd = new SqlCommand(query, con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;
                    cmd.Parameters.Add("@username", SqlDbType.NVarChar, 50).Value = loginDto.username;
                    reader = cmd.ExecuteReader();
                    table.Load(reader);



                    reader.Close();
                    con.Close();
                }
            }

            return table;
        }

        public string GetHash(string pass)
        {
            var hash = "";

            using (var md5 = MD5.Create())
            {
                var biteHash = Encoding.UTF8.GetBytes(pass);
                var compute = md5.ComputeHash(biteHash);

                hash = BitConverter.ToString(compute).Replace("-", "");
            }

            return hash;

        }
    }
}
