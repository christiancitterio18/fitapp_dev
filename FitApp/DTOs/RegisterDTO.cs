﻿using FitApp.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.DTOs
{
    public class RegisterDTO
    {
        public Person person { get; set; }
        public App_user user { get; set; }

    }
}
