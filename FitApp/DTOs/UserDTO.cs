﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.DTOs
{
    public class UserDTO
    {
        public int id { get; set; }
        public string username { get; set; }
        public string authorization { get; set; }
    }
}
