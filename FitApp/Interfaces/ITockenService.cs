﻿using FitApp.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Interfaces
{
    public interface ITockenService
    {
        string CreateTocken(UserDTO appUser);
    }
}
