﻿using FitApp.DTOs;
using FitApp.Models;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Interfaces
{
    public interface IUserMethods
    {
        DataTable AddUsers(App_user appUser, IConfiguration _configuration);
        DataTable AddPerson(Person person, IConfiguration _configuration);

        DataTable GetAccount(LoginDTO loginDto, IConfiguration _configuration);

        String GetHash(string pass);
    }
}
