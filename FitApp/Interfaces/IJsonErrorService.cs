﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FitApp.Interfaces
{
    public interface IJsonErrorService
    {
        JsonResult badRequestResult(string message);
    }
}
